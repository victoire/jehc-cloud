# jehc-front-vue

## 安装项目
```
yarn install
```

### 为开发编译和热重载
```
yarn serve
```

### 编译打包
```
yarn build
```

### Lints和修复文件
```
yarn lint
```

### 自定义配置
See [Configuration Reference](https://cli.vuejs.org/config/).
