const path = require("path");
const baseApi = "http://127.0.0.1:9527";
const port = process.env.port || process.env.npm_config_port || 9090 // dev port
const webpack = require('webpack');
module.exports = {
  publicPath:  "/",
  outputDir: 'dist',
  assetsDir: 'static',
  // publicPath:  process.env.NODE_ENV === "production" ? "/" : "/",
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        'window.Quill': 'quill/dist/quill.js',
        'Quill': 'quill/dist/quill.js'
      })
    ],
    resolve: {
      alias: {
        // If using the runtime only build
        vue$: "vue/dist/vue.runtime.esm.js" // 'vue/dist/vue.runtime.common.js' for webpack 1
        // Or if using full build of Vue (runtime + compiler)
        // vue$: 'vue/dist/vue.esm.js'      // 'vue/dist/vue.common.js' for webpack 1
      }
    }
  },
  devServer: {
    port: port,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    // before: require('./mock/mock-server.js'),
    proxy: {
      '/sysApi': { //平台中心API
        target: baseApi+'/sys', // http://localhost:7001  http://171.16.55.16:32181
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/sysApi': '/'
        }
      },
      '/oauthApi': { //授权中心API
        target: baseApi+'/oauth', // http://localhost:7001  http://171.16.55.16:32181
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/oauthApi': '/'
        }
      },
      '/ompApi':{ //运管平台API
        target: baseApi+'/omp', // http://localhost:7001  http://171.16.55.16:32181
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/ompApi': '/'
        }
      },
      '/fileApi':{ //文件中心API
        target: baseApi+'/file', // http://localhost:7001  http://171.16.55.16:32181
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/fileApi': '/'
        },
      },
      '/logApi':{ //日志中心API
        target: baseApi+'/log', // http://localhost:7001  http://171.16.55.16:32181
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/logApi': '/'
        }
      },
      '/jobApi':{ //调度中心API
        target: baseApi+'/job', // http://localhost:7001  http://171.16.55.16:32181
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/jobApi': '/'
        },
      },
      '/workflowApi':{ //工作流中心API
        target: baseApi+'/workflow', // http://localhost:7001  http://171.16.55.16:32181
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/workflowApi': '/'
        },
      },
      '/imApi':{ //即时通讯中心API
        target: baseApi+'/im', // http://localhost:7001  http://171.16.55.16:32181
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/imApi': '/'
        },
      },
      '/imWebSocketApi':{  //即时通讯中心WebSocket
        target: baseApi+'/imWebSocket', // http://localhost:7001  http://171.16.55.16:32181
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/imWebSocketApi': '/'
        },
      },
      '/monitorApi':{ //监控中心API
        target: baseApi+'/iotMonitor', // http://localhost:7001  http://171.16.55.16:32181
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/monitorApi': '/'
        }
      },
      '/opApi':{ //运维中心API
        target: baseApi+'/iotOp', // http://localhost:7001  http://171.16.55.16:32181
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/opApi': '/'
        },
      },
      '/reportApi':{
        target: baseApi+'/report', // http://localhost:7001  http://171.16.55.16:32181
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/reportApi': '/'
        },
      },
      '/medicalApi':{
        target: baseApi+'/medical', // http://localhost:7001  http://171.16.55.16:32181
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/medicalApi': '/'
        },
      }
    }
  },
  // chainWebpack: config => {
  //   config.module
  //     .rule("eslint")
  //     .use("eslint-loader")
  //     .tap(options => {
  //       options.configFile = path.resolve(__dirname, ".eslintrc.js");
  //       return options;
  //     });
  // },
  css: {
    loaderOptions: {
      postcss: {
        config: {
          path: __dirname
        }
      },
      scss: {
        prependData: `@import "@/assets/sass/vendors/vue/vuetify/variables.scss";`
      }
    }
  },
  transpileDependencies: ["vuetify"]
};
