import { Message, MessageBox } from 'element-ui'
import { Notification } from 'element-ui';
import { Loading } from 'element-ui';
import tokenUtil from "@/core/util/tokenUtil.js";
import { mapGetters } from "vuex";
import apiUtil from "@/core/util/apiUtil.js";
import { ROUTER_STORE, getRoutes } from "@/store/router.module";
import exp from 'constants';
import { async } from 'q';

/**
 * 设置Cookie
 * @param {} cName
 * @param {} value
 */
export function setCookie(cName, value, expiredays) {
  var exdate = new Date();
  exdate.setDate(exdate.getDate() + expiredays);
  document.cookie =
    cName +
    "=" +
    escape(value) +
    (expiredays == null ? "" : ";expires=" + exdate.toGMTString());
}

/**
 * 获取Cookie
 * @param {} Name
 * @return {}
 */
export function getCookie(Name) {
  var search = Name + "=";
  if (window.parent.document.cookie.length > 0) {
    var offset = window.parent.document.cookie.indexOf(search);
    if (offset != -1) {
      offset += search.length;
      var end = window.parent.document.cookie.indexOf(";", offset);
      if (end == -1) end = window.parent.document.cookie.length;
      return unescape(window.parent.document.cookie.substring(offset, end));
    } else return "";
  }
}

/**
 * 删除Cookie
 * @param {} name
 */
export function delCookie(name) {
  var exp = new Date();
  exp.setTime(exp.getTime() - 1);
  var cval = getCookie(name);
  if (cval != null)
    window.parent.document.cookie =
      name + "=" + cval + ";expires=" + exp.toGMTString();
}

/**
 * 从缓存中清除Cookie
 * @param {} name
 */
export function clearCookie(name) {
  var expdate = new Date();
  expdate.setTime(expdate.getTime() - 86400 * 1000 * 1);
  setCookie(name, "", expdate);
}

/**
 * form data转json
 * @param {*} formData 
 * @returns 
 */
export function formDataToJson(formData) {
  var objData = {};
  formData.forEach(function (curValue, index, arr) {
    objData[index] = curValue;
  });
  return JSON.stringify(objData)
}

/**
 * 
 * @param {显示笼罩层} wsetting 
 * @returns 
 */
let globalLoading;
let globalLoadingRequestCount = 0;//当前正在请求的数量
export function showWating(wsetting) {
  if (globalLoadingRequestCount === 0 && !globalLoading) {
    if (wsetting && wsetting.renderBody) {
      // 显示加载进度条
      globalLoading = Loading.service({
        lock: true,//lock的修改符--默认是false
        text: wsetting && wsetting.message ? wsetting.message : "拼命加载中...",//显示在加载图标下方的加载文案
        // spinner: 'el-icon-loading',//自定义加载图标类名
        spinner: 'spinner-border text-primary',//自定义加载图标类名
        background: wsetting && wsetting.background ? wsetting.background : 'rgba(0, 0, 0, 0.6)',//遮罩层颜色
        // body:wsetting && wsetting.body?wsetting.body:true,
        target: document.querySelector('#' + wsetting.renderBody)//loadin覆盖的dom元素节点
      });
    } else {
      // 显示加载进度条
      globalLoading = Loading.service({
        lock: true,//lock的修改符--默认是false
        text: wsetting && wsetting.message ? wsetting.message : "拼命加载中...",//显示在加载图标下方的加载文案
        // spinner: 'el-icon-loading',//自定义加载图标类名
        spinner: 'spinner-border text-primary',//自定义加载图标类名
        background: wsetting && wsetting.background ? wsetting.background : 'rgba(0, 0, 0, 0.6)',//遮罩层颜色、
        target: "body"
      });
    }
  }
  globalLoadingRequestCount++;
}

/**
 * 关闭笼罩层
 * @param {*} wsetting 
 */
export function closeWating(wsetting) {
  globalLoadingRequestCount--;
  globalLoadingRequestCount = Math.max(globalLoadingRequestCount, 0); //做个保护
  if (globalLoadingRequestCount === 0) {
    //关闭loading
    toHideLoading();
  }
}

/**
 * 防抖动设置300ms间隔内的关闭进度条便合并为一次。
 * 防止连续请求时， loading闪烁的问题。
 */
var toHideLoading = _.debounce(() => {
  if (null != globalLoading) {
    globalLoading.close();
    globalLoading = null;
  }
}, 300);

/**
 * 封装 element-ui弹框
 * @param text
 * @param type
 * @returns {Promise}
 */
export function handleConfirm(text = "确定执行此操作吗？", type = "danger") {
  return MessageBox.confirm(text, "提示", {
    confirmButtonText: "确定",
    cancelButtonText: "取消",
    type: type
  });
}

/**
 * 封装element-ui消息提示
 * @param text
 * @param type
 * @returns {Promise}
 */
export function handleAlert(text = "操作成功", type = "success", duration = 3) {
  return Message({
    showClose: true,
    message: text,
    center: true,
    type: type,
    duration: duration * 1000
  });
}

/**
 * 封装Notify提示组件
 * @param title
 * @param message
 * @param type
 * @returns {*}
 */
export function handleNotify(title = '标题', message = '内容', type = '类型', position = 'top-right', time = '3000') {
  Notification({
    title: title,
    message: message,
    type: type,         // success,warning,info,error
    position: position, // bottom-right/left,top-right/left
    duration: time      // 0不自动关闭
  })
}

/**
 * 深度拷贝
 * @param {*} data 
 * @returns 
 */
export const deepClone = (data) => {
  let result
  if (typeof data === 'object') {
    if (data == null) {
      result = null
    } else {
      if (data.constructor === Array) {
        result = []
        for (const i in data) {
          result.push(deepClone(data[i]))
        }
      } else {
        result = {}
        for (const i in data) {
          result[i] = deepClone(data[i])
        }
      }
    }
  } else {
    result = data
  }
  return result
}

/* 
将数组分割成多个数组
arr:要分割的数组
size : 分割后每个数组的长度
*/
export function chunkArray(arr, size) {
  let changeIndex = 0;
  let secondArr = [];
  while (changeIndex < arr.length) {
    secondArr.push(arr.slice(changeIndex, changeIndex += size))
  }
  return secondArr;
}

/**
 *
 * @returns {*}
 */
export function createXHR() {
  //检查是否支持XMLHttpRequest
  if (typeof XMLHttpRequest != "undefined") {
    //支持，返回XMLHttpRequest对象
    return new XMLHttpRequest();
  } else if (typeof ActiveXObject != "undefined") {//若是不支持XMLHttpRequest，就检查是否支持ActiveXObject
    //检查activeXString
    if (typeof arguments.callee.activeXString != "string") {
      let versions = ["MSXML2.XMLHttp.6.0", "MSXML2.XMLHttp.3.0", "MSXML2.XMLHttp"], i, len;
      for (i = 0, len = versions.length; i < len, i++;) {
        try {
          new ActiveXObject(versions[i]);
          arguments.callee.activeXString = versions[i];
          break;
        } catch (ex) {

        }
      }
    }
    return new ActiveXObject(arguments.callee.activeXString);//返回ActiveXObject
  } else {
    handleAlert("浏览器版本不支持下载！", "warning", 3);
    return null;
  }
}


/**
 *
 * @param url
 * @param method
 * @param fileName
 * @param params
 */
export function downloadFileCallFn(url, method, fileName, params) {
  let token = `${tokenUtil.getToken()}`;
  let xhr = createXHR();// 创建ajax异步对象
  console.log("xhr.status", xhr.status);

  if (null == xhr) {
    return;
  }
  xhr.timeout = 30000000;
  xhr.ontimeout = function (event) {
    handleAlert("请求超时", "warning", 3);
  }
  handleAlert("开始下载...", "info", 3);
  if (method.toLowerCase() == "post") {//POST
    xhr.open("POST", url, true);
    xhr.setRequestHeader("token", token);//设置header
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(params);// 发送请求
  } else {//GET
    xhr.open(method, url, true);
    xhr.setRequestHeader("token", token);//设置header
    xhr.send();// 发送请求
  }
  xhr.responseType = "blob"; // 设置响应类型为 blob形式
  let URL = window.URL || window.webkitURL;
  xhr.onreadystatechange = function (res) {// 响应
    if (xhr.readyState === 4) {
      if (xhr.status === 200) { // 响应成功
        let blobText = xhr.response; // 将返回的二进制流文件转换为blob形式的
        let a = document.createElement('a');// 创建a元素
        a.href = URL.createObjectURL(blobText); // 设置href,URL.createObjectURL(blobText) 将blob读取成一个url
        a.download = fileName; // 设置下载文件名称
        document.body.appendChild(a)
        a.click(); // 触发事件
        a.remove(); // 下载完成移除a元素
        handleAlert("下载完毕！", "success", 3);
      } else {
        handleAlert("下载失败！", "error", 3);// 响应失败处理
      }
    }
  };
}

/**
 * 时间对象的格式化;
 */
Date.prototype.format = function (format) {
  /*
   * eg:format="YYYY-MM-dd hh:mm:ss";
   */
  var o =
  {
    "M+": this.getMonth() + 1, // month
    "d+": this.getDate(), // day
    "h+": this.getHours(), // hour
    "m+": this.getMinutes(), // minute
    "s+": this.getSeconds(), // second
    "q+": Math.floor((this.getMonth() + 3) / 3), // quarter
    "S": this.getMilliseconds() // millisecond
  }
  if (/(y+)/.test(format)) {
    format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  }
  for (var k in o) {
    if (new RegExp("(" + k + ")").test(format)) {
      format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
    }
  }
  return format;
}

/**
 * 
 * @param {*} dataStr 
 * @returns 
 */
export function dateformat(dataStr) {
  if (null != dataStr) {
    let date = new Date(dataStr);
    return date.format("yyyy-MM-dd hh:mm:ss");
  }
  return "";
}


/**
 * 验证文件类型是图片还是非图片
 * @param {} type 
 */
export function isImageByType(type) {
  if ("image/jpeg" === type || "image/png" === type || "image/gif" === type || "image/bmp" === type) {
    return true;
  } else {
    return false;
  }
}

// /**
//  * 
//  * @param {*} src 
//  */
// export function getBimghw(src){
//   let imageAttribute = [];
//   try{     
//       let img_url = src+'?'+Date.parse(new Date());
//       //创建对象
//       let img = new Image();
//       //改变图片的src
//       img.src = img_url;
//       // 加载完成执行
//       let w =260;
//       let h =200;      

//       img.onload = function(){

//           w = img.width;
//           h = img.height;
//           if(h > 400){
//               h = 400;
//           }else{
//               h = 200;
//           }

//           if(w>1000){
//               w = 708;
//           }else{
//               w = 260;
//           }
//           imageAttribute.w = w;
//           imageAttribute.h = h; 
//           imageAttribute.url = img_url;
//           return imageAttribute;
//       };
//       img.onerror = function(){
//         handleAlert("该图片不能预览", "warning", 3)
//         return imageAttribute;
//       };
//   }catch(e){
//       //非法即不满足图片
//       handleAlert("该图片不能预览", "warning", 3);
//       return imageAttribute;
//   }
// }


/**
 * 递归方式处理父节点
 * @param {*} data 
 */
export function doRouterTree(data) {
  const parent = []
  for (let i = 0; i < data.length; i++) {
    if ("0" === data[i].resources_parentid) {
      let assemblyUri = data[i].component_router_to;//目标地址或component组件地址
      const pRouter = {
        path: data[i].component_router,
        id: data[i].resources_id,
        component: (resolve) => require(["@/view/layout/layout"], resolve),
      };
      if ('' !== assemblyUri && null !== assemblyUri) {
        pRouter.children = [{
          id: data[i].resources_id,
          name: data[i].component_router,
          path: data[i].component_router,
          component: (resolve) => require(["@/view/" + assemblyUri.replace("/view/", "")], resolve),
        }];
      }
      parent.push(pRouter);
    }
  }
  doRouterChildrenTree(parent, data)//递归处理子菜单

  //解决刷新页面404
  let page404 = {
    path: "/404",
    name: "404",
    component: () => import("@/view/sys/error/404.vue")
  }
  let router404 = {
    path: "*",
    redirect: "/404"
  }
  parent.push(page404);
  parent.push(router404);
  return parent;
}

/**
 * 处理子节点
 * @param {*} parent 
 * @param {*} data 
 */
export function doRouterChildrenTree(parent, data) {
  if (data && parent) {
    for (let i = 0; i < parent.length; i++) {
      for (let j = 0; j < data.length; j++) {
        if (parent[i].id === data[j].resources_parentid) {
          let assemblyUri = data[j].component_router_to;
          if (null === assemblyUri || '' === assemblyUri) {
            let children = {
              id: data[j].resources_id,
              name: data[j].component_router,
              path: data[j].component_router,
            };
            //注意： require(["@/view/"+assemblyUri 这种写法可以 千万不要写成require(["@"+assemblyUri 一个@符合是不行的
            children.component = (resolve) => require(["@/view/layout/layout"], resolve)
            if (undefined === parent[i].children) {
              parent[i].children = [];
            }
            parent[i].children.push(children)
          } else {
            let children = {
              id: data[j].resources_id,
              name: data[j].component_router,
              path: data[j].component_router,
            };
            //注意： require(["@/view/"+assemblyUri 这种写法可以 千万不要写成require(["@"+assemblyUri 一个@符合是不行的
            children.component = (resolve) => require(["@/view/" + assemblyUri.replace("/view/", "")], resolve)
            if (undefined === parent[i].children) {
              parent[i].children = [];
            }
            parent[i].children.push(children)
          }
        }
      }
      doRouterChildrenTree(parent[i].children, data)
    }
  }
}

/**
 * 非递归方式只取一级
 * @param {*} dataMenuList 
 * @returns 
 */
export function doRouter(dataMenuList) {//处理动态路由
  const router = [];
  if (dataMenuList) {
    const pRouter = {
      path: "/",
      component: (resolve) => require(["@/view/layout/layout"], resolve),
      children: [],
    };
    for (let i in dataMenuList) {
      let data = dataMenuList[i];
      let assemblyUri = data.component_router_to;
      if (null != data.component_router && '' != data.component_router) {
        let children = {
          name: data.component_router,
          path: data.component_router,
        };
        //注意： require(["@/view/"+assemblyUri 这种写法可以 千万不要写成require(["@"+assemblyUri 一个@符合是不行的
        children.component = (resolve) => require(["@/view/" + assemblyUri.replace("/view/", "")], resolve)
        pRouter.children.push(children);
      }
    }
    router.push(pRouter);
  }
  //解决刷新页面404
  let page404 = {
    path: "/404",
    name: "404",
    component: () => import("@/view/sys/error/404.vue")
  }
  let router404 = {
    path: "*",
    redirect: "/404"
  }
  router.push(page404);
  router.push(router404);
  return router;
}

/**
 * 
 * @returns 
 */
export function getRouter() {
  return [{
    path: "/",
    component: () => import("@/view/layout/layout"),
    children: [
      {//附件
        name: "/file/attachment",
        path: "/file/attachment",
        component: () => import("@/view/file/file-attachment/file-attachment-list.vue")
      },
      {//页面加载时长统计
        name: "/log/loading",
        path: "/log/loading",
        component: () => import("@/view/log/log-loadinfo/log-loadinfo-list.vue")
      },
      {//服务停止与启动
        name: "/log/startStop",
        path: "/log/startStop",
        component: () => import("@/view/log/log-start-stop/log-start-stop-list.vue")
      },
      {//操作日志
        name: "/log/operate",
        path: "/log/operate",
        component: () => import("@/view/log/log-operate/log-operate-list.vue")
      },
      {//异常日志
        name: "/log/error",
        path: "/log/error",
        component: () => import("@/view/log/log-error/log-error-list.vue")
      },
      {//变更日志
        name: "/log/modify",
        path: "/log/modify",
        component: () => import("@/view/log/log-modify-record/log-modify-record-list.vue")
      },
      {//登录日志
        name: "/log/login",
        path: "/log/login",
        component: () => import("@/view/log/log-login/log-login-list.vue")
      },



      {//公司信息
        name: "/sys/company",
        path: "/sys/company",
        component: () => import("@/view/sys/xt-company/xt-company.vue")
      },
      {//部门管理
        name: "/sys/departinfo",
        path: "/sys/departinfo",
        component: () => import("@/view/sys/xt-departinfo/xt-departinfo-list.vue")
      },
      {//岗位管理
        name: "/sys/post",
        path: "/sys/post",
        component: () => import("@/view/sys/xt-post/xt-post-list.vue")
      },
      {//用户管理
        path: "/sys/userinfo",
        name: "/sys/userinfo",
        component: () => import("@/view/sys/xt-userinfo/xt-userinfo-list.vue"),
      },
      {//组织机构
        path: "/sys/org",
        name: "/sys/org",
        component: () => import("@/view/sys/xt-org/xt-org-list.vue"),
      },

      {//系统消息
        path: "/sys/message",
        name: "/sys/message",
        component: () => import("@/view/sys/xt-message/xt-message-list.vue"),
      },
      {//平台消息
        path: "/sys/platform",
        name: "/sys/platform",
        component: () => import("@/view/sys/xt-platform/xt-platform-list.vue"),
      },
      {//公告
        path: "/sys/notify",
        name: "/sys/notify",
        component: () => import("@/view/sys/xt-notify/xt-notify-list.vue"),
      },
      {//通知
        path: "/sys/notice",
        name: "/sys/notice",
        component: () => import("@/view/sys/xt-notice/xt-notice-list.vue"),
      },
      {//知识库
        path: "/sys/knowledge",
        name: "/sys/knowledge",
        component: () => import("@/view/sys/xt-knowledge/xt-knowledge-list.vue"),
      },
      {//计量单位
        path: " /sys/dev/config/unit",
        name: "/sys/dev/config/unit",
        component: () => import("@/view/sys/xt-unit/xt-unit-list.vue"),
      },
      {//常量
        path: " /sys/dev/config/constant",
        name: "/sys/dev/config/constant",
        component: () => import("@/view/sys/xt-constant/xt-constant-list.vue"),
      },
      {//路径
        path: " /sys/dev/config/path",
        name: "/sys/dev/config/path",
        component: () => import("@/view/sys/xt-path/xt-path-list.vue"),
      },
      {//数据字典
        path: " /sys/dev/config/dictionary",
        name: "/sys/dev/config/dictionary",
        component: () => import("@/view/sys/xt-data-dictionary/xt-data-dictionary-list.vue"),
      },
      {//第三方短信
        path: " /sys/dev/config/sms",
        name: "/sys/dev/config/sms",
        component: () => import("@/view/sys/xt-sms/xt-sms-list.vue"),
      },
      {//ip冻结
        path: " /sys/dev/config/ipFrozen",
        name: "/sys/dev/config/ipFrozen",
        component: () => import("@/view/sys/xt-ip-frozen/xt-ip-frozen-list.vue"),
      },
      {//行政区划
        path: " /sys/dev/area/region",
        name: "/sys/dev/area/region",
        component: () => import("@/view/sys/xt-area-region/xt-area-region-list.vue"),
      },
      {//版本管理
        path: "/sys/dev/version",
        name: "/sys/dev/version",
        component: () => import("@/view/sys/xt-version/xt-version-list.vue"),
      },
      {//单号
        path: "/sys/dev/number",
        name: "/sys/dev/number",
        component: () => import("@/view/sys/xt-number/xt-number-list.vue"),
      },
      {//条码二维码
        path: "/sys/dev/encoderqrcode",
        name: "/sys/dev/encoderqrcode",
        component: () => import("@/view/sys/xt-encoderqrcode/xt-encoderqrcode-list.vue"),
      },


      {//账户
        path: "/oauth/account",
        name: "/oauth/account",
        component: () => import("@/view/oauth/oauth-account/oauth-account-list.vue"),
      },
      {//在线用户
        path: "/oauth/online",
        name: "/oauth/online",
        component: () => import("@/view/oauth/oauth-online/oauth-online-list.vue"),
      },
      {//角色权限
        path: "/oauth/role",
        name: "/oauth/role",
        component: () => import("@/view/oauth/oauth-role/oauth-role-list.vue"),
      },
      {//模块卸载
        path: "/oauth/module/uninstall",
        name: "/oauth/module/uninstall",
        component: () => import("@/view/oauth/oauth-module-uninstall/oauth-module-uninstall.vue"),
      },
      {//账户类型
        path: "/oauth/accountType",
        name: "/oauth/accountType",
        component: () => import("@/view/oauth/oauth-account-type/oauth-account-type-list.vue"),
      },
      {//平台密钥
        path: "/oauth/keyinfo",
        name: "/oauth/keyinfo",
        component: () => import("@/view/oauth/oauth-key-info/oauth-key-info-list.vue"),
      },
      {//配置非权限API
        path: "/oauth/function-common",
        name: "/oauth/function-common",
        component: () => import("@/view/oauth/oauth-function-common/oauth-function-common-list.vue"),
      },
      {//资源模块
        path: "/oauth/modules",
        name: "/oauth/modules",
        component: () => import("@/view/oauth/oauth-sys-modules/oauth-sys-modules-list.vue"),
      },
      {//菜单资源
        path: "/oauth/resources",
        name: "/oauth/resources",
        component: () => import("@/view/oauth/oauth-resources/oauth-resources-list.vue"),
      },
      {//功能资源
        path: "/oauth/function-info",
        name: "/oauth/function-info",
        component: () => import("@/view/oauth/oauth-function-info/oauth-function-info-list.vue"),
      },
      {//授权中心子系统
        path: "/oauth/mode",
        name: "/oauth/mode",
        component: () => import("@/view/oauth/oauth-sys-mode/oauth-sys-mode-list.vue"),
      },

      {//分配管理员
        path: "/oauth/admin",
        name: "/oauth/admin",
        component: () => import("@/view/oauth/oauth-admin/oauth-admin-list.vue"),
      },

      {//调度日志
        path: "/job/log",
        name: "/job/log",
        component: () => import("@/view/job/job-log/job-log-list.vue"),
      },
      {//配置任务
        path: "/job/config",
        name: "/job/config",
        component: () => import("@/view/job/job-config/job-config-list.vue"),
      },
      {//执行任务
        path: "/job/execute",
        name: "/job/execute",
        component: () => import("@/view/job/job-set/job-set-list.vue"),
      },

      //运管平台
      {//Redis配置
        path: " /omp/redisConfig",
        name: " /omp/redisConfig",
        component: () => import("@/view/omp/rds/omp-redis-config/omp-redis-config-list.vue"),
      },

      {//Redis监控
        path: " /omp/redisMain",
        name: " /omp/redisMain",
        component: () => import("@/view/omp/rds/omp-redis-main/omp-redis-main-list.vue"),
      },

      {//Redis监控日志
        path: " /omp/redisInfo",
        name: " /omp/redisInfo",
        component: () => import("@/view/omp/rds/omp-redis-info/omp-redis-info-list.vue"),
      },

      {//内存运行日志
        path: " /omp/mem",
        name: " /omp/mem",
        component: () => import("@/view/omp/scms/scms-monitor-mem/scms-monitor-mem-list.vue"),
      },
      {//CPU运行日志
        path: " /omp/cpu",
        name: " /omp/cpu",
        component: () => import("@/view/omp/scms/scms-monitor-cpu/scms-monitor-cpu-list.vue"),
      },
      {//物理机信息
        path: " /omp/monitor",
        name: " /omp/monitor",
        component: () => import("@/view/omp/scms/scms-monitor/scms-monitor-list.vue"),
      },
      {//授权中心耗时追踪
        path: "/omp/awstats/authLog",
        name: "/omp/awstats/authLog",
        component: () => import("@/view/omp/awstats/auth-log/auth-log-list.vue"),
      },
      {//路由信息
        path: "/omp/route/dynamic/route",
        name: "/omp/route/dynamic/route",
        component: () => import("@/view/omp/route/dynamic-route/dynamic-route-list.vue"),
      },
      {//路由模块
        path: "/omp/route/dynamic/route/modules",
        name: "/omp/route/dynamic/route/modules",
        component: () => import("@/view/omp/route/dynamic-route-modules/dynamic-route-modules-list.vue"),
      },
    ]
  }];
}

/**
 * 
 */
export function reloadRouter(isReq) {
  const JEHC_ROUTER = "Routers";
  return new Promise((resolve, reject) => {
    if(isReq === true){
      apiUtil.get(process.env.VUE_APP_OAUTH_API + "/init/v1").then(({ data }) => {
        let dataMenuList = [];
        if (null != data.data && null != data.data.menuEntity) {
          let result = data.data.menuEntity;       
          for (let i in result) {
            let arr = result[i];
            if (null != arr && "" != arr) {
              let menuArr = JSON.parse(arr.menuList);
              if (null != menuArr && menuArr.length > 0) {
                for (let j in menuArr) {
                  dataMenuList.push(menuArr[j]);
                }
              }
            }
          }
        }
        setLocalItem(JEHC_ROUTER, JSON.stringify(dataMenuList));
        let result = doRouter(dataMenuList);//非递归方式 只取一级
        // let result = doRouterTree(dataMenuList);//采用递归方式
        resolve(result)
      });
    }else{
      let result = doRouter();//非递归方式 只取一级
      resolve(result)
    }  
  })
}

/**
 * 编写动态路由组装业务逻辑方法
 */
export function getDynamicsRouters(isReq,isSetLoad) {
  const JEHC_ROUTER_NEED_LOAD = "JEHC_ROUTER_NEED_LOAD"; 
  return new Promise((resolve, reject) => {
    let localRoutersJson = localStorage.getItem("Routers");
    if (!localRoutersJson) {
      reloadRouter(isReq).then(res => {
        if (null != res && res.length > 0) {
          console.log("重新请求...",res);        
          resolve(res)
          if(isReq && isSetLoad){
            setLocalItem(JEHC_ROUTER_NEED_LOAD, 1);//重新请求的时候 设置标识 用于 动态路由重新加载
          }
        }
      })
    } else {
      let localRouters = JSON.parse(localRoutersJson);
      // let localRoutersRes = doRouterTree(localRouters);      
      let localRoutersRes = doRouter(localRouters);
      if (null != localRoutersRes) {//从缓存中读取        
        console.log("从缓存中加载路由...");
        resolve(localRoutersRes)
      }
    }
  })
}

/**
 * 设置缓存
 * @param {*} key 
 * @param {*} value 
 */
export function setLocalItem(key,value){
  window.localStorage.setItem(key, value);
}

/**
 * 删除缓存
 * @param {*} key 
 * @param {*} value 
 */
export function removeLocalItem(key){
  window.localStorage.removeItem(key);
}

/**
 * 获取缓存
 * @param {*} key 
 * @param {*} value 
 */
export function getLocalItem(key){
  return window.localStorage.getItem(key);
}
