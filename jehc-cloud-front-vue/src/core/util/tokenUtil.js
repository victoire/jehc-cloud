const ID_TOKEN_KEY = "token";
const JEHC_SESSION_ID = "JEHC-SESSION-ID";
const JEHC_ROUTER = "Routers";
const JEHC_ROUTER_NEED_LOAD = "JEHC_ROUTER_NEED_LOAD";
import {setLocalItem,removeLocalItem,getLocalItem} from "@/core/util/jehcUtil.js";  
import Router from "@/router.js";


export const getToken = () => {
//  return window.localStorage.getItem(ID_TOKEN_KEY);////废弃获取token方法
    return getLocalItem(ID_TOKEN_KEY);///通过cookie获取
};

export const saveToken = token => {
//  window.localStorage.setItem(ID_TOKEN_KEY, token);////废弃获取token方法
    setLocalItem(ID_TOKEN_KEY, token); ;//通过cookie存储
};

export const destroyToken = () => {
//  window.localStorage.removeItem(ID_TOKEN_KEY);//废弃删除token方法
    removeLocalItem(ID_TOKEN_KEY);//删除cookie中token
    removeLocalItem(JEHC_SESSION_ID);//删除cookie中sessionid    
    removeLocalItem(JEHC_ROUTER);//删除动态路由
    removeLocalItem(JEHC_ROUTER_NEED_LOAD);//删除是否需要第一次加载
};

/**
 * @description get token form cookie
 */
export const getJSessionId = () => {
    return getLocalItem(JEHC_SESSION_ID);///通过cookie获取
};

export default { getToken, saveToken, destroyToken ,getJSessionId};
