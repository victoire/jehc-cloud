package jehc.cloud.gateway.config;

import jehc.cloud.gateway.route.service.DynamicRouteEvent;
import jehc.cloud.sentinel.log.MetricLog;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    /**
     * 开启Sentinel客户端日志上报
     * @return
     */
    @Bean(destroyMethod = "destroy", initMethod = "execute")
    public MetricLog initUploadLogBean(){
        return new MetricLog();
    }

    /**
     * 监听路由
     * @return
     */
    @Bean(destroyMethod = "destroy", initMethod = "execute")
    public DynamicRouteEvent initRouteBean(){
        return new DynamicRouteEvent();
    }
}