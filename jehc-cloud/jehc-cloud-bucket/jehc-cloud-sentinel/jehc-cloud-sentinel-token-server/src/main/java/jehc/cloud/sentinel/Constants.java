package jehc.cloud.sentinel;
/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public final class Constants {

    public static final String FLOW_RULES_POSTFIX = "-flow-rules";
    public static final String PARAM_RULES_POSTFIX = "-param-rules";

    private Constants() {}
}
