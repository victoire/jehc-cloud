package jehc.cloud.common.base.druid;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;
/**
 * @Desc druid属性
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@Repository
@Data
public class DruidConfigBean {
    @Value("${jehc.datasource.type:}")
    private String type;

    @Value("${jehc.datasource.driverClassName:}")
    private String driverClassName;

    @Value("${master.username:}")
    private String masterName;

    @Value("${master.password:}")
    private String masterPassword;

    @Value("${master.url:}")
    private String masterUrl;

    @Value("${slave.username:}")
    private String slaveName;

    @Value("${slave.password:}")
    private String slavePassword;

    @Value("${slave.url:}")
    private String slaveUrl;

    @Value("${jehc.initialSize:2}")
    private int initialSize;

    @Value("${jehc.minIdle:1}")
    private int minIdle;

    @Value("${jehc.maxActive:5}")
    private int maxActive;

    @Value("${jehc.maxWait:60000}")
    private int maxWait;

    @Value("${jehc.timeBetweenEvictionRunsMillis:60000}")
    private int timeBetweenEvictionRunsMillis;

    @Value("${jehc.minEvictableIdleTimeMillis:300000}")
    private int minEvictableIdleTimeMillis;

    @Value("${jehc.maxEvictableIdleTimeMillis:900000}")
    private int maxEvictableIdleTimeMillis;

    @Value("${jehc.validationQuery:}")
    private String validationQuery;

    @Value("${jehc.testWhileIdle:true}")
    private boolean testWhileIdle;

    @Value("${jehc.testOnBorrow:false}")
    private boolean testOnBorrow;

    @Value("${jehc.testOnReturn:false}")
    private boolean testOnReturn;


    @Value("${jehc.deny:}")
    private String deny;

    @Value("${jehc.allow:}")
    private String allow;

    @Value("${jehc.loginUsername:druid}")
    private String loginUsername;

    @Value("${jehc.loginPassword:}")
    private String loginPassword;

    @Value("${jehc.resetEnable:false}")
    private String resetEnable;

    @Value("${jehc.exclusions:}")
    private String exclusions;

    @Value("${jehc.mergeSql:true}")
    private boolean mergeSql;

    @Value("${jehc.slowSqlMillis:1000}")
    private long lowSqlMillis;

    @Value("${jehc.logSlowSql:true}")
    private boolean logSlowSql;

    @Value("${jehc.filters:stat}")
    private String filters;

    @Value("${jehc.connectionProperties:}")
    private String connectionProperties;

    @Value("${jehc.poolPreparedStatements:true}")
    private boolean poolPreparedStatements;

    @Value("${jehc.maxPoolPreparedStatementPerConnectionSize:20}")
    private Integer maxPoolPreparedStatementPerConnectionSize;
}
