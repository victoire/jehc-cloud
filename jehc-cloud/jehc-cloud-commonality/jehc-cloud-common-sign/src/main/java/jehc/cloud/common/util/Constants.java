package jehc.cloud.common.util;

import lombok.extern.slf4j.Slf4j;

/**
 * @Desc 常量
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class Constants {

    /**
     * POST请求方法名
     */
    public static final String HTTP_POST = "POST";


    /**
     * GET请求方法名
     */
    public static final String HTTP_GET = "GET";

    /**
     * PUT请求方法名
     */
    public static final String HTTP_PUT = "GET";


    /**
     * DELETE请求方法名
     */
    public static final String HTTP_DELETE = "DELETE";

    public static final String TRACE_KEY= "traceKey";//追踪Key

    public static final String SIGN_BODY="BODY";//验签范围类型 BODY

    public static final String SIGN_HEADER="HEADER";//验签范围类型 HEADER

    public static final String SIGN_PATH="PATH";//验签范围类型 PATH

    public static final String SIGN_ALL="ALL";//验签范围类型 ALL

    public static final String SIGN_FORM="FORM";//验签范围类型 FORM
}
