package jehc.cloud.common.base;

import lombok.Data;

/**
 * @Desc JsTree 状态节点
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class BaseJSTreeStateEntity {
    private Boolean opened = false;/**是否展开默认展开**/
    private Boolean disabled = false;/**是否禁用**/
    private Boolean selected = false;/**是否可选择**/

    public BaseJSTreeStateEntity(){

    }

    public BaseJSTreeStateEntity(Boolean opened,Boolean disabled,Boolean selected){
        this.opened = opened;
        this.disabled =disabled;
        this.selected = selected;
    }
}

