package jehc.cloud.common.base;

import lombok.Data;

/**
 * @Desc JsTree 属性
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class BaseJSTreeAtrrEntity {
    private String id;
}
