package jehc.cloud.common.util.logger;

import ch.qos.logback.core.PropertyDefinerBase;
import jehc.cloud.common.util.GetClientIp;
import lombok.extern.slf4j.Slf4j;

/**
 * @Desc Logback 文件增加ip记录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class IPPropertyDefinerBase extends PropertyDefinerBase {

    private String cache;
    @Override
    public String getPropertyValue() {
        if (cache != null) {
            return cache;
        }
        String result = GetClientIp.getLocalIp();
        cache = result;
        return result;
    }
}
