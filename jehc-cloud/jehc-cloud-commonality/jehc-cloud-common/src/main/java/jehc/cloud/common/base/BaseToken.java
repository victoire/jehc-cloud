package jehc.cloud.common.base;

import lombok.Data;
/**
 * @Desc BaseToken
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class BaseToken {
    private String updateTimes;//最后更新时间
    private Long times;//活跃次数
    private Boolean success = true;//是否成功
    private String clientId;//客户端id（账号id）

    private Boolean keepAlive = false;//是否活跃 默认false

    public BaseToken(){

    }
    public BaseToken(String clientId){
        this.clientId =clientId;
    }
    public BaseToken(String clientId,Boolean keepAlive){
        this.clientId =clientId;
        this.keepAlive = keepAlive;
    }

    public BaseToken(String updateTimes, Long times, boolean success){
        this.updateTimes = updateTimes;
        this.times = times;
        this.success =success;
    }
    public BaseToken(String updateTimes, Long times, boolean success,String clientId){
        this.updateTimes = updateTimes;
        this.times = times;
        this.success =success;
        this.clientId = clientId;
    }
}
