package jehc.cloud.common.entity;

import lombok.Data;

/**
 * @Desc 数据字典实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class DataDictionaryEntity {
    private String xt_data_dictionary_id;/**主键**/
    private String xt_data_dictionary_name;/**数据字典名称**/
    private String xt_data_dictionary_pid;/**父编号**/
    private String xt_data_dictionary_remark;/**注备**/
    private String xt_data_dictionary_value;/**数值**/
    private int xt_data_dictionary_state;/**态状0启用1暂停**/
    private String xt_data_dictionary_soft;/**排序号**/
}
