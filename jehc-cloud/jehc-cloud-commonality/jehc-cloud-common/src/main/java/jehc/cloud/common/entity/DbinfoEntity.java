package jehc.cloud.common.entity;
import lombok.Data;
/**
 * @Desc 数据库信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class DbinfoEntity{
    private String xt_dbinfo_id;/**数据库信息表**/
    private String xt_dbinfoName;/**据库数名称**/
    private String xt_dbinfoUName;/**数据库用户名**/
    private String xt_dbinfoPwd;/**据库数密码**/
    private String xt_dbinfoIp;/**份备IP**/
    private String xt_dbinfoPort;/**端口号**/
    private String xt_dbinfoType;/**数据库类型**/
}