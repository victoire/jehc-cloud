package jehc.cloud.common.base;

import lombok.Data;
import java.io.Serializable;
/**
 * @Desc 分页类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class BasePage<T> extends BaseResult<T> implements Serializable{
	private static final long serialVersionUID = 7255593576877669235L;
	private Boolean usePageNo = false;//是否采用当前第几页方式分页（老版本前端采用否，新版本） 默认否
	private Integer start = 0;
	private Integer pageNo = 1;
	private Integer pageSize = 30;
	private Long total = 0L;
	private T data;
	private Boolean success = true;

	public BasePage(){}

	public BasePage(Integer pageNo,Integer pageSize,Long total,T data,Boolean success){
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.total = total;
		this.data = data;
		this.success = success;
	}

	public BasePage(Integer start,Integer pageNo,Integer pageSize,Long total,T data,Boolean success){
		this.start = start;
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.total = total;
		this.data = data;
		this.success = success;
	}
}

