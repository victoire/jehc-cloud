package jehc.cloud.common.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
* @Desc 异常日志
* @Author 邓纯杰
* @CreateTime 2022-08-24 11:24:05
*/
@Data
public class LogErrorDTO extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**异常ID**/
	private String content;/**异常日志内容**/
	private Integer type;/**异常日志级别**/
	private String create_id;/**创建人id**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	private String update_id;/**修改人id**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/
	private Integer del_flag;/**删除标记：0正常1已删除**/
}
