package jehc.cloud.common.base;

import cn.hutool.core.collection.CollectionUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Desc ZTree
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class BaseZTreeEntity {
	private String id;/**ID编号**/
	private String pId;/**父级编号**/
	private String text;/**显示值**/
	private String name;/**节点名称**/
	private String icon;/**图片**/
	private String qtip;/**提示**/
	private Boolean isParent = false;/**是否存在子节点**/
	private Boolean leaf = false;/**叶子**/
	private Boolean hasLeaf;/**叶子**/
	private Boolean expanded = false;/**是否展开默认展开**/
	private Boolean checked = false;/**是否支持全选**/
 	private Boolean singleClickExpand = false;/**单击展开默认否false为双击节点展开 true单击节点展开**/
	private String tempObject;/**临时值**/
	private String content;/**描述**/
	private String integerappend;/**int类型拼接使用可以使用@或逗号隔开**/
	private List<BaseZTreeEntity> children;/**子节点集合**/

	/**
	 *
	 * @param list
	 * @param parent
	 * @return
	 */
	public List<BaseZTreeEntity> buildTree(List<BaseZTreeEntity> list, String parent){
		List<BaseZTreeEntity> baseZTreeEntitieList =new ArrayList<BaseZTreeEntity>();
		for(BaseZTreeEntity baseZTreeEntity : list) {
//			baseZTreeEntity.setLeaf(isLeaf(list,baseZTreeEntity));
			String nodeId = baseZTreeEntity.getId();
			String pid = baseZTreeEntity.getPId();
			if (parent.equals(pid)) {
				List<BaseZTreeEntity> baseZTreeEntities = buildTree(list, nodeId);
				if(!CollectionUtil.isEmpty(baseZTreeEntities)){
					if(null == baseZTreeEntity.getChildren()){
						children = new ArrayList<>();
						baseZTreeEntity.setIsParent(true);
						baseZTreeEntity.setLeaf(true);
						children.addAll(baseZTreeEntities);
						baseZTreeEntity.setChildren(children);
					}

				}
				baseZTreeEntitieList.add(baseZTreeEntity);
			}
		}
		return baseZTreeEntitieList;
	}

//
//	public static String buildTree(List<BaseZTreeEntity> list,boolean isHasChecbox){
//    	StringBuffer html = new StringBuffer();
//    	html.append("[");
//        for(int i = 0; i < list.size(); i++) {
//        	BaseZTreeEntity node = list.get(i);
//            if ("0".equals(node.getPId())) {
//            	String icons = "";
//            	if(!StringUtils.isEmpty(node.getIcon())){
//            		icons = " icon:'"+node.getIcon()+"',";
//            	}
//            	if(isHasChecbox){
//            		html.append("{name:'" + node.getText() + "',id:'" + node.getId()+ "',checked:"+node.getChecked()+",singleClickExpand:"+node.getSingleClickExpand()+",pId:'"+node.getPId()+"',leaf:"+isLeaf(list,node)+","+icons+"qtip:'"+node.getText()+"',open:"+node.getExpanded()+",tempObject:'"+node.getTempObject()+"',content:'"+node.getContent()+"',integerappend:'"+node.getIntegerappend()+"'");
//            	}else{
//            		html.append("{name:'" + node.getText() + "',id:'" + node.getId()+ "',singleClickExpand:"+node.getSingleClickExpand()+",pId:'"+node.getPId()+"',leaf:"+isLeaf(list,node)+","+icons+"qtip:'"+node.getText()+"',open:"+node.getExpanded()+",tempObject:'"+node.getTempObject()+"',content:'"+node.getContent()+"',integerappend:'"+node.getIntegerappend()+"'");
//            	}
//            	html.append(build(list,node,isHasChecbox));
//            	html.append("},");
//            }
//        }
//        html.append("]");
//        String jsonStr = html.toString().replaceAll("},]", "}]");
//        return jsonStr;
//    }
//    /**
//     *
//     * @param list
//     * @param node
//	 * @param isHasChecbox
//     */
//    private static String build(List<BaseZTreeEntity> list,BaseZTreeEntity node,boolean isHasChecbox){
//    	StringBuffer html = new StringBuffer();
//    	List<BaseZTreeEntity> children = getChildren(list,node,isHasChecbox);
//        if (!children.isEmpty() && children.size()>0) {
//        	html.append(",children:[");
//        	for(int i = 0; i < children.size(); i++){
//        		BaseZTreeEntity child = children.get(i);
//        		String icons = "";
//            	if(!StringUtils.isEmpty(child.getIcon())){
//            		icons = " icon:'"+child.getIcon()+"',";
//            	}
//        		if(isHasChecbox){
//        			//如果当前节点下面不存在节点则设置leaf为true 否则设置false
//        			html.append("{name:'" + child.getText() + "',id:'" + child.getId()+ "',checked:"+child.getChecked()+",singleClickExpand:"+child.getSingleClickExpand()+",pId:'"+child.getPId()+"',leaf:"+isLeaf(list,child)+","+icons+"qtip:'"+child.getText()+"',open:"+child.getExpanded()+",tempObject:'"+child.getTempObject()+"',content:'"+child.getContent()+"',integerappend:'"+child.getIntegerappend()+"'");
//        		}else{
//        			//如果当前节点下面不存在节点则设置leaf为true 否则设置false
//        			html.append("{name:'" + child.getText() + "',id:'" + child.getId()+ "',singleClickExpand:"+child.getSingleClickExpand()+",pId:'"+child.getPId()+"',leaf:"+isLeaf(list,child)+","+icons+"qtip:'"+child.getText()+"',open:"+child.getExpanded()+",tempObject:'"+child.getTempObject()+"',content:'"+child.getContent()+"',integerappend:'"+child.getIntegerappend()+"'");
//        		}
//            	html.append(build(list,child,isHasChecbox));
//                if(i < children.size()-1){
//            		html.append("},");
//            	}else{
//            		html.append("}");
//            	}
//            }
//            html.append("]");
//        }
//        return html.toString();
//    }
//    /**
//     * 获取子节点
//     * @param list
//     * @param node
//	 * @param isHasChecbox
//     * @return
//     */
//    private static List<BaseZTreeEntity> getChildren(List<BaseZTreeEntity> list,BaseZTreeEntity node,boolean isHasChecbox){
//        List<BaseZTreeEntity> children = new ArrayList<BaseZTreeEntity>();
//        for(BaseZTreeEntity child : list) {
//        	if (node.getId().equals(child.getPId())) {
//                children.add(child);
//            }
//        }
//        return children;
//    }
//
//    /**
//     * 判断是否存在子叶子
//     * @param list
//     * @param node
//     * @return
//     */
//    public static String isLeaf(List<BaseZTreeEntity> list,BaseZTreeEntity node){
//    	for(BaseZTreeEntity child : list) {
//        	if (node.getId().equals(child.getPId())) {
//        		return "true";
//            }
//        }
//    	return "false";
//    }
}
