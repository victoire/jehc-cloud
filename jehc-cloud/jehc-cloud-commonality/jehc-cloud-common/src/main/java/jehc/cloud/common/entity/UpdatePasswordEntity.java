package jehc.cloud.common.entity;

import lombok.Data;

/**
 * @Desc 修改个人密码参数
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class UpdatePasswordEntity {
    private String accountId;
    private String oldPwd;//原密码
    private String newPwd;//新密码
}
