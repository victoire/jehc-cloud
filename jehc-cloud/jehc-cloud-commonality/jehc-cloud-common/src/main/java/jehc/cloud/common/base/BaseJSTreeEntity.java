package jehc.cloud.common.base;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Desc JsTree通用树实体类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class BaseJSTreeEntity {
    private String id;/**id编号**/
    private String parent;/**父级编号**/
    private String text;/**显示值**/
    private String icon;/**图片**/
    private String qtip;/**提示**/
    private BaseJSTreeAtrrEntity attributes = new BaseJSTreeAtrrEntity();
    private BaseJSTreeStateEntity state = new BaseJSTreeStateEntity();
    private Boolean checked = false;/**是否支持全选**/
    private Boolean singleClickExpand = false;/**单击展开默认否false为双击节点展开 true单击节点展开**/
    private String tempObject;/**临时值**/
    private String content;/**描述**/
    private String integerappend;/**int类型拼接使用可以使用@或逗号隔开**/
    private ArrayList<BaseJSTreeEntity> children = new ArrayList<BaseJSTreeEntity>();/**子节点集合**/

    public List<BaseJSTreeEntity> buildTree(List<BaseJSTreeEntity> list, String parent){
        List<BaseJSTreeEntity> baseJSTreeEntityList=new ArrayList<BaseJSTreeEntity>();
        for(BaseJSTreeEntity baseJSTreeEntity : list) {
            String nodeId = baseJSTreeEntity.getId();
            String pid = baseJSTreeEntity.getParent();
            baseJSTreeEntity.getAttributes().setId(baseJSTreeEntity.getId());
            if (parent.equals(pid)) {
                if("0".equals(pid)){
                    baseJSTreeEntity.setParent(null);
                }
                List<BaseJSTreeEntity> baseJSTreeEntities = buildTree(list, nodeId);
                baseJSTreeEntity.getChildren().addAll(baseJSTreeEntities);
                baseJSTreeEntityList.add(baseJSTreeEntity);
            }
        }
        return baseJSTreeEntityList;
    }

}
