package jehc.cloud.common.entity.index;

import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 资源实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class ResourceEntity  extends BaseEntity {
    private String resources_id;/****/
    private String resources_title;/**名称**/
    private String resources_parentid;/**访问资源地址**/
    private String resources_iconCls;/**图标**/
    private int resources_leaf;/**是否叶子**/
    private String resources_images;/**图片路路径**/
    private int resources_sort;/**排序**/
    private int resources_sys;/**是否是系统菜单1是/0否**/
    private int resources_status;/**状态0可用/1禁用**/
    private String resources_module_id;/**所属子系统模块外键**/
    private String keyid;
    private String resources_url;
    private String sys_mode_id;/**隶属平台编号**/
    private String sysname;/**隶属平台名称**/
    private String sys_mode_icon;/**隶属平台图标**/
    private String sys_mode_url;/**平台地址路径**/
    private int mode_sort;/**模块排序号**/

    private String component_router;/**组件路由**/
    private String component_router_to;/**组件地址即转发**/
    private String component_icon;/**组件图标**/
    private int component_jump_type;/**跳转方式： 0站内 1站外 2 iframe**/
    private int component_hide_menu;/**是否隐藏 0否 1是**/
    private int component_open_new_page;/**是否打开新页面 0是 1否**/
    private String component_open_style;/**激活展开样式（注意存在子菜单时候展开菜单后是否激活样式）**/
}
