package jehc.cloud.common.proxy.shcedule;

import lombok.extern.slf4j.Slf4j;
/**
 * @Desc 检测线程
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class CheckThread extends Thread{
	 private static final int sleepTime = 2000;
	 
	 /**
	  * 默认构造函数
	  * @param threadName
	  */
     public CheckThread(String threadName) {
        this.setName(threadName);
     }
	    
	 public void run() {
         log.info("CheckThread[线程：" + this + "->" + this.getId() + "]");
        //用死循环检测
        for (;;) {
            try {
            	 Thread.sleep(sleepTime);
            }
            catch (Exception e) {
                log.error("CheckThread[" + this + "->" + this.getId() + "]发生异常，线程关闭", e);
            }
        }
    }
}

