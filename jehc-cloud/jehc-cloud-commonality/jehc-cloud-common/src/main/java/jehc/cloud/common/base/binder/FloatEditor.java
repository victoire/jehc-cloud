package jehc.cloud.common.base.binder;
import org.springframework.beans.propertyeditors.PropertiesEditor;

/**
 * @Desc FloatEditor
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class FloatEditor extends PropertiesEditor{
	@Override  
    public void setAsText(String text) throws IllegalArgumentException {  
        if (text == null || text.equals("")) {  
            text = "0";  
        }  
        setValue(Float.parseFloat(text));  
    }  
  
    @Override  
    public String getAsText() {  
        return getValue().toString();  
    } 
}
