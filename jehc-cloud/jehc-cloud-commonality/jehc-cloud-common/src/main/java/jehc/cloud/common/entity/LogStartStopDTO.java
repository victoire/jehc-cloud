package jehc.cloud.common.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
* @Desc 服务器启动与关闭日志
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:28:59
*/
@Data
public class LogStartStopDTO extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**主键**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**启动时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**停止时间**/
	private String error;/**是否出错0正常1错误**/
	private String content;/**加载内容**/
	private String create_id;/**创建人id**/
	private String update_id;/**修改人id**/
	private Integer del_flag;/**删除标记：0正常1已删除**/
}
