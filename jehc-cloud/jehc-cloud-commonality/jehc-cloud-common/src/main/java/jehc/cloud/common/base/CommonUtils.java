package jehc.cloud.common.base;

import jehc.cloud.common.cache.redis.RedisUtil;
import jehc.cloud.common.constant.CacheConstant;
import jehc.cloud.common.entity.*;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.common.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;
/**
 * @Desc CommonUtils
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
public class CommonUtils {

    @Autowired
    RedisUtil redisUtil;

    /**
     * 根据KEY获取平台常量
     * @param key
     * @return
     */
    public ConstantEntity getXtConstantCache(String key) {
        String data = ""+redisUtil.hget(CacheConstant.SYSHASH,CacheConstant.XTCONSTANTCACHE);
        List<ConstantEntity> xtConstantList = JsonUtil.toFList(data,ConstantEntity.class);
        for (int i = 0; i < xtConstantList.size(); i++) {
            ConstantEntity xtConstant = xtConstantList.get(i);
            if (key.equals(xtConstant.getCkey())) {
                return xtConstant;
            }
        }
        return null;
    }

    /**
     * 根据key获取行政区域集合
     * @param key
     * @return
     */
    public List<AreaRegionEntity> getXtAreaRegionCache(String key) {
        String data = ""+redisUtil.hget(CacheConstant.SYSHASH,CacheConstant.XTAREAREGIONCACHE);
        List<AreaRegionEntity> list = JsonUtil.toFList(data,AreaRegionEntity.class);
        List<AreaRegionEntity> arList = new ArrayList<AreaRegionEntity>();
        if(StringUtil.isEmpty(key)){
            for (int i = 0; i < list.size(); i++) {
                AreaRegionEntity xtAreaRegion = list.get(i);
                if ("1".equals(xtAreaRegion.getPARENT_ID())) {
                    arList.add(xtAreaRegion);
                }
            }
        }else{
            for (int i = 0; i < list.size(); i++) {
                AreaRegionEntity xtAreaRegion = list.get(i);
                if (key.equals(""+xtAreaRegion.getPARENT_ID())) {
                    arList.add(xtAreaRegion);
                }
            }
        }
        return arList;
    }

    /**
     * 根据KEY获取平台字典
     * @param key
     * @return
     */
    public List<DataDictionaryEntity> getXtDataDictionaryCache(String key) {
        String data = ""+redisUtil.hget(CacheConstant.SYSHASH,CacheConstant.XTDATADICTIONARYCACHE);
        List<DataDictionaryEntity> xtDataDictionaryList = JsonUtil.toFList(data,DataDictionaryEntity.class);
        List<DataDictionaryEntity> list = new ArrayList<DataDictionaryEntity>();
        String id ="";
        for (int i = 0; i < xtDataDictionaryList.size(); i++) {
            DataDictionaryEntity xtDataDictionary = xtDataDictionaryList.get(i);
            if(key.equals(xtDataDictionary.getXt_data_dictionary_value())){
                id = xtDataDictionary.getXt_data_dictionary_id();
                break;
            }
        }
        for (int i = 0; i < xtDataDictionaryList.size(); i++) {
            DataDictionaryEntity xtDataDictionary = xtDataDictionaryList.get(i);
            if (id.equals(xtDataDictionary.getXt_data_dictionary_pid()) && !"".equals(id)) {
                list.add(xtDataDictionary);
            }
        }
        return list;
    }

    /**
     * 判断IP是否为黑户
     * @param ip
     * @return
     */
    public boolean getXtIpFrozenCache(String ip){
        String data = ""+redisUtil.hget(CacheConstant.SYSHASH,CacheConstant.XTIPFROZENCACHE);
        List<IpFrozenEntity> xtIpFrozenList = JsonUtil.toFList(data,IpFrozenEntity.class);
        for (int i = 0; i < xtIpFrozenList.size(); i++) {
            IpFrozenEntity xtIpFrozen = xtIpFrozenList.get(i);
            if(ip.equals(xtIpFrozen.getXt_ip_frozen_address())){
                return false;
            }
        }
        return true;
    }

    /**
     * 获取公共功能缓存
     * @return
     */
    public String getXtFunctioninfoCommonCache(){
        String data = ""+redisUtil.hget(CacheConstant.SYSHASH,CacheConstant.XTFUNCTIONINFOCOMMONCACHE);
        return data;
    }

    /**
     * 根据KEY获取平台路径
     *
     * @param key
     * @return
     */
    public List<PathEntity> getXtPathCache(String key) {
        String data = ""+redisUtil.hget(CacheConstant.SYSHASH,CacheConstant.XTPATHCACHE);
        List<PathEntity> pathEntities = JsonUtil.toFList(data,PathEntity.class);
        if(null != pathEntities && !pathEntities.isEmpty()){
            List<PathEntity> list = new ArrayList<PathEntity>();
            for (int i = 0; i < pathEntities.size(); i++) {
                PathEntity pathEntity = pathEntities.get(i);
                if (key.equals(pathEntity.getXt_value())) {
                    list.add(pathEntity);
                }
            }
            return list;
        }
        return null;
    }
}
