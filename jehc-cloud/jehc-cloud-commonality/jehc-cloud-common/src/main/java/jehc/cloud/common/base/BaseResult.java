package jehc.cloud.common.base;

import jehc.cloud.common.constant.StatusConstant;
import jehc.cloud.common.util.logger.LogUtil;
import lombok.Data;

/**
 * @Desc BaseResult
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class BaseResult<T> extends LogUtil {
    private int status = StatusConstant.XT_PT_STATUS_VAL_200;
    private String message ="操作成功";
    private Boolean success = true;
    private T data;

    /**
     * constructs a successful result
     */
    public BaseResult () {
        success = true;
    }

    public static BaseResult success(Object data) {
        BaseResult baseResult = new BaseResult();
        baseResult.setStatus(StatusConstant.XT_PT_STATUS_VAL_200);
        baseResult.setMessage("操作成功");
        baseResult.setData(data);
        baseResult.setSuccess(true);
        return baseResult;
    }

    /**
     *
     * @param message
     * @return
     */
    public static BaseResult success(String message) {
        BaseResult baseResult = new BaseResult();
        baseResult.setStatus(StatusConstant.XT_PT_STATUS_VAL_200);
        baseResult.setMessage(message);
        baseResult.setSuccess(true);
        return baseResult;
    }

    /**
     *
     * @return
     */
    public static BaseResult success() {
        BaseResult baseResult = new BaseResult();
        baseResult.setStatus(StatusConstant.XT_PT_STATUS_VAL_200);
        baseResult.setMessage("操作成功");
        baseResult.setSuccess(true);
        return baseResult;
    }

    /**
     *
     * @param data
     * @return
     */
    public static BaseResult fail(Object data) {
        BaseResult baseResult = new BaseResult();
        baseResult.setStatus(StatusConstant.XT_PT_STATUS_VAL_500);
        baseResult.setMessage("操作失败");
        baseResult.setData(data);
        baseResult.setSuccess(false);
        return baseResult;
    }

    /**
     *
     * @param message
     * @return
     */
    public static BaseResult fail(String message) {
        BaseResult baseResult = new BaseResult();
        baseResult.setStatus(StatusConstant.XT_PT_STATUS_VAL_500);
        baseResult.setMessage(message);
        baseResult.setSuccess(false);
        return baseResult;
    }

    /**
     *
     * @return
     */
    public static BaseResult fail() {
        BaseResult baseResult = new BaseResult();
        baseResult.setStatus(StatusConstant.XT_PT_STATUS_VAL_500);
        baseResult.setMessage("操作失败");
        baseResult.setSuccess(false);
        return baseResult;
    }

    /**
     *
     * @param data
     */
    public BaseResult(T data){
        this.success = true;
        this.data = data;
    }

    /**
     *
     * @param message
     * @param success
     */
    public  BaseResult(String message,Boolean success){
        this.message = message;
        this.success = success;
    }

    /**
     *
     * @param message
     * @param success
     * @param data
     */
    public  BaseResult(String message,Boolean success,T data){
        this.message = message;
        this.success = success;
        this.data = data;
    }

    /**
     *
     * @param status
     * @param message
     * @param success
     */
    public  BaseResult(int status,String message,Boolean success){
        this.status = status;
        this.message = message;
        this.success = success;
    }

    /**
     *
     * @param status
     * @param message
     * @param success
     * @param data
     */
    public  BaseResult(int status,String message,Boolean success,T data){
        this.status = status;
        this.message = message;
        this.success = success;
        this.data = data;
    }
}
