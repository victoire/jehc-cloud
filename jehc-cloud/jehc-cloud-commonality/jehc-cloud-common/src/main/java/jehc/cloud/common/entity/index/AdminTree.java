package jehc.cloud.common.entity.index;

import jehc.cloud.common.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * @Desc AdminTree
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class AdminTree {
    private List<ResourceEntity> nodes;

    public AdminTree(List<ResourceEntity> nodes) {
        this.nodes = nodes;
    }

    public String buildTree() {
        StringBuffer html = new StringBuffer();
        for(ResourceEntity node:nodes){
            if ("0".equals(node.getResources_parentid()) && !StringUtil.isEmpty(node.getKeyid())&&  node.getKeyid().equals("jEhcDevModule")) {
                html.append("<li class=\"m-menu__item  m-menu__item--submenu m-menu__item--rel\" id=\"menu"+node.getResources_id()+"\" m-menu-submenu-toggle=\"click\" aria-haspopup=\"true\">");
                //根目录开始
                if(node.getResources_leaf() == 0){
                    html.append("<a href=\"javascript:;\" class=\"m-menu__link m-menu__toggle\">");
                }else{
                    //一级菜单如果没有子级则可以连接
                    html.append("<a href='../../"+node.getResources_url()+"' class=\"m-menu__link m-menu__toggle J_menuItem\" data-index='"+node.getResources_id()+"' rootId='"+node.getResources_id()+"'>");
                }
//					html.append("<i class=\"m-menu__hor-arrow "+node.getXt_menuinfo_iconCls()+"\"></i>");
                html.append("<span class=\"m-menu__link-text\">"+node.getResources_title()+"</span>");
                html.append("<i class=\"m-menu__hor-arrow la la-angle-down\"></i>");
                html.append("<i class=\"m-menu__ver-arrow la la-angle-right\"></i>");
                html.append("</a>");
                //根目录结束
                html.append(build(node,node.getResources_id()));
                html.append("</li>");
            }
        }
        return html.toString();
    }

    private String build(ResourceEntity node,String rootId) {
        StringBuffer html = new StringBuffer();
        List<ResourceEntity> children = getChildren(node);
        if (!children.isEmpty()) {
            //遍历子级菜单----1将父级信息作为子级标题带入开始
//			m-menu__submenu m-menu__submenu--classic m-menu__submenu--left
            if("0".equals(node.getResources_parentid()) || StringUtils.isEmpty(node.getResources_parentid())){
                html.append("<div class=\"m-menu__submenu m-menu__submenu--classic m-menu__submenu--left\">");
                //遍历子级菜单----1将父级信息作为子级标题带入开始
                html.append("<span class=\"m-menu__arrow m-menu__arrow--adjust\"></span>");
            }else{
                html.append("<div class=\"m-menu__submenu m-menu__submenu--classic m-menu__submenu--right\">");
                //遍历子级菜单----1将父级信息作为子级标题带入开始
                html.append("<span class=\"m-menu__arrow \"></span>");
            }
            //遍历子级菜单----1将父级信息作为子级标题带入开始
            html.append("<ul class=\"m-menu__subnav\">");
//					//遍历子级菜单----1将父级信息作为子级标题带入开始
//					html.append("<li class=\"m-menu__item  m-menu__item--submenu\" m-menu-submenu-toggle=\"hover\" m-menu-link-redirect=\"1\" aria-haspopup=\"true\" >");
//						html.append("<span class=\"m-menu__link\">");
//							//此处为父级标题名称
//							html.append("<span class=\"m-menu__link-text\">"+node.getXt_menuinfo_title()+"</span>");
//						html.append("</span>");
//					//遍历子级菜单----1将父级信息作为子级标题带入结束
            html.append("</li>");
            //遍历子目录时（父级除外的下级菜单）
            for (ResourceEntity child:children) {
                boolean existChild=false;
                String existChildText = "";
                if(child.getResources_leaf()==0){
                    existChild = true;
                    existChildText = " class=\"m-menu__item m-menu__item--submenu\" aria-haspopup=\"true\"  m-menu-submenu-toggle=\"hover\" m-menu-link-redirect=\"1\"";
                }else{
                    existChildText = " class=\"m-menu__item \" aria-haspopup=\"true\"  m-menu-link-redirect=\"1\"";
                }
                html.append("<li "+existChildText+" id=\"menu"+child.getResources_id()+"\">");
                if(child.getResources_leaf()!=0){
                    //不存在下级菜单
                    html.append("<a  href='../../"+child.getResources_url()+"' class=\"m-menu__link m-menu__toggle  J_menuItem\" data-index='"+child.getResources_id()+"' rootId='"+rootId+"' idBu='"+idBu(child.getResources_parentid())+"'>");
                }else{
                    //存在下级菜单
                    html.append("<a href=\"javascript:;\" class=\"m-menu__link m-menu__toggle\">");
                }
                //a标签中内容i-span-text
                html.append("<i class=\"m-menu__link-icon "+child.getResources_iconCls()+"\">");
                html.append("<span></span>");
                html.append("</i>");
                html.append("<span class=\"m-menu__link-text\">");
                html.append(""+child.getResources_title()+"");//菜单标题
                html.append("</span>");
                if(existChild){
                    html.append("<i class=\"m-menu__hor-arrow la la-angle-right\"></i>");
                    html.append("<i class=\"m-menu__ver-arrow la la-angle-right\"></i>");
                }
                html.append("</a>");
                //递归
                String buildSbf = build(child,rootId);
                html.append(buildSbf);
                html.append("</li>");
            }
            html.append("</ul>");
            html.append("</div>");
        }
        return html.toString();
    }

    private List<ResourceEntity> getChildren(ResourceEntity node) {
        List<ResourceEntity> children = new ArrayList<ResourceEntity>();
        for (ResourceEntity child:nodes) {
            if (child.getResources_parentid().equals(node.getResources_id())) {
                children.add(child);
            }
        }
        return children;
    }

    private String idBu(String id){
        StringBuffer ids = new StringBuffer();
        return ids.append(idList(id)).toString();
    }
    private String idList(String id){
        StringBuffer ids = new StringBuffer();
        for(ResourceEntity node: nodes){
            if(id.equals(node.getResources_id())){
                if(null != node.getResources_parentid() && !"0".equals(node.getResources_parentid())){
                    ids.append(node.getResources_id()+",");
                    ids.append(idList(node.getResources_parentid()));
                }else{
                    ids.append(node.getResources_id());
                }
            }
        }
        return ids.toString();
    }
}
