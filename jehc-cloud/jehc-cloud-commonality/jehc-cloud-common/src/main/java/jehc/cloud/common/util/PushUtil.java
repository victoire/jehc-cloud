package jehc.cloud.common.util;

import jehc.cloud.common.base.BaseResult;

/**
 * @Desc 统一推送列
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface PushUtil {
    /**
     * 推送消息
     * @param message
     * @return
     */
    BaseResult send(String message);

    /**
     * 推送消息
     * @param client
     * @param message
     * @return
     */
    BaseResult send(String client,String message);
}
