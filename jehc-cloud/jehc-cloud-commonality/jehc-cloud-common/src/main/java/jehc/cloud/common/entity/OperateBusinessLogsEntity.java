package jehc.cloud.common.entity;

import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 业务日志
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OperateBusinessLogsEntity  extends BaseEntity{
    private String xt_operate_b_logs_id;/**平台业务操作日志ID**/
    private String xt_operate_b_logsTime;/**操作时间**/
    private String xt_userinfo_id;/**操作人ID外键**/
    private String xt_operate_b_logsModules;/**模块**/
    private String xt_operate_b_logsMethod;/**执行方法**/
    private String xt_operate_b_logsMethodPar;/**方法参数**/
    private String xt_operate_b_logsResult;/**执行的结果**/

}
