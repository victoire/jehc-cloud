package jehc.cloud.common.entity;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 调度器日志
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class QuartzLogEntity extends BaseEntity{
	private String xt_quartz_log_id;/**主键**/
	private String xt_quartz_log_name;/**名称**/
	private String xt_quartz_log_key;/**调度键**/
	private String xt_quartz_log_content;/**调度内容**/
	private String xt_quartz_log_ctime;/**开始时间**/
	private String xt_quartz_log_etime;/**结束时间**/
	private String xt_userinfo_id;/**操作人编号**/
	private String xt_quartz_log_flag;/**运行标识**/
}
