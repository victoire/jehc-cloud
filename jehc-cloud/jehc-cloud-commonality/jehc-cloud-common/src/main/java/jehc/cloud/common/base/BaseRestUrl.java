package jehc.cloud.common.base;
/**
 * @Desc 基础服务URL地址统一配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class BaseRestUrl extends InitBean {
//    private static final String REST_URL_PREFIX_SYS = "http://JEHC-CLOUD-SYS";
//
//    private static final String REST_URL_PREFIX_SOLR = "http://JEHC-CLOUD-SOLR";
//
//    private static final String REST_URL_PREFIX_WORKFLOW = "http://JEHC-CLOUD-WORKFLOW";
//
//    private static final String REST_URL_PREFIX_CMS = "http://JEHC-CLOUD-CMS";
//
//    private static final String REST_URL_PREFIX_OA = "http://JEHC-CLOUD-OA";
//
//    private static final String REST_URL_PREFIX_CRM = "http://JEHC-CLOUD-CRM";
//
//    private static final String REST_URL_PREFIX_OAUTH = "http://JEHC-CLOUD-OAUTH";
//
//    private static final String REST_URL_PREFIX_PAYMENT = "http://JEHC-CLOUD-PAYMENT";
//
//    private static final String REST_URL_PREFIX_WEIXIN = "http://JEHC-CLOUD-WX";
//
//    private static final String REST_URL_PREFIX_JOB = "http://JEHC-CLOUD-JOB";
//
//    private static final String REST_URL_PREFIX_IOT_MONITOR = "http://JEHC-CLOUD-IOT-MONITOR";
//
//    private static final String REST_URL_PREFIX_IOT_LIVE = "http://JEHC-CLOUD-IOT-LIVE";
//
//    private static final String REST_URL_PREFIX_LOG = "http://JEHC-CLOUD-LOG";
//
//    private static final String REST_URL_PREFIX_REPORT = "http://JEHC-CLOUD-REPORT";
//
//    private static final String REST_URL_PREFIX_MEDICAL = "http://JEHC-CLOUD-MEDICAL";
//
//    private static final String REST_URL_PREFIX_OPERATION = "http://JEHC-CLOUD-OPERATION";


    private static final String REST_URL_PREFIX_SYS = "http://jehc-cloud-sys";

    private static final String REST_URL_PREFIX_SOLR = "http://jehc-cloud-solr";

    private static final String REST_URL_PREFIX_WORKFLOW = "http://jehc-cloud-workflow";

    private static final String REST_URL_PREFIX_CMS = "http://jehc-cloud-cms";

    private static final String REST_URL_PREFIX_OA = "http://jehc-cloud-oa";

    private static final String REST_URL_PREFIX_CRM = "http://jehc-cloud-crm";

    private static final String REST_URL_PREFIX_OAUTH = "http://jehc-cloud-oauth";

    private static final String REST_URL_PREFIX_PAYMENT = "http://jehc-cloud-payment";

    private static final String REST_URL_PREFIX_WEIXIN = "http://jehc-cloud-wx";

    private static final String REST_URL_PREFIX_JOB = "http://jehc-cloud-job";

    private static final String REST_URL_PREFIX_IOT_MONITOR = "http://jehc-cloud-iot-monitor";

    private static final String REST_URL_PREFIX_IOT_LIVE = "http://jehc-cloud-iot-live";

    private static final String REST_URL_PREFIX_LOG = "http://jehc-cloud-log";

    private static final String REST_URL_PREFIX_REPORT = "http://jehc-cloud-report";

    private static final String REST_URL_PREFIX_MEDICAL = "http://jehc-cloud-medical";

    private static final String REST_URL_PREFIX_OPERATION = "http://jehc-cloud-operation";

    private static final String REST_URL_PREFIX_OMP = "http://jehc-cloud-omp";

    private static final String REST_URL_PREFIX_FILE = "http://jehc-cloud-file";

    /**
     * 平台地址
     * @return
     */
    public String restSysURL(){
        return REST_URL_PREFIX_SYS;
    }

    /**
     * 全文检索地址
     * @return
     */
    public String restSolrURL(){
        return REST_URL_PREFIX_SOLR;
    }

    /**
     * 流程地址
     * @return
     */
    public String restWorkflowURL(){
        return REST_URL_PREFIX_WORKFLOW;
    }


    /**
     * cms 地址
     * @return
     */
    public String restCmsURL(){
        return REST_URL_PREFIX_CMS;
    }


    /**
     * OA 地址
     * @return
     */
    public String resOaURL(){
        return REST_URL_PREFIX_OA;
    }

    /**
     * CRM 地址
     * @return
     */
    public String resCrmURL(){
        return REST_URL_PREFIX_CRM;
    }

    /**
     * 授权中心
     * @return
     */
    public String restOauthURL(){
        return REST_URL_PREFIX_OAUTH;
    }


    /**
     * 支付中心
     * @return
     */
    public String restPaymentUrl (){
        return REST_URL_PREFIX_PAYMENT;
    }

    /**
     * 微信中心
     * @return
     */
    public String restWeixinUrl (){
        return REST_URL_PREFIX_WEIXIN;
    }

    /**
     * 调度中心
     * @return
     */
    public String restJobUrl (){
        return REST_URL_PREFIX_JOB;
    }

    /**
     * 监控中心
     * @return
     */
    public String restIOTMonitorUrl(){
        return REST_URL_PREFIX_IOT_MONITOR;
    }

    /**
     * 日志中心
     * @return
     */
    public String restLogUrl(){
        return REST_URL_PREFIX_LOG;
    }


    /**
     * 直播平台
     * @return
     */
    public String restIOTLiveUrl(){
        return REST_URL_PREFIX_IOT_LIVE;
    }

    /**
     * 报表中心
     * @return
     */
    public String restReportUrl(){
        return REST_URL_PREFIX_REPORT;
    }

    /**
     * 病例云中心
     * @return
     */
    public String restMedicalUrl(){
        return REST_URL_PREFIX_MEDICAL;
    }

    /**
     * 运维平台
     * @return
     */
    public String restOperationUrl(){
        return REST_URL_PREFIX_OPERATION;
    }

    /**
     * 运管平台
     * @return
     */
    public String restOMPUrl(){
        return REST_URL_PREFIX_OMP;
    }

    /**
     * 附件平台
     * @return
     */
    public String restFileUrl(){
        return REST_URL_PREFIX_FILE;
    }

}
