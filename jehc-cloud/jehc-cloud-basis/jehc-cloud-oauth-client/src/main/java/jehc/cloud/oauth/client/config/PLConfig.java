package jehc.cloud.oauth.client.config;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 * 日志SDK 配置
 * </p>
 *
 * @author dengcj
 * @since 2023-04-19 10:21:25
 */
@Configuration
@Data
public class PLConfig {
    @Value("${jehc.pl.enabled:true}")
    private Boolean enabled;//SDK是否禁用 true开启 false关闭

    @Value("${jehc.pl.arts:true}")
    private Boolean arts;//SDK是否允许实时发送（Allow real-time sending）

    @Value("${jehc.pl.path:}")
    private String path;//日志存储目录
}
