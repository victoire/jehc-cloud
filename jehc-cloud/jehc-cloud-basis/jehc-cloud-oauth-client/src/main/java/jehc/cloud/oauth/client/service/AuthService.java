package jehc.cloud.oauth.client.service;

import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.entity.InputEntity;

import javax.servlet.http.HttpServletRequest;
/**
 * @Desc 客户端鉴权
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface AuthService {
    /**
     * 验证权限
     * @param inputEntity
     * @return
     */
    BaseResult oauth(HttpServletRequest request,InputEntity inputEntity);
}
