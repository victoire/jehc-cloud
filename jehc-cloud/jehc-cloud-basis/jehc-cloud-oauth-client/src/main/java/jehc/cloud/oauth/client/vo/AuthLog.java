package jehc.cloud.oauth.client.vo;

import lombok.Data;

/**
 * @Desc 授权耗时日志传输对象
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class AuthLog {
    String uri;//访问地址
    Long startTime;//开始时间
    Long endTime;//结束时间
    Long takeUpTime;//耗时
    String protocol;//获取请求协议版本
    String scheme;//获取请求使用的协议名
    String serverName;//获取请求URL上的主机名（内网未转发时，一般为项目部署服务器主机ip）
    String serverPort;//获取请求URL上的端口号
    String method;//获取请求的方法
    String localPort;//获取最终接收请求的端口
    String localName;//获取最终接收请求的主机
    String remoteAddr;//获取发送请求的客户端地址（如果经过Apache等转发，则不是真实的DCN网地址）
    String remoteHost;//获取发送请求的客户端主机名
    String remotePort;//获取发送请求的客户端端口
}
