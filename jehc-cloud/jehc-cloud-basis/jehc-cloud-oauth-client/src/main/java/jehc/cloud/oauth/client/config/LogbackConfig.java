package jehc.cloud.oauth.client.config;

import lombok.Data;

/**
 * <p>
 * logback初始化配置
 * </p>
 *
 * @author dengcj
 * @since 2023-04-13
 */
@Data
public class LogbackConfig {

    private String fileName;//日志根文件名

    private String filePath;//文件存储路径

    private String fileFull;//日志全部文件路径包含路径+名称（如：${logFilePath}/${logFileName}-${logIpFile}.log）

    private String pattern;//日志产生数据规则（如：%msg%n）

    private String fileNamePattern;//每日产生的日志文件命名格式规则（如：${logFilePath}/${logFileName}-${logIpFile}@%d.%i.log）

    /**
     * 生成文件规则：
     * 每天一个日志文件，当天的日志文件超过10MB时，
     * 生成新的日志文件，当天的日志文件数量超过totalSizeCap/maxFileSize，日志文件就会被回滚覆盖。
     */
    private String maxFileSize = "1MB";//最大日志文件大小

    private String totalSizeCap = "100GB";//总大小限制

    private int maxHistory = 30;//设置最大历史记录为30天

    private String contentPattern;//内容格式化规则

    public LogbackConfig(){

    }
}
