package jehc.cloud.oauth.client.vo;

import lombok.Data;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class Token {
    private String updateTimes;//最后更新时间
    private Long times=0L;//活跃次数
    private Boolean success = true;//是否成功
    private String clientId;//客户端id（账号id）
    private Boolean keepAlive = false;//是否活跃 默认false

    public Token(){

    }
    public Token(String clientId){
        this.clientId =clientId;
    }
    public Token(String clientId,Boolean keepAlive){
        this.clientId =clientId;
        this.keepAlive = keepAlive;
    }
    public Token(String updateTimes,Long times,boolean success){
        this.updateTimes = updateTimes;
        this.times = times;
        this.success =success;
    }

    public Token(String updateTimes, Long times, boolean success,String clientId){
        this.updateTimes = updateTimes;
        this.times = times;
        this.success =success;
        this.clientId = clientId;
    }
}
