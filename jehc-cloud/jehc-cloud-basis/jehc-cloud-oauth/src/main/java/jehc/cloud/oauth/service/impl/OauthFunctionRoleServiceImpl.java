package jehc.cloud.oauth.service.impl;
import java.util.List;
import java.util.Map;

import jehc.cloud.oauth.service.OauthFunctionRoleService;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.oauth.dao.OauthFunctionRoleDao;
import jehc.cloud.oauth.model.OauthFunctionRole;
/**
 * @Desc 授权中心功能对角色
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("oauthFunctionRoleService")
public class OauthFunctionRoleServiceImpl extends BaseService implements OauthFunctionRoleService {
	@Autowired
	private OauthFunctionRoleDao oauthFunctionRoleDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<OauthFunctionRole> getOauthFunctionRoleListByCondition(Map<String,Object> condition){
		try{
			return oauthFunctionRoleDao.getOauthFunctionRoleListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param oauthFunctionRole 
	* @return
	*/
	public int addOauthFunctionRole(OauthFunctionRole oauthFunctionRole){
		int i = 0;
		try {
			i = oauthFunctionRoleDao.addOauthFunctionRole(oauthFunctionRole);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}


}
