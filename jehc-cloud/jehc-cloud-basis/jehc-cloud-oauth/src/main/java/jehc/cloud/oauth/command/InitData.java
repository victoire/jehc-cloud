package jehc.cloud.oauth.command;

import jehc.cloud.common.cache.redis.RedisUtil;
import jehc.cloud.common.constant.CacheConstant;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.oauth.model.OauthKeyInfo;
import jehc.cloud.oauth.model.OauthSysMode;
import jehc.cloud.oauth.service.OauthKeyInfoService;
import jehc.cloud.oauth.service.OauthSysModeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import java.util.HashMap;
import java.util.List;

/**
 * @Desc 初始化数据
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class InitData implements CommandLineRunner {
    @Autowired
    OauthKeyInfoService oauthKeyInfoService;

    @Autowired
    OauthSysModeService oauthSysModeService;

    @Autowired
    private RedisUtil redisUtil;

    /**
     *
     */
    private void init(){
        List<OauthKeyInfo> list = oauthKeyInfoService.getOauthKeyInfoListByCondition(new HashMap<>());
        for(OauthKeyInfo oauthKeyInfo:list){
            redisUtil.hset(CacheConstant.OAUTH_KEY,oauthKeyInfo.getKey_name(), JsonUtil.toFastJson(oauthKeyInfo));
        }

        List<OauthSysMode> oauthSysModeList = oauthSysModeService.getOauthSysModeListByCondition(new HashMap<>());
        redisUtil.set(CacheConstant.OAUTH_SYS_MODE,JsonUtil.toFastJson(oauthSysModeList));
    }

    @Override
    public void run(String... args) throws Exception {
        try {
            init();
        } catch (Exception e) {

        }
    }
}
