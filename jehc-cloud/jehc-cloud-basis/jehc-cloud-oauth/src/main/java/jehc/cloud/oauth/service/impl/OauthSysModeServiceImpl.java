package jehc.cloud.oauth.service.impl;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jehc.cloud.common.constant.CacheConstant;
import jehc.cloud.common.session.HttpSessionUtils;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.oauth.model.OauthKeyInfo;
import jehc.cloud.oauth.service.OauthSysModeService;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.oauth.dao.OauthSysModeDao;
import jehc.cloud.oauth.model.OauthSysMode;

import javax.servlet.http.HttpServletRequest;
/**
 * @Desc 授权中心子系统标记
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("oauthSysModeService")
public class OauthSysModeServiceImpl extends BaseService implements OauthSysModeService {
	@Autowired
	private OauthSysModeDao oauthSysModeDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<OauthSysMode> getOauthSysModeListByCondition(Map<String,Object> condition){
		try{
			return oauthSysModeDao.getOauthSysModeListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param sys_mode_id 
	* @return
	*/
	public OauthSysMode getOauthSysModeById(String sys_mode_id){
		try{
			OauthSysMode oauthSysMode = oauthSysModeDao.getOauthSysModeById(sys_mode_id);
			return oauthSysMode;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param oauthSysMode 
	* @return
	*/
	public int addOauthSysMode(OauthSysMode oauthSysMode){
		int i = 0;
		try {
			oauthSysMode.setCreate_time(getDate());
			oauthSysMode.setCreate_id(getXtUid());
			if(!StringUtil.isEmpty(oauthSysMode.getKey_info_id())){
				Map<String,Object> map = new HashMap<>();
				map.put("key_info_id",oauthSysMode.getKey_info_id());
				if(!oauthSysModeDao.getOauthSysModeListByCondition(map).isEmpty()){
					throw new ExceptionUtil("秘钥已被其他产品线使用");
				}
			}
			i = oauthSysModeDao.addOauthSysMode(oauthSysMode);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param oauthSysMode 
	* @return
	*/
	public int updateOauthSysMode(OauthSysMode oauthSysMode){
		int i = 0;
		try {
			oauthSysMode.setUpdate_id(getXtUid());
			oauthSysMode.setUpdate_time(getDate());
			String oldKeyInfoId = oauthSysModeDao.getOauthSysModeById(oauthSysMode.getSys_mode_id()).getKey_info_id();
			if(!StringUtil.isEmpty(oauthSysMode.getKey_info_id()) && !oauthSysMode.getKey_info_id().equals(oldKeyInfoId)){
				Map<String,Object> map = new HashMap<>();
				map.put("key_info_id",oauthSysMode.getKey_info_id());
				if(!oauthSysModeDao.getOauthSysModeListByCondition(map).isEmpty()){
					throw new ExceptionUtil("秘钥已被其他产品线使用");
				}
			}
			i = oauthSysModeDao.updateOauthSysMode(oauthSysMode);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delOauthSysMode(Map<String,Object> condition){
		int i = 0;
		try {
			i = oauthSysModeDao.delOauthSysMode(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	@Autowired
	HttpSessionUtils httpSessionUtils;

	/**
	 * 获取SysModeId
	 * @param request
	 * @return
	 */
	public String getSysModeId(HttpServletRequest request){
		String key = null;
		String pass = null;
		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String k = (String) headerNames.nextElement();
			if(k.toLowerCase().equals(CacheConstant.JEHC_CLOUD_KEY.toLowerCase())){
				key = request.getHeader(k);
			}
			if(k.toLowerCase().equals(CacheConstant.JEHC_CLOUD_SECURITY.toLowerCase())){
				pass = request.getHeader(k);
			}
		}
		if(StringUtil.isEmpty(key)){
			return null;
		}
		if(StringUtil.isEmpty(pass)){
			return null;
		}
		String oauthKeyInfoJson = httpSessionUtils.getHashAttribute(CacheConstant.OAUTH_KEY,key);
		OauthKeyInfo oauthKeyInfo = JsonUtil.fromFJson(oauthKeyInfoJson,OauthKeyInfo.class);
		String key_pass = oauthKeyInfo.getKey_pass();
		if(StringUtil.isEmpty(key_pass)){
			return null;
		}
		if(!pass.equals(key_pass)){
			return null;
		}

		String oauthSysModeJson = httpSessionUtils.getAttribute(CacheConstant.OAUTH_SYS_MODE);
		if(StringUtil.isEmpty(oauthSysModeJson)){
			return null;
		}
		List<OauthSysMode> oauthSysModeList = JsonUtil.toFList(oauthSysModeJson,OauthSysMode.class);
		for(OauthSysMode oauthSysMode:oauthSysModeList){
			if(oauthKeyInfo.getKey_info_id().equals(oauthSysMode.getKey_info_id())){
				return oauthSysMode.getSys_mode_id();
			}
		}
		return null;
	}
}
