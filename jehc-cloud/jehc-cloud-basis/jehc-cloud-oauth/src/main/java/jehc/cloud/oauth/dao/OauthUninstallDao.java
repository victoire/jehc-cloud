package jehc.cloud.oauth.dao;

import java.util.Map;

/**
 * @Desc 卸载模块
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthUninstallDao {

    /**
     * 根据id删除子系统
     * @param sys_mode_id
     * @return
     */
    int delOauthSysModeById(String sys_mode_id);

    /**
     * 删除模块
     * @param sys_mode_id
     * @return
     */
    int delOauthSysModulesByModeId(String sys_mode_id);

    /**
     * 删除菜单
     * @param map
     * @return
     */
    int delOauthResourcesByModuleId(Map<String, Object> map);

    /**
     * 删除功能
     * @param map
     * @return
     */
    int delOauthFunctionInfoByMenuId(Map<String, Object> map);

    /**
     * 删除管理员拥有系统范围
     * @param sysmode_id
     * @return
     */
    int delOauthAdminSysByModeId(String sysmode_id);

    /**
     * 删除授权中心角色模块
     * @param sysmode_id
     * @return
     */
    int delOauthRoleByModeId(String sysmode_id);

    /**
     * 删除授权中心功能对角色
     * @param map
     * @return
     */
    int delOauthFunctionRoleByMenuId(Map<String, Object> map);

    /**
     * 删除授权中心资源对角色
     * @param map
     * @return
     */
    int delOauthResourcesRoleByMenuId(Map<String, Object> map);

    /**
     * 删除账号类型
     * @return
     */
    int delOauthRAccountTypeByModeId(String sys_mode_id);
}
