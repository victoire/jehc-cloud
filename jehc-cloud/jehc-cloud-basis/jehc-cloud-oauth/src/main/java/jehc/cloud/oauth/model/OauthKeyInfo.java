package jehc.cloud.oauth.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Desc 授权中心密钥
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthKeyInfo extends BaseEntity{
	private String key_info_id;/**主键**/
	private String title;/**标题**/
	private String key_name;/**名称**/
	private String key_pass;/**秘钥**/
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	private Date key_exp_date;/**有效期**/
	private int isUseExpDate;/**是否采用有效期默认False**/
	private int status;/**状态 正常/冻结/禁用**/
}
