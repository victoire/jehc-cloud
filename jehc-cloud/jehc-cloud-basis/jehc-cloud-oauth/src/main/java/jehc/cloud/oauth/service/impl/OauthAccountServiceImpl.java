package jehc.cloud.oauth.service.impl;

import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.constant.CacheConstant;
import jehc.cloud.common.entity.ConstantEntity;
import jehc.cloud.common.entity.UpdatePasswordEntity;
import jehc.cloud.common.entity.UserParamInfo;
import jehc.cloud.common.entity.UserinfoEntity;
import jehc.cloud.common.util.*;
import jehc.cloud.log.client.service.LogsUtil;
import jehc.cloud.oauth.dao.OauthAccountDao;
import jehc.cloud.oauth.model.OauthAccount;
import jehc.cloud.oauth.model.OauthAccountType;
import jehc.cloud.oauth.model.OauthRole;
import jehc.cloud.oauth.service.*;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.oauth.util.OauthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @Desc 授权中心账户
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
@Slf4j
public class OauthAccountServiceImpl extends BaseService implements OauthAccountService {
    @Autowired
    OauthAccountDao oauthAccountDao;

    @Autowired
    OauthRoleService oauthRoleService;

    @Autowired
    OauthAccountTypeService oauthAccountTypeService;

    @Autowired
    OauthAccountRoleService oauthAccountRoleService;

    @Autowired
    OauthUtil oauthUtil;

    @Autowired
    RestTemplateUtil restTemplateUtil;

    @Autowired
    LogsUtil logsUtil;

    /**
     * 分页
     * @param condition
     * @return
     */
    public List<OauthAccount> getOauthAccountList(Map<String,Object> condition){
        return oauthAccountDao.getOauthAccountList(condition);
    }
    /**
     * 查询对象
     * @param xt_account_id
     * @return
     */
    public OauthAccount getOauthAccountById(String xt_account_id){
        return oauthAccountDao.getOauthAccountById(xt_account_id);
    }

    /**
     * 登录
     * @param condition
     * @return
     */
    public OauthAccount login(Map<String,Object> condition){
        if(null == condition.get("account")){
            throw new ExceptionUtil("用户名为空");
        }
        if(null == condition.get("password")){
            throw new ExceptionUtil("密码为空");
        }
        return oauthAccountDao.login(condition);
    }

    /**
     * 添加
     * @param oauthAccount
     * @return
     */
    public int addOauthAccount(OauthAccount oauthAccount){
        int i = 0;
        try {
            if(StringUtil.isEmpty(oauthAccount.getAccount())){
                throw new ExceptionUtil("未能获取到登录名");
            }
            OauthAccount account = oauthAccountDao.getSingleOauthAccount(oauthAccount.getAccount());
            if(null != account){
                throw new ExceptionUtil("登录名已存在！");
            }
            MD5 md5 = new MD5();
            oauthAccount.setPassword(md5.getMD5ofStr(CacheConstant.JEHC_CLOUD_USER_DEFAULT_PWD));
            i = oauthAccountDao.addOauthAccount(oauthAccount);
        } catch (Exception e) {
            i = 0;
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
        return i;
    }
    /**
     * 修改（根据动态条件）
     * @param oauthAccount
     * @return
     */
    public int updateOauthAccount(OauthAccount oauthAccount){
        int i = 0;
        try {
            //如果账户存在 则判断是否重复
            if(!StringUtil.isEmpty(oauthAccount.getAccount())){
                //获取原登录名
                OauthAccount account = oauthAccountDao.getOauthAccountById(oauthAccount.getAccount_id());
                if(null != account){
                    if(!account.getAccount().equals(oauthAccount.getAccount())){ //如果登录名发生变化 则需要验证
                        if(null != oauthAccountDao.getSingleOauthAccount(oauthAccount.getAccount())){
                            throw new ExceptionUtil("登录名已存在！");
                        }
                    }
                    i = oauthAccountDao.updateOauthAccount(oauthAccount);
                    doAccountType(account,oauthAccount.getAccount_type_id());//处理类型变更逻辑
                }
            }
        } catch (Exception e) {
            i = 0;
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
        return i;
    }

    /**
     * 冻结账户
     * @param oauthAccount
     * @return
     */
    public int freezeAccount(OauthAccount oauthAccount){
        int i = 0;
        try {
            i = oauthAccountDao.freezeAccount(oauthAccount);
        } catch (Exception e) {
            i = 0;
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
        return i;
    }

    /**
     * 查询账户集合
     * @param condition
     * @return
     */
    public List<OauthAccount> infoList(Map<String,Object> condition){
        return oauthAccountDao.infoList(condition);
    }


    /**
     * 处理账号类型
     * @param oauthAccount
     * @param new_account_type_id
     */
    private void doAccountType(OauthAccount oauthAccount,String new_account_type_id){
        //找出原账户类型在新账号类型中 是否减少元素，如果减少元素 则删除掉该用户原分配的角色
        List<String> changeList = new ArrayList<>();
        String old_ccount_type_id = oauthAccount.getAccount_type_id();
        if(!StringUtil.isEmpty(old_ccount_type_id)){
            String[] old_account_type_idList = old_ccount_type_id.split(",");
            if(null != old_account_type_idList){
                String[] new_account_type_idList = new_account_type_id.split(",");
                if(!StringUtil.isEmpty(new_account_type_id)){//如果新类型中存在记录 则判断老类型中数据在新的中是否减少 如果减少则为变更数据
                    for(int i = 0; i < old_account_type_idList.length; i++){
                        String oldVal = old_account_type_idList[i];
                        int existNumber = 0;
                        for(int j=0; j< new_account_type_idList.length;j++){
                            String newVal = new_account_type_idList[j];
                            if(!oldVal.equals(newVal)){
                                existNumber++;
                            }
                        }
                        if(existNumber == new_account_type_idList.length){
                            changeList.add(old_account_type_idList[i]);
                        }
                    }
                }else{
                    //如果新类型中为空 则返回变更数据为所有原类型
                    for(int i=0; i< old_account_type_idList.length;i++){
                        changeList.add(old_account_type_idList[i]);
                    }
                }
            }
        }
        doAccountTypeChange(oauthAccount,changeList);//处理类型
    }


    /**
     * 处理角色中该用户类型
     * @param oauthAccount
     * @param changeList
     */
    public void doAccountTypeChange(OauthAccount oauthAccount,List<String> changeList){
        if(!CollectionUtils.isEmpty(changeList)){
            StringBuilder stringBuilder = new StringBuilder();
            for(String account_type_id:changeList){
                if(stringBuilder.length() == 0){
                    stringBuilder.append(account_type_id);
                }else{
                    stringBuilder.append(","+account_type_id);
                }
            }
            //根据类型查找所有子系统
            Map<String,Object> condition = new HashMap<>();
            condition.put("account_type_id",stringBuilder.toString());
            List<OauthAccountType> oauthAccountTypes = oauthAccountTypeService.getOauthAccountTypeList(condition);

            //获取所有子系统
            StringBuilder stringBuilderSysMode = new StringBuilder();
            for(OauthAccountType oauthAccountType:oauthAccountTypes){
                if(!StringUtil.isEmpty(oauthAccountType.getSys_mode_id())){
                    if(stringBuilderSysMode.length() == 0){
                        stringBuilderSysMode.append(oauthAccountType.getSys_mode_id());
                    }else{
                        stringBuilderSysMode.append(","+oauthAccountType.getSys_mode_id());
                    }
                }
            }

            //获取角色
            if(!StringUtil.isEmpty(stringBuilderSysMode.toString())){
                StringBuilder stringBuilderRole = new StringBuilder();
                condition = new HashMap<>();
                condition.put("sysmode_ids",stringBuilderSysMode.toString());
                List<OauthRole> oauthRoles = oauthRoleService.getOauthRoleListByCondition(condition);
                if(!CollectionUtils.isEmpty(oauthRoles)){
                    for(OauthRole oauthRole: oauthRoles){
                        if(StringUtil.isEmpty(stringBuilderRole.toString())){
                            stringBuilderRole.append(oauthRole.getRole_id());
                        }else{
                            stringBuilderRole.append(","+oauthRole.getRole_id());
                        }
                    }
                }


                //删除操作
                if(!StringUtil.isEmpty(stringBuilderRole.toString())){
                    condition = new HashMap<>();
                    condition.put("role_id",stringBuilderRole.toString().split(","));
                    condition.put("account_id",oauthAccount.getAccount_id());
                    oauthAccountRoleService.delBatchOauthAccountRole(condition);
                }

                //处理变更功能资源
                if(!StringUtil.isEmpty(oauthAccount.getAccount_id())){
                    String[] accountIdArray = oauthAccount.getAccount_id().split(",");
                    List<String> accountIdList = new ArrayList<>();
                    if(null != accountIdArray){
                        for(String id: accountIdArray){
                            accountIdList.add(id);
                        }
                    }
                    oauthUtil.doTokenResources(null,accountIdList);
                }
            }
        }
    }


    /**
     * 修改登录时间
     * @param oauthAccount
     */
    public void updateLoginTime(OauthAccount oauthAccount){
        oauthAccountDao.updateLoginTime(oauthAccount);
    }

    /**
     * 重置密码
     * @param userParamInfo
     * @param request
     * @return
     */
    public int restPwd(UserParamInfo userParamInfo, HttpServletRequest request){
        try {
            String account_id = userParamInfo.getAccount_id();
            OauthAccount oauthAccount = null;
            if(!StringUtil.isEmpty(account_id)){
                oauthAccount = oauthAccountDao.getOauthAccountById(account_id);
            }
            if(null == oauthAccount){
                oauthAccount = oauthAccountDao.getSingleOauthAccount(userParamInfo.getAccount());
            }
            if(null == oauthAccount){
                throw new ExceptionUtil("账户不存在");
            }
            MD5 md5 = new MD5();
            ConstantEntity constantEntity = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtCommon/constantByKey/XtUserinfoDefaultPwd",ConstantEntity.class,request);
            oauthAccount.setPassword(md5.getMD5ofStr(constantEntity.getValue()));
            int i = updateOauthAccount(oauthAccount);
            if(i>0){
                logsUtil.aBLogs("用户控制层", "重置用户密码", "重置用户密码，用户名：【"+oauthAccount.getAccount()+"】用户姓名：【"+oauthAccount.getName()+"】成功");
            }else{
                logsUtil.aBLogs("用户控制层", "重置用户密码", "重置用户密码，用户名：【"+oauthAccount.getAccount()+"】用户姓名：【"+oauthAccount.getName()+"】失败");
            }
            return i;
        }catch (Exception e){
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
    }

    /**
     * 修改密码
     * @param updatePasswordEntity
     * @return
     */
    public int updatePwd(UpdatePasswordEntity updatePasswordEntity){
        try {
            String accountId = updatePasswordEntity.getAccountId();
            OauthAccount oauthAccount = null;
            if(!StringUtil.isEmpty(accountId)){
                oauthAccount = oauthAccountDao.getOauthAccountById(accountId);
            }
            oauthAccount.setPassword(updatePasswordEntity.getNewPwd());
            int i = updateOauthAccount(oauthAccount);
            if(i>0){
                logsUtil.aBLogs("用户控制层", "修改个人密码", "修改个人密码，用户名：【"+oauthAccount.getAccount()+"】用户姓名：【"+oauthAccount.getName()+"】成功");
            }else{
                logsUtil.aBLogs("用户控制层", "修改个人密码", "修改个人密码，用户名：【"+oauthAccount.getAccount()+"】用户姓名：【"+oauthAccount.getName()+"】失败");
            }
            return i;
        }catch (Exception e){
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
    }

    /**
     * 同步数据
     * @param userinfoEntities
     * @return
     */
    public BaseResult sync(List<UserinfoEntity> userinfoEntities,HttpServletRequest request){
        BaseResult baseResult = new BaseResult();
        StringBuilder stringBuilder = new StringBuilder();
        List<OauthAccount> oauthAccounts = new ArrayList<>();
        if(!CollectionUtils.isEmpty(userinfoEntities)) {
            for (UserinfoEntity userinfoEntity : userinfoEntities) {
                try {
                    userinfoEntity.setXt_userinfo_passWord("");
                    if(StringUtil.isEmpty(userinfoEntity.getXt_userinfo_id())){
                        stringBuilder.append(userinfoEntity.getXt_userinfo_realName());
                        continue;
                    }
                    OauthAccount account = oauthAccountDao.getSingleOauthAccount(userinfoEntity.getXt_userinfo_name());
                    if(null == account){
                        account = new OauthAccount();
                        MD5 md5 = new MD5();
                        ConstantEntity constantEntity = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtCommon/constantByKey/XtUserinfoDefaultPwd",ConstantEntity.class,request);
                        account.setPassword(md5.getMD5ofStr(constantEntity.getValue()));
                        account.setAccount(userinfoEntity.getXt_userinfo_name());
                        account.setName(userinfoEntity.getXt_userinfo_realName());
                        account.setEmail(userinfoEntity.getXt_userinfo_email());
                        account.setAccount_id(userinfoEntity.getXt_userinfo_id());
                        account.setAccount_type_id(userinfoEntity.getAccount_type_id());
                        account.setInfo_body(JsonUtil.toJson(userinfoEntity));
                        oauthAccountDao.addOauthAccount(account);
                    }else{
                        account.setName(userinfoEntity.getXt_userinfo_realName());
                        account.setEmail(userinfoEntity.getXt_userinfo_email());
                        account.setInfo_body(JsonUtil.toJson(userinfoEntity));
                        if(userinfoEntity.getSync() == 1){//同步账号类型
                            if(StringUtil.isEmpty(userinfoEntity.getAccount_type_id())){
                                stringBuilder.append(userinfoEntity.getXt_userinfo_realName());
                                continue;
                            }
                            account.setAccount_type_id(userinfoEntity.getAccount_type_id());//同步账号类型
                            //如果账户存在 则判断是否重复
                            if(!StringUtil.isEmpty(account.getAccount())){
                                //获取原登录名
                                if(!account.getAccount().equals(userinfoEntity.getXt_userinfo_name())){ //如果登录名发生变化 则需要验证
                                    if(null != oauthAccountDao.getSingleOauthAccount(userinfoEntity.getXt_userinfo_name())){
                                        log.info("登录名已存在{}：",account);
                                        continue;
                                    }
                                }
                                doAccountType(account,account.getAccount_type_id());//处理类型变更逻辑
                            }
                        }else{
                            //只同步登录状态的用户基本信息
                            oauthAccounts.add(account);
                        }
                        oauthAccountDao.updateOauthAccount(account);
                    }
                }catch (Exception e){
                    log.error("同步数据异常：{}",e);
                }
            }
        }
        oauthUtil.doTokenAccount(oauthAccounts);
        if(StringUtil.isEmpty(stringBuilder.toString())){
            baseResult.setMessage("同步至授权中心成功");
            return new BaseResult();
        }
        baseResult.setMessage("未能同步成功数据如下：【"+stringBuilder.toString()+"】");
        return baseResult;
    }
}
