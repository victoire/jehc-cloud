package jehc.cloud.oauth.model;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 授权中心账户对角色
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthAccountRole extends BaseEntity {
	private String account_role_id;/****/
	private String role_id;/**角色ID外键**/
	private String account_id;/**账号ID外键**/

	private String role_name;
}
