package jehc.cloud.oauth.publisher;

import io.netty.channel.ChannelFuture;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.oauth.vo.ChannelEntity;
import jehc.cloud.oauth.vo.RequestInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Desc Netty 工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Component
public class NettyUtil {

    /**
     * 发送消息
     * @param channelEntity
     */
    public boolean sendMessage(ChannelEntity channelEntity){
        boolean result = true;
        try {
            if(null == channelEntity.getChannel()){
                log.info("未能获取到通道号");
                result = false;
            }else{
                RequestInfo requestInfo = channelEntity.getRequestInfo();
                String json = JsonUtil.toJson(requestInfo);
//                ChannelFuture cf = channelEntity.getChannel().writeAndFlush(Unpooled.copiedBuffer(json.getBytes()));
                ChannelFuture cf = channelEntity.getChannel().writeAndFlush(json+"\r\n");
                cf.addListener((f)-> {
                    log.info("数据发送成功！{}-{}",f.isSuccess(),json);
                });
            }
        }catch (Exception e){
            log.error("发送消息异常：",e);
            result =  false;
        }
        return result;
    }
}
