package jehc.cloud.oauth.service;

import jehc.cloud.oauth.model.OauthAdminSys;

import java.util.List;
import java.util.Map;
/**
 * @Desc 管理员拥有系统范围
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthAdminSysService {
    /**
     *
     * @param condition
     * @return
     */
    List<OauthAdminSys> getOauthAdminSysList(Map<String,Object> condition);

    /**
     *
     * @param oauthAdminSys
     * @return
     */
    int addOauthAdminSys(OauthAdminSys oauthAdminSys);
}
