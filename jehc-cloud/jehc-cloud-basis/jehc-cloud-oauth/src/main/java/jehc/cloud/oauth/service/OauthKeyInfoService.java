package jehc.cloud.oauth.service;
import java.util.List;
import java.util.Map;
import jehc.cloud.oauth.model.OauthKeyInfo;
/**
 * @Desc 授权中心密钥管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthKeyInfoService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthKeyInfo> getOauthKeyInfoListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param key_info_id 
	* @return
	*/
	OauthKeyInfo getOauthKeyInfoById(String key_info_id);
	/**
	* 添加
	* @param oauthKeyInfo 
	* @return
	*/
	int addOauthKeyInfo(OauthKeyInfo oauthKeyInfo);
	/**
	* 修改
	* @param oauthKeyInfo 
	* @return
	*/
	int updateOauthKeyInfo(OauthKeyInfo oauthKeyInfo);
	/**
	* 修改（根据动态条件）
	* @param oauthKeyInfo 
	* @return
	*/
	int updateOauthKeyInfoBySelective(OauthKeyInfo oauthKeyInfo);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delOauthKeyInfo(Map<String, Object> condition);
	/**
	* 批量修改
	* @param oauthKeyInfoList 
	* @return
	*/
	int updateBatchOauthKeyInfo(List<OauthKeyInfo> oauthKeyInfoList);
	/**
	* 批量修改（根据动态条件）
	* @param oauthKeyInfoList 
	* @return
	*/
	int updateBatchOauthKeyInfoBySelective(List<OauthKeyInfo> oauthKeyInfoList);
}
