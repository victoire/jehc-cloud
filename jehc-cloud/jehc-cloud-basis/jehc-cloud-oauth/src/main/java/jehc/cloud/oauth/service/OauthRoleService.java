package jehc.cloud.oauth.service;
import java.util.List;
import java.util.Map;
import jehc.cloud.oauth.model.OauthRole;
/**
 * @Desc 角色模块
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthRoleService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthRole> getOauthRoleListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param role_id 
	* @return
	*/
	OauthRole getOauthRoleById(String role_id);
	/**
	* 添加
	* @param oauthRole 
	* @return
	*/
	int addOauthRole(OauthRole oauthRole);
	/**
	* 修改
	* @param oauthRole 
	* @return
	*/
	int updateOauthRole(OauthRole oauthRole);
	/**
	* 修改（根据动态条件）
	* @param oauthRole 
	* @return
	*/
	int updateOauthRoleBySelective(OauthRole oauthRole);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delOauthRole(Map<String, Object> condition);
	/**
	* 批量修改
	* @param oauthRoleList 
	* @return
	*/
	int updateBatchOauthRole(List<OauthRole> oauthRoleList);
	/**
	* 批量修改（根据动态条件）
	* @param oauthRoleList 
	* @return
	*/
	int updateBatchOauthRoleBySelective(List<OauthRole> oauthRoleList);

	/**
	 * 恢复
	 * @param condition
	 * @return
	 */
	int recoverOauthRole(Map<String, Object> condition);
}
