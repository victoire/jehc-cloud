package jehc.cloud.oauth.service;
import java.util.List;
import java.util.Map;
import jehc.cloud.oauth.model.OauthFunctionCommon;

/**
 * @Desc 授权中心公共功能
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthFunctionCommonService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthFunctionCommon> getOauthFunctionCommonListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param function_common_id 
	* @return
	*/
	OauthFunctionCommon getOauthFunctionCommonById(String function_common_id);
	/**
	* 添加
	* @param oauthFunctionCommon 
	* @return
	*/
	int addOauthFunctionCommon(OauthFunctionCommon oauthFunctionCommon);
	/**
	* 修改
	* @param oauthFunctionCommon 
	* @return
	*/
	int updateOauthFunctionCommon(OauthFunctionCommon oauthFunctionCommon);
	/**
	* 修改（根据动态条件）
	* @param oauthFunctionCommon 
	* @return
	*/
	int updateOauthFunctionCommonBySelective(OauthFunctionCommon oauthFunctionCommon);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delOauthFunctionCommon(Map<String, Object> condition);
	/**
	* 批量修改
	* @param oauthFunctionCommonList 
	* @return
	*/
	int updateBatchOauthFunctionCommon(List<OauthFunctionCommon> oauthFunctionCommonList);
	/**
	* 批量修改（根据动态条件）
	* @param oauthFunctionCommonList 
	* @return
	*/
	int updateBatchOauthFunctionCommonBySelective(List<OauthFunctionCommon> oauthFunctionCommonList);
}
