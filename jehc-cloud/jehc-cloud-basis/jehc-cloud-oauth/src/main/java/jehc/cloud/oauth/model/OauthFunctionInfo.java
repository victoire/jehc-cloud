package jehc.cloud.oauth.model;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 功能中心
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthFunctionInfo extends BaseEntity{
	private String function_info_id;/**编号**/
	private String function_info_name;/**功能名称**/
	private String function_info_url;/**拦截地址**/
	private String function_info_method;/**拦截方法**/
	private String sys_id;/**子系统标记外键**/
	private String modules_id;/**模块编号（即模块表主键）**/
	private int issysmodules;/**是否平台模块**/
	private int isfilter;/**是否拦截**/
	private String menu_id;/**资源编号外键即菜单表主键**/
	private int isAuthority;//是否数据权限0是1否
	private int status;//状态0正常1禁用
	private String resources_title;
}
