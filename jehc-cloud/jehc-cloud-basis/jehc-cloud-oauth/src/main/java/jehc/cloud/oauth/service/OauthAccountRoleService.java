package jehc.cloud.oauth.service;
import java.util.List;
import java.util.Map;

import jehc.cloud.common.entity.RoleinfoEntity;
import jehc.cloud.oauth.model.OauthAccount;
import jehc.cloud.oauth.model.OauthAccountRole;

/**
 * @Desc 授权中心账户对角色
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthAccountRoleService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthAccountRole> getOauthAccountRoleListByCondition(Map<String, Object> condition);
	/**
	* 添加
	* @param oauthAccountRoleList
	* @return
	*/
	int addOauthAccountRole(RoleinfoEntity roleinfoEntity,List<OauthAccountRole> oauthAccountRoleList);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delOauthAccountRole(Map<String, Object> condition);
	List<OauthAccount> getOauthARListByCondition(Map<String, Object> condition);

	/**
	 * 批量删除
	 * @param condition
	 * @return
	 */
	int delBatchOauthAccountRole(Map<String, Object> condition);
}
