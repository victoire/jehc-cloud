package jehc.cloud.oauth.service.impl;
import java.util.List;
import java.util.Map;

import jehc.cloud.oauth.service.OauthKeyInfoService;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.oauth.dao.OauthKeyInfoDao;
import jehc.cloud.oauth.model.OauthKeyInfo;

/**
 * @Desc 授权中心密钥管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("oauthKeyInfoService")
public class OauthKeyInfoServiceImpl extends BaseService implements OauthKeyInfoService {
	@Autowired
	private OauthKeyInfoDao oauthKeyInfoDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<OauthKeyInfo> getOauthKeyInfoListByCondition(Map<String,Object> condition){
		try{
			return oauthKeyInfoDao.getOauthKeyInfoListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param key_info_id 
	* @return
	*/
	public OauthKeyInfo getOauthKeyInfoById(String key_info_id){
		try{
			OauthKeyInfo oauthKeyInfo = oauthKeyInfoDao.getOauthKeyInfoById(key_info_id);
			return oauthKeyInfo;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param oauthKeyInfo 
	* @return
	*/
	public int addOauthKeyInfo(OauthKeyInfo oauthKeyInfo){
		int i = 0;
		try {
			oauthKeyInfo.setCreate_id(getXtUid());
			oauthKeyInfo.setCreate_time(getDate());
			i = oauthKeyInfoDao.addOauthKeyInfo(oauthKeyInfo);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param oauthKeyInfo 
	* @return
	*/
	public int updateOauthKeyInfo(OauthKeyInfo oauthKeyInfo){
		int i = 0;
		try {
			oauthKeyInfo.setUpdate_id(getXtUid());
			oauthKeyInfo.setUpdate_time(getDate());
			i = oauthKeyInfoDao.updateOauthKeyInfo(oauthKeyInfo);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param oauthKeyInfo 
	* @return
	*/
	public int updateOauthKeyInfoBySelective(OauthKeyInfo oauthKeyInfo){
		int i = 0;
		try {
			oauthKeyInfo.setUpdate_id(getXtUid());
			oauthKeyInfo.setUpdate_time(getDate());
			i = oauthKeyInfoDao.updateOauthKeyInfoBySelective(oauthKeyInfo);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delOauthKeyInfo(Map<String,Object> condition){
		int i = 0;
		try {
			i = oauthKeyInfoDao.delOauthKeyInfo(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param oauthKeyInfoList 
	* @return
	*/
	public int updateBatchOauthKeyInfo(List<OauthKeyInfo> oauthKeyInfoList){
		int i = 0;
		try {
			i = oauthKeyInfoDao.updateBatchOauthKeyInfo(oauthKeyInfoList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param oauthKeyInfoList 
	* @return
	*/
	public int updateBatchOauthKeyInfoBySelective(List<OauthKeyInfo> oauthKeyInfoList){
		int i = 0;
		try {
			i = oauthKeyInfoDao.updateBatchOauthKeyInfoBySelective(oauthKeyInfoList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
