package jehc.cloud.oauth.service;

import jehc.cloud.oauth.model.OauthAccountType;

import java.util.List;
import java.util.Map;

/**
 * @Desc 账号类型
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthAccountTypeService {
    /**
     * 分页
     * @param condition
     * @return
     */
    List<OauthAccountType> getOauthAccountTypeList(Map<String,Object> condition);
    /**
     * 查询对象
     * @param account_type_id
     * @return
     */
    OauthAccountType getOauthAccountTypeById(String account_type_id);


    /**
     * 添加
     * @param oauthAccountType
     * @return
     */
    int addOauthAccountType(OauthAccountType oauthAccountType);
    /**
     * 修改（根据动态条件）
     * @param oauthAccountType
     * @return
     */
    int updateOauthAccountType(OauthAccountType oauthAccountType);

    /**
     * 删除类型
     * @param oauthAccountType
     * @return
     */
    int delAccountType(OauthAccountType oauthAccountType);

    /**
     * 根据角色获取当前系统中隶属于其账户类型
     * @param condition
     * @return
     */
    List<OauthAccountType> getAccountTypeListByRoleId(Map<String,Object> condition);

    /**
     * 根据账号类型及子系统获取有多少个
     * @param condition
     * @return
     */
    int getAccountTypeInSysMode(Map<String,Object> condition);
}
