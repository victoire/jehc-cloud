package jehc.cloud.oauth.service.impl;
import java.util.List;
import java.util.Map;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.common.util.file.FileUtil;
import jehc.cloud.oauth.service.OauthFunctionInfoService;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.oauth.dao.OauthFunctionInfoDao;
import jehc.cloud.oauth.model.OauthFunctionInfo;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Desc 功能中心
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("oauthFunctionInfoService")
@Slf4j
public class OauthFunctionInfoServiceImpl extends BaseService implements OauthFunctionInfoService {
	@Autowired
	private OauthFunctionInfoDao oauthFunctionInfoDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<OauthFunctionInfo> getOauthFunctionInfoListByCondition(Map<String,Object> condition){
		try{
			return oauthFunctionInfoDao.getOauthFunctionInfoListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param function_info_id 
	* @return
	*/
	public OauthFunctionInfo getOauthFunctionInfoById(String function_info_id){
		try{
			OauthFunctionInfo oauthFunctionInfo = oauthFunctionInfoDao.getOauthFunctionInfoById(function_info_id);
			return oauthFunctionInfo;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param oauthFunctionInfo 
	* @return
	*/
	public int addOauthFunctionInfo(OauthFunctionInfo oauthFunctionInfo){
		int i = 0;
		try {
			i = oauthFunctionInfoDao.addOauthFunctionInfo(oauthFunctionInfo);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param oauthFunctionInfo 
	* @return
	*/
	public int updateOauthFunctionInfo(OauthFunctionInfo oauthFunctionInfo){
		int i = 0;
		try {
			i = oauthFunctionInfoDao.updateOauthFunctionInfo(oauthFunctionInfo);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param oauthFunctionInfo 
	* @return
	*/
	public int updateOauthFunctionInfoBySelective(OauthFunctionInfo oauthFunctionInfo){
		int i = 0;
		try {
			i = oauthFunctionInfoDao.updateOauthFunctionInfoBySelective(oauthFunctionInfo);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delOauthFunctionInfo(Map<String,Object> condition){
		int i = 0;
		try {
			i = oauthFunctionInfoDao.delOauthFunctionInfo(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param oauthFunctionInfoList 
	* @return
	*/
	public int updateBatchOauthFunctionInfo(List<OauthFunctionInfo> oauthFunctionInfoList){
		int i = 0;
		try {
			i = oauthFunctionInfoDao.updateBatchOauthFunctionInfo(oauthFunctionInfoList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param oauthFunctionInfoList 
	* @return
	*/
	public int updateBatchOauthFunctionInfoBySelective(List<OauthFunctionInfo> oauthFunctionInfoList){
		int i = 0;
		try {
			i = oauthFunctionInfoDao.updateBatchOauthFunctionInfoBySelective(oauthFunctionInfoList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	 * 初始化集合（for admin all function）
	 * @param condition
	 * @return
	 */
	public List<OauthFunctionInfo> getFunctioninfoListForAdmin(Map<String, Object> condition){
		return oauthFunctionInfoDao.getFunctioninfoListForAdmin(condition);
	}

	/**
	 * 读取全部功能
	 * @param condition
	 * @return
	 */
	public List<OauthFunctionInfo> getOauthFunctionInfoList(Map<String, Object> condition){
		return oauthFunctionInfoDao.getOauthFunctionInfoList(condition);
	}

	/**
	 * 导出功能
	 * @param response
	 * @param function_info_id
	 */
	public void exportOauthFunctionInfo(HttpServletResponse response, String function_info_id){
		try{
			if(StringUtil.isEmpty(function_info_id)){
				log.warn("未能获取到功能id");
				throw new ExceptionUtil("未能获取到功能id");
			}
			OauthFunctionInfo oauthFunctionInfo = oauthFunctionInfoDao.getOauthFunctionInfoById(function_info_id);
			if(null == oauthFunctionInfo){
				log.warn("未能获取到功能信息");
				throw new ExceptionUtil("未能获取到功能信息");
			}

			String json = JsonUtil.toFastJson(oauthFunctionInfo);
			FileUtil.exportJsonFile(response,json,oauthFunctionInfo.getResources_title()+"-"+oauthFunctionInfo.getFunction_info_name()+".json");
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 导入功能
	 * @param multipartFile
	 * @return
	 */
	public BaseResult importOauthFunctionInfo(HttpServletRequest request, HttpServletResponse response, MultipartFile multipartFile){
		String jsonText = FileUtil.readJson(multipartFile);
		if(StringUtil.isEmpty(jsonText)){
			return BaseResult.fail("导入内容为空！");
		}
		OauthFunctionInfo oauthFunctionInfo = JsonUtil.fromAliFastJson(jsonText,OauthFunctionInfo.class);
		if(null == oauthFunctionInfo){
			return BaseResult.fail("未能解析到功能信息！");
		}
		if(StringUtil.isEmpty(oauthFunctionInfo.getFunction_info_id())){
			return BaseResult.fail("未能解析到功能id！");
		}
		if(StringUtil.isEmpty(oauthFunctionInfo.getFunction_info_name())){
			return BaseResult.fail("未能解析到功能名称！");
		}
		if(StringUtil.isEmpty(oauthFunctionInfo.getFunction_info_url())){
			return BaseResult.fail("未能解析到功能url！");
		}
		OauthFunctionInfo functionInfo = getOauthFunctionInfoById(oauthFunctionInfo.getFunction_info_id());
		if(null == functionInfo){//新增
			addOauthFunctionInfo(oauthFunctionInfo);
		}else{
			updateOauthFunctionInfoBySelective(oauthFunctionInfo);
		}
		return BaseResult.success();
	}
}
