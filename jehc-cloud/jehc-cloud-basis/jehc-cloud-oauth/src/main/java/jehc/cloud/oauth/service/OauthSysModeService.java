package jehc.cloud.oauth.service;
import java.util.List;
import java.util.Map;
import jehc.cloud.oauth.model.OauthSysMode;

import javax.servlet.http.HttpServletRequest;

/**
 * @Desc 授权中心子系统标记
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthSysModeService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthSysMode> getOauthSysModeListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param sys_mode_id 
	* @return
	*/
	OauthSysMode getOauthSysModeById(String sys_mode_id);
	/**
	* 添加
	* @param oauthSysMode 
	* @return
	*/
	int addOauthSysMode(OauthSysMode oauthSysMode);
	/**
	* 修改
	* @param oauthSysMode 
	* @return
	*/
	int updateOauthSysMode(OauthSysMode oauthSysMode);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delOauthSysMode(Map<String, Object> condition);

	/**
	 * 获取SysModeId
	 * @param request
	 * @return
	 */
	String getSysModeId(HttpServletRequest request);
}
