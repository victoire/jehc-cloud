package jehc.cloud.oauth.dao;

import jehc.cloud.oauth.model.OauthAdmin;

import java.util.List;
import java.util.Map;

/**
 * oauth_admin 管理员中心
 * 2019-06-20 14:51:55  邓纯杰
 */
public interface OauthAdminDao {

    /**
     *
     * @param condition
     * @return
     */
    List<OauthAdmin> getOauthAdminList(Map<String,Object> condition);

    /**
     *
     * @param oauthAdmin
     * @return
     */
    int addOauthAdmin(OauthAdmin oauthAdmin);

    /**
     *
     * @param condition
     * @return
     */
    List<OauthAdmin> getOauthAdminListAll(Map<String,Object> condition);

    /**
     * 删除
     * @param id
     * @return
     */
    int delOauthAdmin(String id);
}
