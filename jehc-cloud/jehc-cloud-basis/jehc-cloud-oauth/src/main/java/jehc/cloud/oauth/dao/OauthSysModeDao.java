package jehc.cloud.oauth.dao;
import java.util.List;
import java.util.Map;
import jehc.cloud.oauth.model.OauthSysMode;

/**
* 授权中心子系统标记 
* 2019-06-20 15:14:06  邓纯杰
*/
public interface OauthSysModeDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthSysMode> getOauthSysModeListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param sys_mode_id 
	* @return
	*/
	OauthSysMode getOauthSysModeById(String sys_mode_id);
	/**
	* 添加
	* @param oauthSysMode 
	* @return
	*/
	int addOauthSysMode(OauthSysMode oauthSysMode);
	/**
	* 修改
	* @param oauthSysMode 
	* @return
	*/
	int updateOauthSysMode(OauthSysMode oauthSysMode);

	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delOauthSysMode(Map<String, Object> condition);
}
