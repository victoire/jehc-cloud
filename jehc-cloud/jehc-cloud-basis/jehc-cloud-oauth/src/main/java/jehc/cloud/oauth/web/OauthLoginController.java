package jehc.cloud.oauth.web;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.AuthUneedLogin;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BaseHttpSessionEntity;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.constant.SessionConstant;
import jehc.cloud.common.entity.LoginEntity;
import jehc.cloud.common.entity.OauthAccountEntity;
import jehc.cloud.common.util.MD5;
import jehc.cloud.common.session.HttpSessionUtils;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.log.client.entity.LogLoginDTO;
import jehc.cloud.log.client.service.LogsUtil;
import jehc.cloud.oauth.constant.Constant;
import jehc.cloud.oauth.model.*;
import jehc.cloud.oauth.publisher.ChannelUtil;
import jehc.cloud.oauth.publisher.NettyUtil;
import jehc.cloud.oauth.service.*;
import jehc.cloud.oauth.util.OauthUtil;
import jehc.cloud.oauth.vo.ChannelEntity;
import jehc.cloud.oauth.vo.Transfer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @Desc 授权中心登录API
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@Api(value = "授权中心登录API",tags = "授权中心登录API",description = "授权中心登录API")
@Slf4j
public class OauthLoginController  extends BaseAction {

    @Autowired
    NettyUtil nettyUtil;
    @Autowired
    ChannelUtil channelUtil;

    @Autowired
    private OauthUtil oauthUtil;

    @Autowired
    HttpSessionUtils httpSessionUtils;

    @Autowired
    private OauthAccountService oauthAccountService;

    @Autowired
    private OauthAccountRoleService oauthAccountRoleService;

    @Autowired
    private OauthFunctionRoleService oauthFunctionRoleService;

    @Autowired
    private OauthAdminService oauthAdminService;

    @Autowired
    private OauthAdminSysService oauthAdminSysService;

    @Autowired
    private OauthSysModeService oauthSysModeService;

    @Autowired
    private OauthAccountTypeService oauthAccountTypeService;

    @Autowired
    private LogsUtil loginLogs;

    Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * 登录
     * @param loginEntity
     * @param request
     * @return
     */
    @AuthUneedLogin
    @PostMapping(value="/login")
    @ApiOperation(value="登录", notes="登录")
    public BaseResult login(@RequestBody LoginEntity loginEntity, HttpServletRequest request){
        if(null == loginEntity){
            return outAudStr(false,"登录信息为空");
        }
        if(StringUtil.isEmpty(loginEntity.getAccount())){
            return outAudStr(false,"用户名为空");
        }
        if(StringUtil.isEmpty(loginEntity.getPassword())){
            return outAudStr(false,"密码为空");
        }
        if(!StringUtil.isEmpty(loginEntity.getValidateCode())){
            String rand = httpSessionUtils.getVerifyAttribute(request);
            if(!StringUtils.isEmpty(rand)){
                rand = rand.toLowerCase();
            }
            if(!loginEntity.getValidateCode().equals(rand)){
                return outAudStr(false,"验证码不正确",1);
            }
        }
        LogLoginDTO logLoginDTO = new LogLoginDTO();
        MD5 md5 = new MD5();
        Map<String,Object> condition = new HashMap<>();
        condition.put("account",loginEntity.getAccount());
        condition.put("password",md5.getMD5ofStr(loginEntity.getPassword().trim()));
        OauthAccount oauthAccount = oauthAccountService.login(condition);
        if(null == oauthAccount){
            logLoginDTO.setContent("账号："+loginEntity.getAccount()+"，用户名或密码有误，登录失败");
            loginLogs.loginLogs(logLoginDTO);
            return outAudStr(false,"用户名或密码有误",2);
        }
        if(StringUtil.isEmpty(oauthAccount.getAccount_type_id())){
            logLoginDTO.setContent("账号："+loginEntity.getAccount()+"，该账号未设置账号类型，登录失败");
            loginLogs.loginLogs(logLoginDTO);
            return outAudStr(false,"该账号未设置账号类型",2);
        }
        /* （登录时候废弃判断账号类型是否存在子系统中）
        condition.put("sys_mode_id",oauthSysModeService.getSysModeId(request));
        condition.put("account_type_id",oauthAccount.getAccount_type_id().split(","));
        int count = oauthAccountTypeService.getAccountTypeInSysMode(condition);
        if(count == 0){
            return outAudStr(false,"该账号类型不存在当前子系统",2);
        }*/


        //TODO 处理最后一次登录时间
        oauthAccount.setLast_login_time(new Date());
        oauthAccountService.updateLoginTime(oauthAccount);

        StringBuffer role_id = new StringBuffer();
        OauthAccountEntity oauthAccountEntity = new OauthAccountEntity();
        BeanUtils.copyProperties(oauthAccount, oauthAccountEntity);


        condition = new HashMap<>();
        condition.put("account_id",oauthAccountEntity.getAccount_id());

        //TODO 处理管理员拥有系统范围
        List<OauthAdminSys> oauthAdminSys = adminSys(condition);

        //TODO 处理角色
        List<OauthAccountRole> urList = oauthAccountRoleService.getOauthAccountRoleListByCondition(condition);
        for(int i = 0; i < urList.size(); i++){
            OauthAccountRole oauthAccountRole = urList.get(i);
            if(null != role_id && !StringUtil.isEmpty(role_id.toString())){
                role_id.append(","+oauthAccountRole.getRole_id());
            }else{
                role_id.append(oauthAccountRole.getRole_id());
            }
        }

        //TODO 处理功能
        List<OauthFunctionRole> oauthFunctionRoleList = new ArrayList<OauthFunctionRole>();
        if(!StringUtils.isEmpty(role_id.toString())){
            /////////////根据角色集合查找该用户下所有功能
            condition = new HashMap<String, Object>();
            condition.put("role_id", role_id.toString().split(","));
            oauthFunctionRoleList = oauthFunctionRoleService.getOauthFunctionRoleListByCondition(condition);
        }
        Map<String,String> oauthFunctionInfoUrlMap = new HashMap<>();
        Map<String,String> oauthFunctionInfoMethodMap = new HashMap<>();
        for(OauthFunctionRole oauthFunctionRole:oauthFunctionRoleList){
            oauthFunctionInfoUrlMap.put(oauthFunctionRole.getFunction_info_url(),oauthFunctionRole.getFunction_info_url());
            oauthFunctionInfoMethodMap.put(oauthFunctionRole.getFunction_info_method(),oauthFunctionRole.getFunction_info_method());
        }

        BaseHttpSessionEntity baseHttpSessionEntity = new BaseHttpSessionEntity(oauthAccountEntity,role_id.toString(),oauthFunctionInfoUrlMap,oauthFunctionInfoMethodMap,dataAuthority(request,oauthAccountEntity),null, JsonUtil.toFastJson(oauthAdminSys));
        loginEntity.setBaseHttpSessionEntity(JsonUtil.toFastJson(baseHttpSessionEntity));

        String token = oauthUtil.doLastToken(oauthAccount.getAccount_id(),loginEntity.getBaseHttpSessionEntity());
        if(StringUtil.isEmpty(token)){//上次Token不存在则重新创建
            Map<String,Object> map = oauthUtil.createTokenFn(oauthAccount.getAccount_id(),loginEntity.getBaseHttpSessionEntity(),12);//默认1小时
            if(null != map && !map.isEmpty()){
                token = (String)map.get("Token");
            }
        }
        if(StringUtil.isEmpty(token)){
            logger.debug("-------------未能生成Token信息----------------");
            logLoginDTO.setContent("账号："+loginEntity.getAccount()+"，登录失败");
            loginLogs.loginLogs(logLoginDTO);
            return outAudStr(false,"未能生成Token信息");
        }
        removeVerify(request);
        logger.debug("-------------Token创建成功----------------");
        logLoginDTO.setContent("账号："+loginEntity.getAccount()+"，登录成功");
        loginLogs.loginLogs(logLoginDTO);
        return outAudStr(true,"Login Success，Token创建成功",token);
    }

    /**
     * 处理管理员拥有系统范围
     * @param condition
     * @return
     */
    public List<OauthAdminSys> adminSys(Map<String,Object> condition){
        List<OauthAdmin> oauthAdmins = oauthAdminService.getOauthAdminList(condition);
        List<String> adminIds = new ArrayList<>();
        for(OauthAdmin oauthAdmin:oauthAdmins){
            adminIds.add(oauthAdmin.getId());
        }
        if(null == adminIds || adminIds.isEmpty()){
            return new ArrayList<>();
        }
        condition.put("admin_id",adminIds);
        List<OauthAdminSys> oauthAdminSysList = oauthAdminSysService.getOauthAdminSysList(condition);
        MD5 md5 = new MD5();
        for(int i = 0; i < oauthAdminSysList.size(); i++){
            oauthAdminSysList.get(i).setKey_pass(md5.getMD5ofStr(oauthAdminSysList.get(i).getKey_pass()));
        }
        return oauthAdminSysList;
    }

    /**
     * 数据权限
     * @param request
     */
    public List<String> dataAuthority(HttpServletRequest request, OauthAccountEntity oauthAccountEntity){
        /////////////////////////////////
        /////////////////////////操作数据及数据功能权限 开始
        /////////////////////////////////
//        Map<String,Object> condition = new HashMap<String, Object>();
//
//        if(!isAdmin(oauthAccountEntity)){
//            condition.put("xt_userinfo_id", oauthAccountEntity.getAccount_id());
//        }
        List<String> systemUandM = new ArrayList<String>();//用户及功能URL
//        List<XtDataAuthority> xt_Data_AuthorityList = xtDataAuthorityService.getXtDataAuthorityListForLogin(condition);
//        for(XtDataAuthority xtDataAuthority :xt_Data_AuthorityList){
//            systemUandM.add(xtDataAuthority.getXtUID()+"#"+xtDataAuthority.getXt_functioninfoURL());
//        }
//        //将数据及数据功能权限等信息放入到里面
//        /////////////////////////////////
//        /////////////////////////操作数据及数据功能权限 结束
//        /////////////////////////////////
        return systemUandM;
    }

    /**
     * 注销
     * @param request
     */
    @AuthUneedLogin
    @PostMapping(value="/loginOut")
    @ApiOperation(value="注销", notes="注销")
    public BaseResult loginOut(HttpServletRequest request){
        String token = oauthUtil.getTokenId(request);
        OauthAccountEntity oauthAccountEntity = getXtU();
        oauthUtil.destory(request);
        if(null != oauthAccountEntity){
            LogLoginDTO logLoginDTO = new LogLoginDTO();
            logLoginDTO.setContent("账号："+oauthAccountEntity.getAccount()+"，注销平台成功");
            loginLogs.loginLogs(logLoginDTO);
            publish(token);
        }
        return outAudStr(true, "注销平台成功");
    }

    /**
     *
     * @param token
     */
    public void publish(String token){
        try {
            Map<String,ChannelEntity> map = channelUtil.getChannelConcurrentHashMap();
            if(CollectionUtil.isEmpty(map)){
                return;
            }
            Iterator<Map.Entry<String, ChannelEntity>> entries = map.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, ChannelEntity> entry = entries.next();
                ChannelEntity channelEntity = entry.getValue();
                channelEntity.getRequestInfo().setData(new Transfer(token, Constant.TOKEN_DESTORY));
                boolean res = nettyUtil.sendMessage(entry.getValue());
                log.info("发布Token至客户端. 发送结果：{}-Token：{}",res,token);
            }
        }catch (Exception e){
            log.info("发布Token至客户端异常. Token：{}-ERROR：{}",token,e);
        }
    }
    /**
     * 创建SessionId
     * @param request
     */
    @AuthUneedLogin
    @GetMapping(value="/JSessionId")
    @ApiOperation(value="创建SessionId", notes="创建SessionId")
    public BaseResult GenJSessionId(HttpServletRequest request){
        String sessionId = httpSessionUtils.getSessionId(request);
        return new BaseResult("Create JSessionId success",true,sessionId);
    }



    /**
     *
     * @param random
     * @param request
     * @return
     */
    @AuthUneedLogin
    @GetMapping(value="/verify")
    @ApiOperation(value="创建验证码", notes="创建验证码")
    public BaseResult verify(String random,HttpServletRequest request){
       String sessionId = httpSessionUtils.getSessionId(request);
       httpSessionUtils.setAttributeExpTime(SessionConstant.VERIFY_STORE_PATH+sessionId, random,1);//5分钟
        return new BaseResult("Create verify success",true,sessionId);
    }

    /**
     * @param request
     * @return
     */
    @AuthUneedLogin
    @GetMapping(value="/query/verify")
    @ApiOperation(value="查询验证码", notes="查询验证码")
    public BaseResult queryVerify(HttpServletRequest request){
        String rand = httpSessionUtils.getVerifyAttribute(request);
        return new BaseResult("Query verify success",true,rand);
    }

    /**
     *
     * @param request
     */
    public void removeVerify(HttpServletRequest request){
        httpSessionUtils.del(SessionConstant.VERIFY_STORE_PATH+httpSessionUtils.getSessionId(request));
    }
}
