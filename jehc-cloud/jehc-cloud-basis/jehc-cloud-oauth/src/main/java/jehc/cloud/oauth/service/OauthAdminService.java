package jehc.cloud.oauth.service;

import jehc.cloud.oauth.model.OauthAdmin;

import java.util.List;
import java.util.Map;

/**
 * @Desc 管理员中心
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthAdminService {
    /**
     *
     * @param condition
     * @return
     */
    List<OauthAdmin> getOauthAdminList(Map<String,Object> condition);

    /**
     *
     * @param oauthAdmin
     * @return
     */
    int addOauthAdmin(OauthAdmin oauthAdmin);

    /**
     *
     * @param condition
     * @return
     */
    List<OauthAdmin> getOauthAdminListAll(Map<String,Object> condition);

    /**
     * 从模块系统中移除管理员
     * @param oauthAdmin
     * @return
     */
    int delOauthAdmin(OauthAdmin oauthAdmin);

}
