package jehc.cloud.sys.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.common.entity.OauthAccountEntity;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.sys.model.XtSms;
import jehc.cloud.sys.service.XtSmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

/**
 * @Desc 短信配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtSms")
@Api(value = "短信配置API",tags = "短信配置API",description = "短信配置API")
public class XtSmsController extends BaseAction {
	@Autowired
	private XtSmsService xtSmsService;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询短信列表并分页", notes="查询短信列表并分页")
	public BasePage getXtSmsListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtSms> xtSmsList = xtSmsService.getXtSmsListByCondition(condition);
		for(XtSms xtSms:xtSmsList){
			if(!jehc.cloud.common.util.StringUtil.isEmpty(xtSms.getCreate_id())){
				OauthAccountEntity createBy = getAccount(xtSms.getCreate_id());
				if(null != createBy){
					xtSms.setCreateBy(createBy.getName());
				}
			}
			if(!jehc.cloud.common.util.StringUtil.isEmpty(xtSms.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(xtSms.getUpdate_id());
				if(null != modifiedBy){
					xtSms.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<XtSms> page = new PageInfo<XtSms>(xtSmsList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单个短信
	* @param xt_sms_id
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_sms_id}")
	@ApiOperation(value="查询单个短信", notes="查询单个短信")
	public BaseResult getXtSmsById(@PathVariable("xt_sms_id")String xt_sms_id){
		XtSms xtSms = xtSmsService.getXtSmsById(xt_sms_id);
		if(!jehc.cloud.common.util.StringUtil.isEmpty(xtSms.getCreate_id())){
			OauthAccountEntity createBy = getAccount(xtSms.getCreate_id());
			if(null != createBy){
				xtSms.setCreateBy(createBy.getName());
			}
		}
		if(!jehc.cloud.common.util.StringUtil.isEmpty(xtSms.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(xtSms.getUpdate_id());
			if(null != modifiedBy){
				xtSms.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(xtSms);
	}
	/**
	* 添加
	* @param xtSms
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个短信", notes="创建单个短信")
	public BaseResult addXtSms(@RequestBody XtSms xtSms){
		int i = 0;
		if(null != xtSms){
			xtSms.setXt_sms_id(toUUID());
			i=xtSmsService.addXtSms(xtSms);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 修改
	* @param xtSms
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个短信", notes="编辑单个短信")
	public BaseResult updateXtSms(@RequestBody XtSms xtSms){
		int i = 0;
		if(null != xtSms){
			i=xtSmsService.updateXtSms(xtSms);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_sms_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除短信", notes="删除短信")
	public BaseResult delXtSms(String xt_sms_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_sms_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_sms_id",xt_sms_id.split(","));
			i=xtSmsService.delXtSms(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
