package jehc.cloud.sys.init;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 初始化平台数据至缓存中
 * @author 邓纯杰
 *
 */
@Component
@Slf4j
@Order(-1)
public class InitData implements CommandLineRunner {
	@Autowired
	GlobalPersistentComponent globalPersistentComponent;
	@Override
	public void run(String... args) throws Exception {
		try {
			globalPersistentComponent.initXtDataDictionary();
			globalPersistentComponent.initXtAreaRegion();
			globalPersistentComponent.initPath();
			globalPersistentComponent.initXtIpFrozen();
			globalPersistentComponent.initXtConstant();
		} catch (Exception e) {
			log.error("加载异常：{}",e);
		}
	}
}
