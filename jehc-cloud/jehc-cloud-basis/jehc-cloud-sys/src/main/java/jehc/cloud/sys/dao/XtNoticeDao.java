package jehc.cloud.sys.dao;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.model.XtNotice;

/**
 * @Desc 平台公告
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtNoticeDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtNotice> getXtNoticeListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_notice_id 
	* @return
	*/
	XtNotice getXtNoticeById(String xt_notice_id);
	/**
	* 添加
	* @param xtNotice
	* @return
	*/
	int addXtNotice(XtNotice xtNotice);
	/**
	* 修改
	* @param xtNotice
	* @return
	*/
	int updateXtNotice(XtNotice xtNotice);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtNotice(Map<String,Object> condition);
	/**
	 * 统计
	 * @param condition
	 * @return
	 */
	int getXtNoticeCountByCondition(Map<String,Object> condition);
}
