package jehc.cloud.sys.service.impl;

import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import jehc.cloud.log.client.service.LogsUtil;
import jehc.cloud.sys.dao.XtPostDao;
import jehc.cloud.sys.model.XtPost;
import jehc.cloud.sys.service.XtPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 用户岗位
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtPostService")
public class XtPostServiceImpl extends BaseService implements XtPostService {

	@Autowired
	XtPostDao xtPostDao;

	@Autowired
	LogsUtil logsUtil;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtPost> getXtPostListByCondition(Map<String,Object> condition){
		try {
			return xtPostDao.getXtPostListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_post_id 
	* @return
	*/
	public XtPost getXtPostById(String xt_post_id){
		try {
			return xtPostDao.getXtPostById(xt_post_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtPost
	* @return
	*/
	public int addXtPost(XtPost xtPost){
		int i = 0;
		try {
			xtPost.setCreate_id(getXtUid());
			xtPost.setCreate_time(getDate());
			i = xtPostDao.addXtPost(xtPost);
			//统一推送
			addPushDataAuthority();
			logsUtil.aBLogs("岗位业务类", "添加", "添加岗位成功");
		} catch (Exception e) {
			logsUtil.aBLogs("岗位业务类", "添加", "添加岗位失败");
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtPost
	* @return
	*/
	public int updateXtPost(XtPost xtPost){
		int i = 0;
		try {
			xtPost.setUpdate_id(getXtUid());
			xtPost.setUpdate_time(getDate());
			i = xtPostDao.updateXtPost(xtPost);
			//统一推送
			addPushDataAuthority();
			logsUtil.aBLogs("岗位业务类", "修改", "修改岗位成功");
		} catch (Exception e) {
			logsUtil.aBLogs("岗位业务类", "修改", "修改岗位失败");
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtPost(Map<String,Object> condition){
		int i = 0;
		try {
			String[] xt_post_idList = (String[])condition.get("xt_post_id");
			//验证下级岗位是否存在
			for(String xt_post_id:xt_post_idList){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("xt_post_parentId", xt_post_id);
				map.put("del_flag", 0);
				if(!xtPostDao.getXtPostListChild(map).isEmpty()){
					logsUtil.aBLogs("岗位业务类", "删除", "执行删除岗位，编号【"+xt_post_id+"】存在下级岗位，不能删除");
					return 0;
				}
			}
			i = xtPostDao.delXtPost(condition);
			//统一推送
			addPushDataAuthority();
			logsUtil.aBLogs("岗位业务类", "删除", "删除岗位成功");
		} catch (Exception e) {
			logsUtil.aBLogs("岗位业务类", "删除", "删除岗位失败");
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	
	/**
	 * 岗位根目录集合
	 * @return
	 */
	public List<XtPost> getXtPostinfoList(Map<String,Object> condition){
		return xtPostDao.getXtPostinfoList(condition);
	}
	
	/**
	 * 查找子集合
	 * @return
	 */
	public List<XtPost> getXtPostListChild(Map<String,Object> condition){
		try {
			return xtPostDao.getXtPostListChild(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 查找所有集合
	 * @return
	 */
	public List<XtPost> getXtPostListAll(Map<String,Object> condition){
		try {
			return xtPostDao.getXtPostListAll(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 根据各种情况查找集合不分页（流程设计器中处理组 发起组等使用）
	 * @param condition
	 * @return
	 */
	public List<XtPost> getXtPostList(Map<String,Object> condition){
		return xtPostDao.getXtPostList(condition);
	}
	
	/**
	 * 非根岗位全部集合
	 * @param condition
	 * @return
	 */
	public List<XtPost> getXtPostinfoUnRootList(Map<String,Object> condition){
		return xtPostDao.getXtPostinfoUnRootList(condition);
	}
}
