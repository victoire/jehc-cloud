package jehc.cloud.sys.service.impl;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.dao.XtIpFrozenDao;
import jehc.cloud.sys.init.GlobalPersistentComponent;
import jehc.cloud.sys.model.XtIpFrozen;
import jehc.cloud.sys.service.XtIpFrozenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;

import javax.annotation.Resource;

/**
 * @Desc 平台IP冻结账户
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtIpFrozenService")
public class XtIpFrozenServiceImpl extends BaseService implements XtIpFrozenService {
	@Resource
	private XtIpFrozenDao xtIpFrozenDao;

	@Autowired
	GlobalPersistentComponent globalPersistentComponent;

	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtIpFrozen> getXtIpFrozenListByCondition(Map<String,Object> condition){
		try{
			return xtIpFrozenDao.getXtIpFrozenListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_ip_frozen_id 
	* @return
	*/
	public XtIpFrozen getXtIpFrozenById(String xt_ip_frozen_id){
		try{
			return xtIpFrozenDao.getXtIpFrozenById(xt_ip_frozen_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtIpFrozen
	* @return
	*/
	public int addXtIpFrozen(XtIpFrozen xtIpFrozen){
		int i = 0;
		try {
			xtIpFrozen.setCreate_id(getXtUid());
			xtIpFrozen.setCreate_time(getDate());
			xtIpFrozenDao.addXtIpFrozen(xtIpFrozen);
			globalPersistentComponent.initXtIpFrozen();
			i = 1;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtIpFrozen
	* @return
	*/
	public int updateXtIpFrozen(XtIpFrozen xtIpFrozen){
		int i = 0;
		try {
			xtIpFrozen.setUpdate_id(getXtUid());
			xtIpFrozen.setUpdate_time(getDate());
			xtIpFrozenDao.updateXtIpFrozen(xtIpFrozen);
			globalPersistentComponent.initXtIpFrozen();
			i = 1;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtIpFrozen(Map<String,Object> condition){
		int i = 0;
		try {
			xtIpFrozenDao.delXtIpFrozen(condition);
			globalPersistentComponent.initXtIpFrozen();
			i = 1;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	
	/**
	 * 获取所有集合
	 * @param condition
	 * @return
	 */
	public List<XtIpFrozen> getXtIpFrozenListAllByCondition(Map<String,Object> condition){
		try{
			return xtIpFrozenDao.getXtIpFrozenListAllByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
