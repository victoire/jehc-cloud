package jehc.cloud.sys.service.impl;
import java.util.List;
import java.util.Map;
import jehc.cloud.sys.dao.XtMessageDao;
import jehc.cloud.sys.model.XtMessage;
import jehc.cloud.sys.service.XtMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
/**
 * @Desc 短消息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtMessageService")
public class XtMessageServiceImpl extends BaseService implements XtMessageService {
	@Autowired
	private XtMessageDao xtMessageDao;
	/**
	 * 分页
	 * @param condition
	 * @return
	 */
	public List<XtMessage> getXtMessageListByCondition(Map<String,Object> condition){
		try{
			return xtMessageDao.getXtMessageListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	 * 查询对象
	 * @param xt_message_id
	 * @return
	 */
	public XtMessage getXtMessageById(String xt_message_id){
		try{
			return xtMessageDao.getXtMessageById(xt_message_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	 * 添加
	 * @param xtMessage
	 * @return
	 */
	public int addXtMessage(XtMessage xtMessage){
		int i = 0;
		try {
			xtMessage.setCreate_id(getXtUid());
			xtMessage.setCtime(getDate());
			i = xtMessageDao.addXtMessage(xtMessage);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 修改
	 * @param xtMessage
	 * @return
	 */
	public int updateXtMessage(XtMessage xtMessage){
		int i = 0;
		try {
			i = xtMessageDao.updateXtMessage(xtMessage);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 删除
	 * @param condition
	 * @return
	 */
	public int delXtMessage(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtMessageDao.delXtMessage(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 批量添加
	 * @param xtMessageList
	 * @return
	 */
	public int addBatchXtMessage(List<XtMessage> xtMessageList){
		int i = 0;
		try {
			i = xtMessageDao.addBatchXtMessage(xtMessageList);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 批量修改
	 * @param xtMessageList
	 * @return
	 */
	public int updateBatchXtMessage(List<XtMessage> xtMessageList){
		int i = 0;
		try {
			i = xtMessageDao.updateBatchXtMessage(xtMessageList);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 更新已读状态
	 * @param condition
	 * @return
	 */
	public int updateRead(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtMessageDao.updateRead(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	 * 统计
	 * @param condition
	 * @return
	 */
	public List<XtMessage> getXtMessageCountByCondition(Map<String,Object> condition){
		try{
			return xtMessageDao.getXtMessageCountByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}