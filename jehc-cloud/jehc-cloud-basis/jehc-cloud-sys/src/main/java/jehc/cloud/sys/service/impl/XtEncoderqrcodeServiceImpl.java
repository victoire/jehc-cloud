package jehc.cloud.sys.service.impl;

import java.util.List;
import java.util.Map;
import cn.hutool.core.collection.CollectionUtil;
import jehc.cloud.common.constant.SysContanst;
import jehc.cloud.common.util.QR.QRUtil;
import jehc.cloud.sys.dao.XtEncoderqrcodeDao;
import jehc.cloud.sys.model.XtEncoderqrcode;
import jehc.cloud.sys.service.XtEncoderqrcodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import javax.annotation.Resource;
/**
 * @Desc 平台二维码
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtEncoderqrcodeService")
public class XtEncoderqrcodeServiceImpl extends BaseService implements XtEncoderqrcodeService {
	@Resource
	private XtEncoderqrcodeDao xtEncoderqrcodeDao;

	@Autowired
	private QRUtil qrUtil;

	/**
	 * 分页
	 * @param condition
	 * @return
	 */
	public List<XtEncoderqrcode> getXtEncoderqrcodeListByCondition(Map<String,Object> condition){
		try{
			List<XtEncoderqrcode> encoderqrcodes = xtEncoderqrcodeDao.getXtEncoderqrcodeListByCondition(condition);
			if(CollectionUtil.isNotEmpty(encoderqrcodes)){
				for(XtEncoderqrcode encoderqrcode: encoderqrcodes){
					String base64 = qrUtil.encodeImageBase64( "png",encoderqrcode.getContent(), 430, 430, SysContanst.LOGO_BASE64);
					encoderqrcode.setQrImage(base64);
				}
			}
			return encoderqrcodes;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	 * 查询对象
	 * @param xt_encoderQRCode_id
	 * @return
	 */
	public XtEncoderqrcode getXtEncoderqrcodeById(String xt_encoderQRCode_id){
		try{
			XtEncoderqrcode encoderqrcode = xtEncoderqrcodeDao.getXtEncoderqrcodeById(xt_encoderQRCode_id);
			String base64 = qrUtil.encodeImageBase64( "png",encoderqrcode.getContent(), 430, 430, SysContanst.LOGO_BASE64);
			encoderqrcode.setQrImage(base64);
			return encoderqrcode;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	 * 添加
	 * @param xtEncoderqrcode
	 * @return
	 */
	public int addXtEncoderqrcode(XtEncoderqrcode xtEncoderqrcode){
		int i = 0;
		try {
//			String base64 = qrUtil.encodeImageBase64( "png",xtEncoderqrcode.getContent(), 430, 430, SysContanst.LOGO_BASE64);
//			String newName = AllUtils.getRandom()+".png";
//			String path = getXtPathCache("xt_encoderqrcode_default_path").get(0).getXt_path();
//			String relative_path = getXtPathCache("xt_encoderqrcode_relative_path").get(0).getXt_path();
//			File f = new File(path);
//			if(!f.exists()){
//				f.mkdirs();
//			}
			xtEncoderqrcode.setCreate_id(getXtUid());
			xtEncoderqrcode.setCreate_time(getDate());
////			twoDimensionCode.encoderQRCode(xtEncoderqrcode.getXt_encoderqrcode_url(), path+newName, "png");
//			XtAttachment xt_Attachment = new XtAttachment();
//			xt_Attachment.setXt_attachment_id(UUID.toUUID());
//			xt_Attachment.setXt_attachmentCtime(DateUtil.getSimpleDateFormat());
//			xt_Attachment.setXt_attachmentName(newName);
//			xt_Attachment.setXt_attachmentPath(relative_path+newName);
//			xt_Attachment.setXt_userinfo_id(getXtUid());
//			xt_Attachment.setXt_attachmentType("image/png");
//			xt_Attachment.setXt_attachmentTitle(xtEncoderqrcode.getXt_encoderqrcode_title());
//			xt_Attachment.setXt_userinfo_id(getXtUid());
//			xtAttachmentService.addXtAttachment(xt_Attachment);
//			xtEncoderqrcode.setXt_attachment_id(xt_Attachment.getXt_attachment_id());
			i = xtEncoderqrcodeDao.addXtEncoderqrcode(xtEncoderqrcode);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 修改
	 * @param xtEncoderqrcode
	 * @return
	 */
	public int updateXtEncoderqrcode(XtEncoderqrcode xtEncoderqrcode){
		int i = 0;
		try {
			xtEncoderqrcode.setUpdate_id(getXtUid());
			xtEncoderqrcode.setUpdate_time(getDate());
//			String newName = AllUtils.getRandom()+".png";
//			String path = getXtPathCache("xt_encoderqrcode_default_path").get(0).getXt_path();
//			String relative_path = getXtPathCache("xt_encoderqrcode_relative_path").get(0).getXt_path();
//			File f = new File(path);
//			if(!f.exists()){
//				f.mkdirs();
//			}
//			XtAttachment xt_Attachment = new XtAttachment();
//			xt_Attachment.setXt_attachment_id(UUID.toUUID());
//			xt_Attachment.setXt_attachmentCtime(getSimpleDateFormat());
//			xt_Attachment.setXt_attachmentName(newName);
//			xt_Attachment.setXt_attachmentPath(relative_path+newName);
//			xt_Attachment.setXt_userinfo_id(getXtUid());
//			xt_Attachment.setXt_attachmentType("image/png");
//			xt_Attachment.setXt_attachmentTitle(xtEncoderqrcode.getXt_encoderqrcode_title());
//			xt_Attachment.setXt_userinfo_id(getXtUid());
//			xtAttachmentService.addXtAttachment(xt_Attachment);
//			xtEncoderqrcode.setXt_attachment_id(xt_Attachment.getXt_attachment_id());
			i = xtEncoderqrcodeDao.updateXtEncoderqrcode(xtEncoderqrcode);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 删除
	 * @param condition
	 * @return
	 */
	public int delXtEncoderqrcode(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtEncoderqrcodeDao.delXtEncoderqrcode(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
