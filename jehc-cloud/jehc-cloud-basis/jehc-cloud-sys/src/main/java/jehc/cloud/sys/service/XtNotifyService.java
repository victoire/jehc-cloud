package jehc.cloud.sys.service;

import java.util.List;
import java.util.Map;

import jehc.cloud.sys.model.XtNotify;

/**
 * @Desc 通知
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtNotifyService {
	/**
	 * 初始化分页
	 * @param condition
	 * @return
	 */
	List<XtNotify> getXtNotifyListByCondition(Map<String, Object> condition);
	
	/**
	 * 查询对象
	 * @param xt_notify_id
	 * @return
	 */
	XtNotify getXtNotifyById(String xt_notify_id);
	
	/**
	 * 插入对象
	 * @param xtNotify
	 * @return
	 */
	int addXtNotify(XtNotify xtNotify);
	
	/***
	 * 删除
	 * @param condition
	 * @return
	 */
	int delXtNotify(Map<String, Object> condition);
}
