package jehc.cloud.sys.dao;

import java.util.List;
import java.util.Map;

import jehc.cloud.sys.model.XtNumber;

/**
 * @Desc 单号生成
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtNumberDao {
	/**
	 * 分页查询
	 * @param condition
	 * @return
	 */
	List<XtNumber> getXtNumberListByCondition(Map<String, Object> condition);
	
	/**
	 * 读取唯一一条记录根据类型
	 * @param modulesType
	 * @return
	 */
	XtNumber getXtNumberSingleByType(String modulesType);
	
	/**
	 * 添加
	 * @param xtNumber
	 * @return
	 */
	int addXtNumber(XtNumber xtNumber);
	
	/**
	 * 修改
	 * @param xtNumber
	 * @return
	 */
	int updateXtNumber(XtNumber xtNumber);
}
