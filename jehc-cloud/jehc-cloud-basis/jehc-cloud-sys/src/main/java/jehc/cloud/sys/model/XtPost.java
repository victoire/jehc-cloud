package jehc.cloud.sys.model;

import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 用户岗位
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtPost extends BaseEntity{
	private String xt_post_id;/**序列号**/
	private String xt_departinfo_id;/**部门编号**/
	private String xt_post_parentId;/**上级岗位**/
	private String xt_post_name;/**岗位名称**/
	private String xt_post_desc;/**岗位描述**/
	private int xt_post_maxNum;/**岗位最大人数**/
	private int xt_post_isLeaf;/**是否存在子级0表示存在**/
	private String xt_post_image;/**图片**/
	private int xt_post_grade;/**岗位级别**/
	private int xt_post_isdelete=0;/**是否删除0正常1删除**/
	private String xt_departinfo_name;/**部门名称**/
	private String xt_post_pname;//父级名称
	private String p_code;//岗位编码
}
