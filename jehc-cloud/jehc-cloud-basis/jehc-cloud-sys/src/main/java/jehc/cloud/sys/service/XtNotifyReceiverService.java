package jehc.cloud.sys.service;

import java.util.List;
import java.util.Map;

import jehc.cloud.sys.model.XtNotifyReceiver;

/**
 * @Desc 通知接收人
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtNotifyReceiverService {
	/**
	 * 初始化分页
	 * @param condition
	 * @return
	 */
	List<XtNotifyReceiver> getXtNotifyReceiverListByCondition(Map<String, Object> condition);
	
	XtNotifyReceiver getXtNotifyReceiverById(String notify_receiver_id);
	
	int delXtNotifyReceiver(Map<String, Object> condition);
	
	/**
	 * 根据通知编号查找集合
	 * @param notify_id
	 * @return
	 */
	List<XtNotifyReceiver> getXtNotifyReceiverListById(String notify_id);
	/**
	 * 更新已读
	 * @param condition
	 * @return
	 */
	int updateXtNotifyReceiver(Map<String, Object> condition);
}
