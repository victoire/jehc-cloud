package jehc.cloud.sys.dao;
import java.util.List;
import java.util.Map;
import jehc.cloud.sys.model.XtAreaRegion;

/**
 * @Desc 行政区划
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtAreaRegionDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtAreaRegion> getXtAreaRegionListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param ID 
	* @return
	*/
	XtAreaRegion getXtAreaRegionById(String ID);
	/**
	* 添加
	* @param xtAreaRegion
	* @return
	*/
	int addXtAreaRegion(XtAreaRegion xtAreaRegion);
	/**
	* 修改
	* @param xtAreaRegion
	* @return
	*/
	int updateXtAreaRegion(XtAreaRegion xtAreaRegion);
	/**
	* 修改（根据动态条件）
	* @param xtAreaRegion
	* @return
	*/
	int updateXtAreaRegionBySelective(XtAreaRegion xtAreaRegion);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtAreaRegion(Map<String,Object> condition);
	/**
	* 批量添加
	* @param xtAreaRegionList
	* @return
	*/
	int addBatchXtAreaRegion(List<XtAreaRegion> xtAreaRegionList);
	/**
	* 批量修改
	* @param xtAreaRegionList
	* @return
	*/
	int updateBatchXtAreaRegion(List<XtAreaRegion> xtAreaRegionList);
	/**
	* 批量修改（根据动态条件）
	* @param xtAreaRegionList
	* @return
	*/
	int updateBatchXtAreaRegionBySelective(List<XtAreaRegion> xtAreaRegionList);
}
