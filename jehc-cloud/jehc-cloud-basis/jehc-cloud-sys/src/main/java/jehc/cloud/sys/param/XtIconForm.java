package jehc.cloud.sys.param;

import lombok.Data;

@Data
public class XtIconForm {
    private String name;/**名称**/
    private String categories;/**分类**/
    private String icon;/**字体**/
    private String remark;/**备注**/
}
