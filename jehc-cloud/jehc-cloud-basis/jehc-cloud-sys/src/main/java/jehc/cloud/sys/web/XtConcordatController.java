package jehc.cloud.sys.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.sys.model.XtConcordat;
import jehc.cloud.sys.service.XtConcordatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

/**
 * @Desc 合同管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtConcordat")
@Api(value = "合同API",tags = "合同API",description = "合同API")
public class XtConcordatController extends BaseAction {
	@Autowired
	private XtConcordatService xtConcordatService;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询合同列表并分页", notes="查询合同列表并分页")
	public BasePage getXtConcordatListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtConcordat>XtConcordatList = xtConcordatService.getXtConcordatListByCondition(condition);
		PageInfo<XtConcordat> page = new PageInfo<XtConcordat>(XtConcordatList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单个合同
	* @param xt_concordat_id 
	* @param request 
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_concordat_id}")
	@ApiOperation(value="查询单个合同", notes="查询单个合同")
	public BaseResult getXtConcordatById(@PathVariable("xt_concordat_id") String xt_concordat_id, HttpServletRequest request){
		XtConcordat xt_Concordat = xtConcordatService.getXtConcordatById(xt_concordat_id);
		return outDataStr(xt_Concordat);
	}
	/**
	* 添加
	* @param xtConcordat
	* @param request 
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个合同", notes="创建单个合同")
	public BaseResult addXtConcordat(@RequestBody XtConcordat xtConcordat,HttpServletRequest request){
		int i = 0;
		if(null != xtConcordat){
			xtConcordat.setXt_concordat_id(toUUID());
			i=xtConcordatService.addXtConcordat(xtConcordat);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtConcordat
	* @param request 
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个合同", notes="编辑单个合同")
	public BaseResult updateXtConcordat(@RequestBody XtConcordat xtConcordat,HttpServletRequest request){
		int i = 0;
		if(null != xtConcordat){
			i=xtConcordatService.updateXtConcordat(xtConcordat);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_concordat_id 
	* @param request 
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除合同", notes="删除合同")
	public BaseResult delXtConcordat(String xt_concordat_id,HttpServletRequest request){
		int i = 0;
		if(null != xt_concordat_id && !"".equals(xt_concordat_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_concordat_id",xt_concordat_id.split(","));
			i=xtConcordatService.delXtConcordat(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
