package jehc.cloud.sys.model;

import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 平台二维码
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtEncoderqrcode extends BaseEntity{
	private String xt_encoderqrcode_id;/**二维码编号**/
	private String url;/**二维码链接地址**/
	private String title;/**标题**/
	private String content;/**备注**/
	private String xt_attachment_id;/**图片编号**/
	private String qrImage;
}
