package jehc.cloud.sys.dao;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.model.XtMessage;

/**
 * @Desc 短消息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtMessageDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtMessage> getXtMessageListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_message_id 
	* @return
	*/
	XtMessage getXtMessageById(String xt_message_id);
	/**
	* 添加
	* @param xtMessage
	* @return
	*/
	int addXtMessage(XtMessage xtMessage);
	/**
	* 修改
	* @param xtMessage
	* @return
	*/
	int updateXtMessage(XtMessage xtMessage);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtMessage(Map<String,Object> condition);
	/**
	* 批量添加
	* @param xtMessageList
	* @return
	*/
	int addBatchXtMessage(List<XtMessage> xtMessageList);
	/**
	* 批量修改
	* @param xtMessageList
	* @return
	*/
	int updateBatchXtMessage(List<XtMessage> xtMessageList);
	
	/**
	 * 更新已读状态
	 * @param condition
	 * @return
	 */
	int updateRead(Map<String,Object> condition);
	
	/**
	 * 分组统计
	 * @param condition
	 * @return
	 */
	List<XtMessage> getXtMessageCountByCondition(Map<String,Object> condition);
}
