package jehc.cloud.sys.web;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BaseResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.sf.json.JSONArray;
/**
 * 监控磁盘
 * @author 邓纯杰
 *
 */
@RestController
@RequestMapping("/xtdisksinfo")
@Api(value = "监控磁盘API",tags = "监控磁盘API",description = "监控磁盘API")
public class XtDisksController extends BaseAction {
	/**
	 * 获取当前服务器硬盘信息
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/info")
	@ApiOperation(value="当前服务器硬盘信息", notes="当前服务器硬盘信息")
	public BaseResult getDisksInfo(){
		JSONArray jsonArray = new JSONArray();  
		Map<String, Object> model = new HashMap<String, Object>();
		File[] arFileRoot = File.listRoots();
		for(int i=0;i<arFileRoot.length;i++){
			File rootFile = arFileRoot[i];
			//1盘名称
			String rootName = rootFile.getAbsolutePath().replaceAll(":\\\\","盘");
			//2是否可以读取
			if(rootFile.canRead()){
				model.put("isRead", "是");
			}else{
				model.put("isRead", "否");
			}
			//3可用空间大小
			long freeSpace = rootFile.getFreeSpace();
			//4总空间大小
			long totalSpace = rootFile.getTotalSpace();
			//5已用空间大小
			long usableSpace = rootFile.getUsableSpace();
			//6是否可写
			if(rootFile.canWrite()){
				model.put("isWrite", "是");
			}else{
				model.put("isWrite", "否");
			}
			if(totalSpace != 0){
				model.put("rootName", rootName);
//				model.put("freeSpace", (freeSpace)/(1024*1024*1024));
//				model.put("totalSpace", totalSpace/(1024*1024*1024));
//				model.put("usableSpace", (totalSpace/(1024*1024*1024))-(usableSpace)/(1024*1024*1024));
				model.put("freeSpace", (freeSpace)/(1024*1024));
				model.put("totalSpace", totalSpace/(1024*1024));
				model.put("usableSpace", (totalSpace/(1024*1024))-(usableSpace)/(1024*1024));
			}
			jsonArray.add(model);
		}
		return outItemsStr(jsonArray);
	}
}
