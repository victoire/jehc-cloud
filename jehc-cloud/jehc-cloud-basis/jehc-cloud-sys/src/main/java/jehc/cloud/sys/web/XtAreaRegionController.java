package jehc.cloud.sys.web;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.AuthUneedLogin;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseZTreeEntity;
import jehc.cloud.common.idgeneration.SnowflakeIdWorker;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.sys.model.XtAreaRegion;
import jehc.cloud.sys.service.XtAreaRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Desc 行政区划
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtAreaRegion")
@Api(value = "行政区划API",tags = "行政区划API",description = "行政区划API")
public class XtAreaRegionController extends BaseAction {
	@Autowired
	private XtAreaRegionService xtAreaRegionService;

	@Autowired
	SnowflakeIdWorker snowflakeIdWorker;
	/**
	 * 查询行政区划
	 * @param request
	 */
	@NeedLoginUnAuth
	@ApiOperation(value="查询行政区划", notes="查询行政区划")
	@GetMapping(value="/list")
	public BaseResult getXtAreaRegionListByCondition(HttpServletRequest request){
		List<BaseZTreeEntity> list = new ArrayList<BaseZTreeEntity>();
		Map<String,Object> condition = new HashMap<String,Object>();
		List<XtAreaRegion> xtAreaRegionList = xtAreaRegionService.getXtAreaRegionListByCondition(condition);
		for(int i = 0; i < xtAreaRegionList.size(); i++){
			XtAreaRegion xtAreaRegion = xtAreaRegionList.get(i);
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(xtAreaRegion.getID());
			BaseZTreeEntity.setPId(""+xtAreaRegion.getPARENT_ID());
			BaseZTreeEntity.setText(xtAreaRegion.getNAME());
			BaseZTreeEntity.setName(xtAreaRegion.getNAME());
			BaseZTreeEntity.setTempObject(""+xtAreaRegion.getREGION_LEVEL());
			BaseZTreeEntity.setContent(xtAreaRegion.getNAME_EN());
			BaseZTreeEntity.setIntegerappend("经度："+xtAreaRegion.getLONGITUDE()+"<br>纬度："+xtAreaRegion.getLATITUDE());
			list.add(BaseZTreeEntity);
		}
		BaseZTreeEntity baseZTreeEntity = new BaseZTreeEntity();
		List<BaseZTreeEntity> baseZTreeEntityList = baseZTreeEntity.buildTree(list,"0");
		String json = JsonUtil.toFastJson(baseZTreeEntityList);
		return outStr(json);
	}

	/**
	 * 查询单个行政区划
	 * @param ID
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/get/{ID}")
	@ApiOperation(value="查询单个行政区划", notes="根据Id查询单个行政区划")
	public BaseResult<XtAreaRegion> getXtAreaRegionById(@PathVariable("ID") String ID){
		XtAreaRegion xtAreaRegion = xtAreaRegionService.getXtAreaRegionById(ID);
		return BaseResult.success(xtAreaRegion);
	}

	/**
	 *查询全部行政区划
	 * @return
	 */
	@AuthUneedLogin
	@GetMapping(value="/lists")
	@ApiOperation(value="查询全部行政区划", notes="查询全部行政区划")
	public BaseResult lists(){
		Map<String,Object> condition = new HashMap<String,Object>();
		List<XtAreaRegion> xtAreaRegionList = xtAreaRegionService.getXtAreaRegionListByCondition(condition);
		return outDataStr(xtAreaRegionList);
	}

	/**
	 * 添加
	 * @param xtAreaRegion
	 */
	@PostMapping(value="/add")
	@ApiOperation(value="创建行政区划", notes="创建行政区划")
	public BaseResult addXtAreaRegion(@RequestBody XtAreaRegion xtAreaRegion){
		int i = 0;
		if(null != xtAreaRegion){
			xtAreaRegion.setID(""+snowflakeIdWorker.nextId());
			i=xtAreaRegionService.addXtAreaRegion(xtAreaRegion);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	 * 修改
	 * @param xtAreaRegion
	 */
	@PutMapping(value="/update")
	@ApiOperation(value="编辑行政区划", notes="编辑行政区划")
	public BaseResult updateXtAreaRegion(@RequestBody XtAreaRegion xtAreaRegion){
		int i = 0;
		if(null != xtAreaRegion && !StringUtil.isEmpty(xtAreaRegion.getID())){
			i=xtAreaRegionService.updateXtAreaRegion(xtAreaRegion);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	 * 删除
	 * @param ID
	 */
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除行政区划", notes="删除行政区划")
	public BaseResult delXtAreaRegion(String ID){
		int i = 0;
		if(!StringUtil.isEmpty(ID)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("ID",ID.split(","));
			i=xtAreaRegionService.delXtAreaRegion(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	 * 复制一行并生成记录
	 * @param ID
	 */
	@GetMapping(value="/copy")
	@ApiOperation(value="复制并生成行政区划", notes="复制并生成删除行政区划")
	public BaseResult copyXtAreaRegion(String ID){
		int i = 0;
		XtAreaRegion xtAreaRegion = xtAreaRegionService.getXtAreaRegionById(ID);
		if(null != xtAreaRegion){
			xtAreaRegion.setID(""+snowflakeIdWorker.nextId());
			i=xtAreaRegionService.addXtAreaRegion(xtAreaRegion);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}