package jehc.cloud.sys.web;

import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.*;
import jehc.cloud.common.entity.OauthAccountEntity;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.sys.model.XtPost;
import jehc.cloud.sys.service.XtPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 用户岗位
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtPost")
@Api(value = "用户岗位API",tags = "用户岗位API",description = "用户岗位API")
public class XtPostController extends BaseAction {
	@Autowired
	private XtPostService xtPostService;
	
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询岗位列表并分页", notes="查询岗位列表并分页")
	public BasePage getXtPostListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtPost> xtPostList = xtPostService.getXtPostListByCondition(condition);
		for(XtPost xtPost:xtPostList){
			if(!jehc.cloud.common.util.StringUtil.isEmpty(xtPost.getCreate_id())){
				OauthAccountEntity createBy = getAccount(xtPost.getCreate_id());
				if(null != createBy){
					xtPost.setCreateBy(createBy.getName());
				}
			}
			if(!jehc.cloud.common.util.StringUtil.isEmpty(xtPost.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(xtPost.getUpdate_id());
				if(null != modifiedBy){
					xtPost.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<XtPost> page = new PageInfo<XtPost>(xtPostList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	 * 一级岗位
	 * @param departId
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/one/level/lists/{departId}")
	@ApiOperation(value="一级岗位", notes="一级岗位")
	public BaseResult oneLevelLists(@PathVariable("departId")String departId){
		Map<String,Object> condition = new HashMap<>();
		condition.put("xt_departinfo_id",departId);
		List<XtPost> xt_Post = xtPostService.getXtPostinfoList(condition);
		return outDataStr(xt_Post);
	}

	/**
	* 查询单个岗位
	* @param xt_post_id
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_post_id}")
	@ApiOperation(value="查询单个岗位", notes="查询单个岗位")
	public BaseResult getXtPostById(@PathVariable("xt_post_id")String xt_post_id){
		XtPost xtPost = xtPostService.getXtPostById(xt_post_id);
		if(!jehc.cloud.common.util.StringUtil.isEmpty(xtPost.getCreate_id())){
			OauthAccountEntity createBy = getAccount(xtPost.getCreate_id());
			if(null != createBy){
				xtPost.setCreateBy(createBy.getName());
			}
		}
		if(!jehc.cloud.common.util.StringUtil.isEmpty(xtPost.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(xtPost.getUpdate_id());
			if(null != modifiedBy){
				xtPost.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(xtPost);
	}
	
	/**
	* 添加
	* @param xtPost
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个岗位", notes="创建单个岗位")
	public BaseResult addXtPost(@RequestBody XtPost xtPost){
		int i = 0;
		if(null != xtPost){
			xtPost.setXt_post_id(toUUID());
			if(StringUtil.isEmpty(xtPost.getXt_post_parentId())){
				xtPost.setXt_post_parentId("0");
			}
			i=xtPostService.addXtPost(xtPost);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 修改
	* @param xtPost
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个岗位", notes="编辑单个岗位")
	public BaseResult updateXtPost(@RequestBody XtPost xtPost){
		int i = 0;
		if(null != xtPost){
			if(StringUtil.isEmpty(xtPost.getXt_post_parentId())){
				xtPost.setXt_post_parentId("0");
			}
			i=xtPostService.updateXtPost(xtPost);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 删除
	* @param xt_post_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除岗位", notes="删除岗位")
	public BaseResult delXtPost(String xt_post_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_post_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_post_id",xt_post_id.split(","));
			i=xtPostService.delXtPost(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	 * 获取岗位树
	 * @param id
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/tree/{id}")
	@ApiOperation(value="获取岗位树", notes="获取岗位树")
	public BaseResult getXtPostTree(@PathVariable("id")String id){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<BaseTreeGridEntity> list = new ArrayList<BaseTreeGridEntity>();
		List<XtPost> xtPostList = xtPostService.getXtPostListAll(condition);
		for(int i = 0; i < xtPostList.size(); i++){
			XtPost xtPost = xtPostList.get(i);
			BaseTreeGridEntity BaseTreeGridEntity = new BaseTreeGridEntity();
			BaseTreeGridEntity.setId(xtPost.getXt_post_id());
			BaseTreeGridEntity.setPid(xtPost.getXt_post_parentId());
			BaseTreeGridEntity.setText(xtPost.getXt_post_name());
			BaseTreeGridEntity.setExpanded(true);
			BaseTreeGridEntity.setSingleClickExpand(true);
			BaseTreeGridEntity.setIcon("/deng/images/icons/target.png");
			list.add(BaseTreeGridEntity);
		}
		BaseTreeGridEntity baseTreeGridEntity = new BaseTreeGridEntity();
		List<BaseTreeGridEntity> baseTreeGridEntityList = baseTreeGridEntity.buildTree(list,"0");
		String json = JsonUtil.toFastJson(baseTreeGridEntityList);
		return outStr(json);
	}
	
	/**
	 * 获取岗位树（bootstrap-ztree风格）
	 * @param xt_departinfo_id 部门编号
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/bztree/{xt_departinfo_id}")
	@ApiOperation(value="获取岗位树（bootstrap-ztree风格）", notes="获取岗位树（bootstrap-ztree风格）")
	public BaseResult getXtPostBZTree(@PathVariable("xt_departinfo_id")String xt_departinfo_id){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("xt_departinfo_id", xt_departinfo_id);
		List<BaseZTreeEntity> list = new ArrayList<BaseZTreeEntity>();
		List<XtPost> xtPostList = xtPostService.getXtPostListByCondition(condition);
		for(int i = 0; i < xtPostList.size(); i++){
			XtPost xtPost = xtPostList.get(i);
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(xtPost.getXt_post_id());
			BaseZTreeEntity.setPId(xtPost.getXt_post_parentId());
			BaseZTreeEntity.setText(xtPost.getXt_post_name());
			BaseZTreeEntity.setName(xtPost.getXt_post_name());
			BaseZTreeEntity.setExpanded(true);
			BaseZTreeEntity.setSingleClickExpand(true);
			list.add(BaseZTreeEntity);
		}
		BaseZTreeEntity baseZTreeEntity = new BaseZTreeEntity();
		List<BaseZTreeEntity> baseZTreeEntityList = baseZTreeEntity.buildTree(list,"0");
		String json = JsonUtil.toFastJson(baseZTreeEntityList);
		return outStr(json);
	}
	
	/**
	 * 根据各种情况查找集合不分页（流程设计器中处理组 发起组等使用）
	 * @param xt_post_id
	 * @return
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/list/{xt_post_id}")
	@ApiOperation(value="根据各种情况查找集合不分页", notes="根据各种情况查找集合不分页")
	public BaseResult getXtPostList(@PathVariable("xt_post_id")String xt_post_id){
		List<XtPost> list = new ArrayList<XtPost>();
		if(!StringUtil.isEmpty(xt_post_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_post_id", xt_post_id.split(","));
			list = xtPostService.getXtPostList(condition);
		}
		return  outItemsStr(list);
	}
}
