package jehc.cloud.sys.service;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.model.XtConcordat;

/**
 * @Desc 合同管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtConcordatService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtConcordat> getXtConcordatListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_concordat_id 
	* @return
	*/
	XtConcordat getXtConcordatById(String xt_concordat_id);
	/**
	* 添加
	* @param xtConcordat
	* @return
	*/
	int addXtConcordat(XtConcordat xtConcordat);
	/**
	* 修改
	* @param xtConcordat
	* @return
	*/
	int updateXtConcordat(XtConcordat xtConcordat);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtConcordat(Map<String,Object> condition);
}
