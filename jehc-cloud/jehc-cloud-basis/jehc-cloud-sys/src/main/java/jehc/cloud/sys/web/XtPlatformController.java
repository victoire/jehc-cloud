package jehc.cloud.sys.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.entity.OauthAccountEntity;
import jehc.cloud.sys.service.XtPlatformFeedbackService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.sys.model.XtPlatform;
import jehc.cloud.sys.service.XtPlatformService;
/**
 * @Desc 平台信息发布
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtPlatform")
@Api(value = "平台信息发布API",tags = "平台信息发布API",description = "平台信息发布API")
public class XtPlatformController extends BaseAction{

	@Autowired
	private XtPlatformService xtPlatformService;

	@Autowired
	private XtPlatformFeedbackService xtPlatformFeedbackService;

	/**
	 * 查询并分页
	 * @param baseSearch
	 */
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getXtPlatformListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtPlatform> xtPlatformList = xtPlatformService.getXtPlatformListByCondition(condition);
		for(XtPlatform xtPlatform : xtPlatformList){
			if(!StringUtil.isEmpty(xtPlatform.getCreate_id())){
				OauthAccountEntity createBy = getAccount(xtPlatform.getCreate_id());
				if(null != createBy){
					xtPlatform.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(xtPlatform.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(xtPlatform.getUpdate_id());
				if(null != modifiedBy){
					xtPlatform.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<XtPlatform> page = new PageInfo<XtPlatform>(xtPlatformList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	 * 查询单条记录
	 * @param xt_platform_id
	 */
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_platform_id}")
	public BaseResult getXtPlatformById(@PathVariable("xt_platform_id")String xt_platform_id){
		XtPlatform xtPlatform = xtPlatformService.getXtPlatformById(xt_platform_id);
		if(!StringUtil.isEmpty(xtPlatform.getCreate_id())){
			OauthAccountEntity createBy = getAccount(xtPlatform.getCreate_id());
			if(null != createBy){
				xtPlatform.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(xtPlatform.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(xtPlatform.getUpdate_id());
			if(null != modifiedBy){
				xtPlatform.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(xtPlatform);
	}
	/**
	 * 添加
	 * @param xtPlatform
	 */
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addXtPlatform(@RequestBody XtPlatform xtPlatform){
		int i = 0;
		if(null != xtPlatform){
			xtPlatform.setXt_platform_id(toUUID());
			xtPlatform.setCreate_id(getXtUid());
			i=xtPlatformService.addXtPlatform(xtPlatform);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	 * 修改
	 * @param xtPlatform
	 */
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateXtPlatform(@RequestBody XtPlatform xtPlatform){
		int i = 0;
		if(null != xtPlatform){
			xtPlatform.setUpdate_id(getXtUid());
			xtPlatform.setUpdate_time(getDate());
			i=xtPlatformService.updateXtPlatform(xtPlatform);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	 * 删除
	 * @param xt_platform_id
	 */
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delXtPlatform(String xt_platform_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_platform_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_platform_id",xt_platform_id.split(","));
			i=xtPlatformService.delXtPlatform(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
