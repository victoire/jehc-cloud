package jehc.cloud.sys.model;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 平台知识内容
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtKnowledge extends BaseEntity{
	private String xt_knowledge_id;/**ID**/
	private String title;/**标题**/
	private String content;/**内容**/
	private String type;/**类型：0平台问题1学习知识**/
	private int state;/**状态：0待解决1已解决**/
	private int level;/**1紧急2正常3一般**/
}
