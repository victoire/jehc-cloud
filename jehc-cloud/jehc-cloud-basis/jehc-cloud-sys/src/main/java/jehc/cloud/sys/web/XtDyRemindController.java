package jehc.cloud.sys.web;

import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.sys.model.XtDyRemind;
import jehc.cloud.sys.model.XtMessage;
import jehc.cloud.sys.model.XtNotifyReceiver;
import jehc.cloud.sys.service.XtMessageService;
import jehc.cloud.sys.service.XtNotifyReceiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

/**
 * @Desc 全局动态提醒
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtdyremind")
@Api(value = "全局统一动态提醒API",tags = "全局统一动态提醒API",description = "全局统一动态提醒API")
public class XtDyRemindController extends BaseAction {
	@Autowired
	private XtNotifyReceiverService xtNotifyReceiverService;
	@Autowired
	private XtMessageService xtMessageService;

	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	* @param receive_status
	*/
	@PostMapping(value="/list")
	@NeedLoginUnAuth
	@ApiOperation(value="查询提醒信息", notes="查询提醒信息")
	public XtDyRemind getXtDyRemindList(@RequestBody(required=true)BaseSearch baseSearch, String receive_status){
		XtDyRemind xtDyRemind = new XtDyRemind();
		//处理通知
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		condition.put("xt_userinfo_id", getXtUid());
		condition.put("receive_status",0);
		List<XtNotifyReceiver> xtNotifyReceiverList = xtNotifyReceiverService.getXtNotifyReceiverListByCondition(condition);
		PageInfo<XtNotifyReceiver> page = new PageInfo<XtNotifyReceiver>(xtNotifyReceiverList);
		xtDyRemind.setXtNotifyReceiverList(page.getList());

		//处理短消息
		condition.put("type", "1");
		condition.put("to_id", getXtUid());
		condition.put("isread", 0);
		List<XtMessage> xt_MessageList = xtMessageService.getXtMessageListByCondition(condition);
		PageInfo<XtMessage> xtMessage = new PageInfo<XtMessage>(xt_MessageList);
		xtDyRemind.setXtMessageList(xtMessage.getList());
		return xtDyRemind;
	}
}
