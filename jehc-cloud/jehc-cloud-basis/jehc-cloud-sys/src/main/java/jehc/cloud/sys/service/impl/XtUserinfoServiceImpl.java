package jehc.cloud.sys.service.impl;
import java.util.List;
import java.util.Map;

import jehc.cloud.log.client.service.LogsUtil;
import jehc.cloud.sys.dao.XtUserinfoDao;
import jehc.cloud.sys.model.XtUserinfo;
import jehc.cloud.sys.service.XtUserinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;

/**
 * @Desc 员工信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtUserinfoService")
public class XtUserinfoServiceImpl extends BaseService implements XtUserinfoService {

	@Autowired
	XtUserinfoDao xtUserinfoDao;

	@Autowired
	LogsUtil logsUtil;

	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtUserinfo> getXtUserinfoListByCondition(Map<String,Object> condition){
		try {
			return xtUserinfoDao.getXtUserinfoListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_userinfo_id 
	* @return
	*/
	public XtUserinfo getXtUserinfoById(String xt_userinfo_id){
		try {
			return xtUserinfoDao.getXtUserinfoById(xt_userinfo_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtUserinfo
	* @return
	*/
	public int addXtUserinfo(XtUserinfo xtUserinfo){
		int i = 0;
		try {
			i = xtUserinfoDao.addXtUserinfo(xtUserinfo);
			//统一推送
			addPushDataAuthority();
			logsUtil.aBLogs("用户业务类", "添加", "执行添加操作");
		} catch (Exception e) {
			logsUtil.aBLogs("用户业务类", "添加", "执行添加操作--失败");
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtUserinfo
	* @return
	*/
	public int updateXtUserinfo(XtUserinfo xtUserinfo){
		int i = 0;
		try {
			XtUserinfo before = xtUserinfoDao.getXtUserinfoById(xtUserinfo.getXt_userinfo_id());
			i = xtUserinfoDao.updateXtUserinfo(xtUserinfo);
			//统一推送
			addPushDataAuthority();
			logsUtil.aBLogs("用户业务类", "修改", "执行修改操作");
			//记录字段变更日志
			logsUtil.aRecord(before, xtUserinfo, "XtUserinfo",xtUserinfo.getXt_userinfo_id());
		} catch (Exception e) {
			logsUtil.aBLogs("用户业务类", "修改", "执行修改操作--失败");
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtUserinfo(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtUserinfoDao.delXtUserinfo(condition);
			//统一推送
			addPushDataAuthority();
			logsUtil.aBLogs("用户业务类", "删除", "执行删除操作");
		} catch (Exception e) {
			logsUtil.aBLogs("用户业务类", "删除", "执行删除操作--失败");
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 登录
	 * @param condition
	 * @return
	 */
	public XtUserinfo getXtUserinfoByUP(Map<String,Object> condition){
		try {
			return xtUserinfoDao.getXtUserinfoByUP(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	 * 根据用户名查找对象
	 * @param userName
	 * @return
	 */
	public XtUserinfo getXtUserinfoByUserName(String userName){
		return xtUserinfoDao.getXtUserinfoByUserName(userName);
	}
	/**
	 * 读取所有用户根据各种情况非分页
	 * @param condition
	 * @return
	 */
	public List<XtUserinfo> getXtUserinfoListAllByCondition(Map<String,Object> condition){
		try {
			return xtUserinfoDao.getXtUserinfoListAllByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 修改密码
	 * @param condition
	 * @return
	 */
	public int updatePwd(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtUserinfoDao.updatePwd(condition);
			logsUtil.aBLogs("用户业务类", "修改密码", "执行修改密码操作");
		} catch (Exception e) {
			logsUtil.aBLogs("用户业务类", "修改密码", "执行修改密码操作--失败");
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	
	/**
	 * 验证用户名是否存在
	 * @return
	 */
	public int validateUser(Map<String,Object> condition){
		try {
			return xtUserinfoDao.validateUser(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 已删除用户
	 * @param condition
	 * @return
	 */
	public List<XtUserinfo> getXtUserinfoDeletedListByCondition(Map<String,Object> condition){
		try {
			return xtUserinfoDao.getXtUserinfoDeletedListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 恢复用户
	 * @param condition
	 * @return
	 */
	public int recoverXtUserinfo(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtUserinfoDao.recoverXtUserinfo(condition);
			//统一推送
			addPushDataAuthority();
			logsUtil.aBLogs("用户业务类", "恢复用户", "执行恢复用户操作");
		} catch (Exception e) {
			logsUtil.aBLogs("用户业务类", "恢复用户", "执行恢复用户操作--失败");
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	 * 根据各种情况查找集合不分页（流程设计器中处理人 发起人 发起人组及同步数据之授权中心等使用）
	 * @param condition
	 * @return
	 */
	public List<XtUserinfo> getXtUserinfoList(Map<String,Object> condition){
		try {
			return xtUserinfoDao.getXtUserinfoList(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
