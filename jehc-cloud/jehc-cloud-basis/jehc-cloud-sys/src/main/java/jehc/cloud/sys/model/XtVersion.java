package jehc.cloud.sys.model;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 平台版本
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtVersion extends BaseEntity{
	private String xt_version_id;/**平台版本编号**/
	private String name;/**版本名称**/
	private int down;/**下载次数**/
	private String remark;/**版本描述**/
	private String xt_attachment_id;/**附件编号**/
}
