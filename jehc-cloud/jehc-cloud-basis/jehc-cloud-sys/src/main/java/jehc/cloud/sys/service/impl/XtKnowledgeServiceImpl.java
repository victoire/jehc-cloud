package jehc.cloud.sys.service.impl;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.dao.XtKnowledgeDao;
import jehc.cloud.sys.model.XtKnowledge;
import jehc.cloud.sys.service.XtKnowledgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;

/**
 * @Desc 平台知识内容
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtKnowledgeService")
public class XtKnowledgeServiceImpl extends BaseService implements XtKnowledgeService {
	@Autowired
	private XtKnowledgeDao xtKnowledgeDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtKnowledge> getXtKnowledgeListByCondition(Map<String,Object> condition){
		try{
			return xtKnowledgeDao.getXtKnowledgeListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_knowledge_id 
	* @return
	*/
	public XtKnowledge getXtKnowledgeById(String xt_knowledge_id){
		try{
			return xtKnowledgeDao.getXtKnowledgeById(xt_knowledge_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtKnowledge
	* @return
	*/
	public int addXtKnowledge(XtKnowledge xtKnowledge){
		int i = 0;
		try {
			xtKnowledge.setCreate_id(getXtUid());
			xtKnowledge.setCreate_time(getDate());
			i = xtKnowledgeDao.addXtKnowledge(xtKnowledge);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtKnowledge
	* @return
	*/
	public int updateXtKnowledge(XtKnowledge xtKnowledge){
		int i = 0;
		try {
			xtKnowledge.setUpdate_time(getDate());
			xtKnowledge.setUpdate_id(getXtUid());
			i = xtKnowledgeDao.updateXtKnowledge(xtKnowledge);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtKnowledge(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtKnowledgeDao.delXtKnowledge(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 统计知识点数
	 * @param condition
	 * @return
	 */
	public int getXtKnowledgeCount(Map<String,Object> condition){
		return xtKnowledgeDao.getXtKnowledgeCount(condition);
	}
}
