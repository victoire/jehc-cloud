package jehc.cloud.log.client.service;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseUtils;
import jehc.cloud.common.entity.OauthAccountEntity;
import jehc.cloud.common.util.RestTemplateUtil;
import jehc.cloud.common.util.date.DateUtil;
import jehc.cloud.log.client.entity.*;
import jehc.cloud.log.client.util.IDGenerate;
import jehc.cloud.log.client.util.JsonUtil;
import jehc.cloud.log.client.worker.*;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import nl.bitwalker.useragentutils.Browser;
import nl.bitwalker.useragentutils.OperatingSystem;
import nl.bitwalker.useragentutils.UserAgent;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
/**
 * <p>
 * 系统日志
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Component
@Slf4j
public class LogsUtil {
    @Resource
    IDGenerate idGenerate;

    @Resource
    RestTemplateUtil restTemplateUtil;

    @Resource
    BaseUtils baseUtils;

    /**
     * 登录日志
     * @param logLoginDTO
     */
    public void loginLogs(LogLoginDTO logLoginDTO){
        try {
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
            logLoginDTO.setIp(request.getRemoteAddr());
            UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
            Browser browser = userAgent.getBrowser();
            OperatingSystem os = userAgent.getOperatingSystem();
            logLoginDTO.setBrowser_name(browser.getName());
            logLoginDTO.setBrowser_type(browser.getBrowserType().getName());
            logLoginDTO.setSystem(os.getName());
            logLoginDTO.setId(""+idGenerate.nextId());
            logLoginDTO.setCreate_time(baseUtils.getDate());
            if(null != userAgent.getBrowserVersion()){
                logLoginDTO.setBrowser_version(userAgent.getBrowserVersion().getVersion());
            }
            Runnable loginLogsRunnble = new LoginLogsRunnble(logLoginDTO,request);
            Thread thread = new Thread(loginLogsRunnble);
            thread.start();
        }catch (Exception e){
            log.error("添加登录日志异常：{}-{}",e,logLoginDTO);
        }
    }

    /**
     * 日志线程
     */
    class LoginLogsRunnble implements Runnable{
        LogLoginDTO logLoginDTO;
        HttpServletRequest request;
        public LoginLogsRunnble(){

        }

        public LoginLogsRunnble(LogLoginDTO logLoginDTO, HttpServletRequest request){
            this.logLoginDTO = logLoginDTO;
            this.request = request;
        }

        public void run() {
            BaseResult baseResult= restTemplateUtil.post(restTemplateUtil.restLogUrl() + "/logLogin/add",BaseResult.class,logLoginDTO,request);
            if(baseResult.getSuccess()){
                log.info("添加登录日志成功：{}",logLoginDTO);
            }else{
                log.info("添加登录日志失败：{}",logLoginDTO);
            }
        }
    }

    /**
     * 登录日志
     * @param result 登录结果
     */
    public void loginLogs(String result){
        LogLoginDTO logLoginDTO = new LogLoginDTO();
        try {
            //查询当前用户
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
            UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
            Browser browser = userAgent.getBrowser();
            OperatingSystem os = userAgent.getOperatingSystem();
            logLoginDTO.setBrowser_name(browser.getName());
            logLoginDTO.setBrowser_type(browser.getBrowserType().getName());
            logLoginDTO.setCreate_time(new Date());
            logLoginDTO.setSystem(os.getName());
            logLoginDTO.setId(""+idGenerate.nextId());
            logLoginDTO.setContent(result);//登录结果
            if(null != userAgent.getBrowserVersion()){
                logLoginDTO.setBrowser_version(userAgent.getBrowserVersion().getVersion());
            }
            Runnable runnable = new LogLoginWorker(logLoginDTO,request,restTemplateUtil);
            Thread thread = new Thread(runnable);
            thread.start();
        }catch (Exception e){
            log.error("处理登录日志异常：{}-{}",e,logLoginDTO);
        }
    }

    /**
     * 登出日志
     * @param
     */
    public void logoutLogs(){
        LogLoginDTO logLoginDTO = new LogLoginDTO();
        try {
            //查询当前用户
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
            UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
            Browser browser = userAgent.getBrowser();
            OperatingSystem os = userAgent.getOperatingSystem();
            logLoginDTO.setBrowser_name(browser.getName());
            logLoginDTO.setBrowser_type(browser.getBrowserType().getName());
            logLoginDTO.setCreate_time(new Date());
            logLoginDTO.setSystem(os.getName());
            logLoginDTO.setId(""+idGenerate.nextId());
            OauthAccountEntity oauthAccountEntity = baseUtils.getXtU();
            String message = "";
            if(null != oauthAccountEntity){
               message ="账号："+ oauthAccountEntity.getAccount()+",姓名："+oauthAccountEntity.getName();
               logLoginDTO.setCreate_id(oauthAccountEntity.getAccount_id());
            }
            logLoginDTO.setContent(message+"退出系统");
            if(null != userAgent.getBrowserVersion()){
                logLoginDTO.setBrowser_version(userAgent.getBrowserVersion().getVersion());
            }
            Runnable runnable = new LogLoginWorker(logLoginDTO,request,restTemplateUtil);
            Thread thread = new Thread(runnable);
            thread.start();
        }catch (Exception e){
            log.error("处理登录日志异常：{}-{}",e,logLoginDTO);
        }
    }

    /**
     * 业务日志 （仅限于拦截器使用）
     * @param param 参数
     * @param batch 批次
     */
    public void addOpLog(String param,Long batch){
        //查询当前用户
        OauthAccountEntity oauthAccountEntity = baseUtils.getXtU();
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
        LogOperateDTO logOperateDTO = new LogOperateDTO();
        logOperateDTO.setMethod(request.getMethod());
        logOperateDTO.setUri(request.getRequestURI());
        logOperateDTO.setCreate_time(new Date());
        logOperateDTO.setParam(param);
        logOperateDTO.setId(""+idGenerate.nextId());
        logOperateDTO.setType(1);
        if(null != oauthAccountEntity){
            logOperateDTO.setCreate_id(oauthAccountEntity.getAccount_id());
        }
        logOperateDTO.setBatch(""+batch);
        LogOperateWorker logOperateWorker = new LogOperateWorker(logOperateDTO,request,restTemplateUtil);
        new Thread(logOperateWorker).start();
    }

    /**
     * 业务日志
     * @param message 执行描述
     */
    public void addOpLog(String message){
        //查询当前用户
        OauthAccountEntity oauthAccountEntity = baseUtils.getXtU();
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
        LogOperateDTO logOperateDTO = new LogOperateDTO();
        logOperateDTO.setMethod(request.getMethod());
        logOperateDTO.setUri(request.getRequestURI());
        logOperateDTO.setCreate_time(new Date());
        logOperateDTO.setResult(message);
        logOperateDTO.setId(""+idGenerate.nextId());
        logOperateDTO.setType(0);
        if(null != oauthAccountEntity){
            logOperateDTO.setCreate_id(oauthAccountEntity.getAccount_id());
        }
        logOperateDTO.setBatch(""+idGenerate.nextId());
        LogOperateWorker operateLogWorker = new LogOperateWorker(logOperateDTO,request,restTemplateUtil);
        new Thread(operateLogWorker).start();
    }

    /**
     * 业务日志
     * @param modules 模块
     * @param message 执行描述
     */
    public void addOpLog(String modules,String message){
        OauthAccountEntity oauthAccountEntity = baseUtils.getXtU();
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
        LogOperateDTO logOperateDTO = new LogOperateDTO();
        logOperateDTO.setMethod(request.getMethod());
        logOperateDTO.setUri(request.getRequestURI());
        logOperateDTO.setCreate_time(new Date());
        logOperateDTO.setModules(modules);
        logOperateDTO.setResult(message);
        logOperateDTO.setId(""+idGenerate.nextId());
        logOperateDTO.setType(0);
        if(null != oauthAccountEntity){
            logOperateDTO.setCreate_id(oauthAccountEntity.getAccount_id());
        }
        logOperateDTO.setBatch(""+idGenerate.nextId());
        LogOperateWorker operateLogWorker = new LogOperateWorker(logOperateDTO,request,restTemplateUtil);
        new Thread(operateLogWorker).start();
    }

    /**
     * 业务日志
     * @param modules 执行描述
     * @param message 执行描述
     * @param parm 参数
     */
    public void addOpLog(String modules,String message,String parm){
        OauthAccountEntity oauthAccountEntity = baseUtils.getXtU();
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
        LogOperateDTO logOperateDTO = new LogOperateDTO();
        logOperateDTO.setMethod(request.getMethod());
        logOperateDTO.setUri(request.getRequestURI());
        logOperateDTO.setCreate_time(new Date());
        logOperateDTO.setModules(modules);
        logOperateDTO.setResult(message);
        logOperateDTO.setParam(parm);
        logOperateDTO.setId(""+idGenerate.nextId());
        logOperateDTO.setType(0);
        if(null != oauthAccountEntity){
            logOperateDTO.setCreate_id(oauthAccountEntity.getAccount_id());
        }
        logOperateDTO.setBatch(""+idGenerate.nextId());
        LogOperateWorker operateLogWorker = new LogOperateWorker(logOperateDTO,request,restTemplateUtil);
        new Thread(operateLogWorker).start();
    }

    /**
     * 执行变更记录
     * @param <T>
     * @param oldT
     * @param newT
     * @param modules
     * @param businessId
     */
    public <T> void aRecord(T oldT, T newT, String modules,String businessId){
        try {
            long batch = idGenerate.nextId();
            OauthAccountEntity oauthAccountEntity = baseUtils.getXtU();
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
            JSONObject oldJson = JsonUtil.toJsonObj(oldT);
            JSONObject newJson = JsonUtil.toJsonObj(newT);
            List<LogModifyRecordDTO> list = new ArrayList<LogModifyRecordDTO>();
            Iterator iterator = oldJson.keys();
            while(iterator.hasNext()){
                String key = (String) iterator.next();
                String oldV = oldJson.getString(key);
                String newV = newJson.getString(key);
                if(!oldV.equals(newV)){
                    LogModifyRecordDTO record = new LogModifyRecordDTO();
                    record.setAfter_value(""+newV);
                    record.setBefore_value(""+oldV);
                    record.setCreate_time(new Date());
                    record.setField(key);
                    record.setBusiness_id(businessId);
                    record.setModules(modules);
                    record.setId(""+idGenerate.nextId());
                    record.setBatch(""+batch);
                    if(null != oauthAccountEntity){
                        record.setCreate_id(oauthAccountEntity.getAccount_id());
                    }
                    record.setBatch(""+idGenerate.nextId());
                    list.add(record);
                }
            }
            LogModifyRecordWorker logModifyRecordWorker = new LogModifyRecordWorker(list,request,restTemplateUtil);
            new Thread(logModifyRecordWorker).start();
        } catch (Exception e) {
            log.error("记录变更日志异常：{}",e);
        }
    }

    /**
     * 执行变更记录并过滤字段
     * @param <T>
     * @param oldT
     * @param newT
     * @param modules
     * @param business_id
     * @param fieldList
     */
    public <T> void aRecord(T oldT, T newT, String modules,String business_id,List<String> fieldList){
        try {
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
            JSONObject oldJson = jehc.cloud.common.util.JsonUtil.toJsonObj(oldT);
            JSONObject newJson = jehc.cloud.common.util.JsonUtil.toJsonObj(newT);
            List<LogModifyRecordDTO> list = new ArrayList<LogModifyRecordDTO>();
            Iterator iterator = oldJson.keys();
            while(iterator.hasNext()){
                String key = (String) iterator.next();
                if(!fieldList.isEmpty() && fieldList.size() > 0){
                    for(String field:fieldList){
                        if(field.equals(key)){
                            String oldV = oldJson.getString(key);
                            String newV = newJson.getString(key);
                            if(!oldV.equals(newV)){
                                LogModifyRecordDTO record = new LogModifyRecordDTO();
                                record.setAfter_value(""+newV);
                                record.setBefore_value(""+oldV);
                                record.setCreate_time(DateUtil.getDate());
                                record.setField(key);
                                record.setModules(modules);
                                list.add(record);
                            }
                        }
                    }
                }
            }
            OauthAccountEntity oauthAccountEntity = baseUtils.getXtU();
            String userId = null;
            if(null != oauthAccountEntity){
                userId = oauthAccountEntity.getAccount_id();
            }
            for(int i = 0; i < list.size(); i++){
                list.get(i).setId(""+idGenerate.nextId());
                list.get(i).setBusiness_id(business_id);
                list.get(i).setCreate_id(userId);
            }
            LogModifyRecordWorker logModifyRecordWorker = new LogModifyRecordWorker(list,request,restTemplateUtil);
            new Thread(logModifyRecordWorker).start();
        } catch (Exception e) {
        }
    }

    /**
     * 创建异常日志
     * @param logErrorDTO
     */
    public void aLogError(LogErrorDTO logErrorDTO){
        try {
            //查询当前用户
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
            logErrorDTO.setCreate_time(new Date());
            logErrorDTO.setId(""+idGenerate.nextId());
            OauthAccountEntity oauthAccountEntity = baseUtils.getXtU();
            if(null != oauthAccountEntity){
                logErrorDTO.setCreate_id(oauthAccountEntity.getAccount_id());
            }
            Runnable runnable = new LogErrorWorker(logErrorDTO,request,restTemplateUtil);
            Thread thread = new Thread(runnable);
            thread.start();
        }catch (Exception e){
            log.error("处理”异常“日志异常：{}-{}",e,logErrorDTO);
        }
    }

    /**
     * 创建加载页面监控日志
     * @param logLoadinfoDTO
     */
    public void aLogLoadinfo(LogLoadinfoDTO logLoadinfoDTO){
        try {
            //查询当前用户
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
            logLoadinfoDTO.setCreate_time(new Date());
            logLoadinfoDTO.setId(""+idGenerate.nextId());
            OauthAccountEntity oauthAccountEntity = baseUtils.getXtU();
            if(null != oauthAccountEntity){
                logLoadinfoDTO.setCreate_id(oauthAccountEntity.getAccount_id());
            }
            Runnable runnable = new LogLoadinfoWorker(logLoadinfoDTO,request,restTemplateUtil);
            Thread thread = new Thread(runnable);
            thread.start();
        }catch (Exception e){
            log.error("处理”页面加载监控“日志异常：{}-{}",e,logLoadinfoDTO);
        }
    }


    /**
     * 创建启动或关闭服务日志
     * @param logStartStopDTO
     */
    public void aLogStartStop(LogStartStopDTO logStartStopDTO){
        try {
            //查询当前用户
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
            logStartStopDTO.setCreate_time(new Date());
            logStartStopDTO.setId(""+idGenerate.nextId());
            OauthAccountEntity oauthAccountEntity = baseUtils.getXtU();
            if(null != oauthAccountEntity){
                logStartStopDTO.setCreate_id(oauthAccountEntity.getAccount_id());
            }
            Runnable runnable = new LogStartStopWorker(logStartStopDTO,request,restTemplateUtil);
            Thread thread = new Thread(runnable);
            thread.start();
        }catch (Exception e){
            log.error("处理”服务启动或关闭“日志异常：{}-{}",e,logStartStopDTO);
        }
    }

    /**
     * 添加平台业务操作日志通用 采用put方法目的不走事务控制
     * @param classname
     * @param method
     * @param message
     */
    public void aBLogs(String classname,String method,String message){
        OauthAccountEntity oauthAccountEntity = baseUtils.getXtU();
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
        LogOperateDTO logOperateDTO = new LogOperateDTO();
        logOperateDTO.setClass_name(classname);
        logOperateDTO.setMethod(method);
        logOperateDTO.setUri(request.getRequestURI());
        logOperateDTO.setCreate_time(new Date());
        logOperateDTO.setResult(message);
        logOperateDTO.setId(""+idGenerate.nextId());
        logOperateDTO.setType(0);
        if(null != oauthAccountEntity){
            logOperateDTO.setCreate_id(oauthAccountEntity.getAccount_id());
        }
        logOperateDTO.setBatch(""+idGenerate.nextId());
        LogOperateWorker operateLogWorker = new LogOperateWorker(logOperateDTO,request,restTemplateUtil);
        new Thread(operateLogWorker).start();
    }

    /**
     * 添加平台业务操作日志通用 采用put方法目的不走事务控制
     * @param classname
     * @param method
     * @param message
     * @param parm
     */
    public void aBLogs(String classname,String method,String message,String parm){
        OauthAccountEntity oauthAccountEntity = baseUtils.getXtU();
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
        LogOperateDTO logOperateDTO = new LogOperateDTO();
        logOperateDTO.setClass_name(classname);
        logOperateDTO.setMethod(method);
        logOperateDTO.setUri(request.getRequestURI());
        logOperateDTO.setCreate_time(new Date());
        logOperateDTO.setResult(message);
        logOperateDTO.setId(""+idGenerate.nextId());
        logOperateDTO.setParam(parm);
        logOperateDTO.setType(0);
        if(null != oauthAccountEntity){
            logOperateDTO.setCreate_id(oauthAccountEntity.getAccount_id());
        }
        logOperateDTO.setBatch(""+idGenerate.nextId());
        LogOperateWorker operateLogWorker = new LogOperateWorker(logOperateDTO,request,restTemplateUtil);
        new Thread(operateLogWorker).start();
    }
}
