package jehc.cloud.log.client.worker;

import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.util.RestTemplateUtil;
import jehc.cloud.log.client.entity.LogLoadinfoDTO;
import lombok.extern.slf4j.Slf4j;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 页面监控日志工作线程
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Slf4j
public class LogLoadinfoWorker implements Runnable {

    LogLoadinfoDTO logLoadinfoDTO;//页面监控日志信息

    HttpServletRequest request;//请求

    RestTemplateUtil restTemplateUtil;//restTemplateUtil工具类


    public LogLoadinfoWorker(){

    }

    /**
     *
     * @param logLoadinfoDTO
     * @param request
     * @param restTemplateUtil
     */
    public LogLoadinfoWorker(LogLoadinfoDTO logLoadinfoDTO, HttpServletRequest request, RestTemplateUtil restTemplateUtil){
        this.logLoadinfoDTO = logLoadinfoDTO;
        this.request = request;
        this.restTemplateUtil = restTemplateUtil;
    }

    /**
     *
     */
    public void run(){
        try {
            send(request);
        } catch (Exception e) {
            log.info("记录页面监控日志失败:"+logLoadinfoDTO+"，异常信息：{}",e);
        }
    }

    /**
     *
     * @param request
     */
    public void send(HttpServletRequest request){
        try {
            log.info("记录页面监控日志失败开始");
            restTemplateUtil.post(restTemplateUtil.restLogUrl()+"/logLoadinfo/add", BaseResult.class,logLoadinfoDTO);
            log.info("记录页面监控日志失败结束");
        } catch (Exception e) {
            log.info("记录页面监控日志失败失败:"+logLoadinfoDTO+"，异常信息：{}",e);
        }
    }
}
