package jehc.cloud.log.client.util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * @Desc JSON转换工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class JsonUtil {
    /**
     * Convert objects to JSON strings
     * @param obj Object to be converted
     * @return The string character of the object
     */
    public static String toJson(Object obj) {
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class,new JsonDateValueProcessor());
        JSONObject jSONObject = JSONObject.fromObject(obj,jsonConfig);
        return jSONObject.toString();
    }

    /**
     * Convert objects to JSON strings
     * @param obj Object to be converted
     * @return The string character of the object
     */
    public static JSONObject toJsonObj(Object obj) {
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class,new JsonDateValueProcessor());
        JSONObject jSONObject = JSONObject.fromObject(obj,jsonConfig);
        return jSONObject;
    }

//    /**
//     * Convert JSON strings to objects
//     * @param jsonString String to be converted
//     * @param type The type of object that needs to be converted
//     * @return object
//     */
//    public static <T> T fromJson(String jsonString, Class<T> type) {
//        JsonConfig jsonConfig = new JsonConfig();
//        jsonConfig.registerJsonValueProcessor(Date.class,new JsonDateValueProcessor());
//        JSONObject jsonObject = JSONObject.fromObject(jsonString,jsonConfig);
//        return (T) JSONObject.toBean(jsonObject, type);
//    }

//    /**
//     * Convert JSON strings to objects
//     * @param jsonString String to be converted
//     * @param type The type of object that needs to be converted
//     * @return object
//     */
//    public static <T> T fromJson(String jsonString, Class<T> type,Map map) {
//        JsonConfig jsonConfig = new JsonConfig();
//        jsonConfig.registerJsonValueProcessor(Date.class,new JsonDateValueProcessor());
//        JSONObject jsonObject = JSONObject.fromObject(jsonString,jsonConfig);
//        return (T) JSONObject.toBean(jsonObject, type,map);
//    }

    /**
     * Convert a collection according to a string
     * @param <T>
     * @param text
     * @param clazz
     * @return
     */
    public static <T> List<T> toList(String text, Class<T> clazz) {
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class,new JsonDateValueProcessor());
        JSONArray jsonArray = JSONArray.fromObject(text.toString(),jsonConfig);
        List<T> tList = JSONArray.toList(jsonArray,clazz);
        return tList;
    }

    /**
     *
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> toJsonList(String json, Object clazz) {
        if(StringUtils.isEmpty(json)){
            return null;
        }
        JsonConfig jsonCfg = new JsonConfig();
        //register date
        jsonCfg.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
        JSONArray jsonArray = JSONArray.fromObject(json.toString());
        List<T> tList = JSONArray.toList(jsonArray,clazz ,jsonCfg);
        return tList;
    }


    /**
     * Convert a JSONArray object to a list collection
     * @param jsonArr
     * @return
     */
    public static List<Object> jsonToList(JSONArray jsonArr) {
        List<Object> list = new ArrayList<Object>();
        for (Object obj : jsonArr) {
            if (obj instanceof JSONArray) {
                list.add(jsonToList((JSONArray) obj));
            } else if (obj instanceof JSONObject) {
                list.add(jsonToMap((JSONObject) obj));
            } else {
                list.add(obj);
            }
        }
        return list;
    }

    /**
     * Convert a json string to a map object
     * @param json
     * @return
     */
    public static Map<String, Object> jsonToMap(String json) {
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class,new JsonDateValueProcessor());
        JSONObject jsonObject = JSONObject.fromObject(json,jsonConfig);
        return jsonToMap(jsonObject);
    }

    /**
     * Convert JSONObject to map object
     * @param obj
     * @return
     */
    public static Map<String, Object> jsonToMap(JSONObject obj) {
        Set<?> set = obj.keySet();
        Map<String, Object> map = new HashMap<String, Object>(set.size());
        for (Object key : obj.keySet()) {
            Object value = obj.get(key);
            if (value instanceof JSONArray) {
                map.put(key.toString(), jsonToList((JSONArray) value));
            } else if (value instanceof JSONObject) {
                map.put(key.toString(), jsonToMap((JSONObject) value));
            } else {
                map.put(key.toString(), obj.get(key));
            }
        }
        return map;
    }
}
