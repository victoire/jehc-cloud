package jehc.cloud.log.client.worker;

import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.util.RestTemplateUtil;
import jehc.cloud.log.client.entity.LogErrorDTO;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 异常日志工作线程
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Slf4j
public class LogErrorWorker implements Runnable {
    LogErrorDTO logErrorDTO;//异常日志信息

    HttpServletRequest request;//请求

    RestTemplateUtil restTemplateUtil;//restTemplateUtil工具类


    public LogErrorWorker(){

    }

    /**
     *
     * @param logErrorDTO
     * @param request
     * @param restTemplateUtil
     */
    public LogErrorWorker(LogErrorDTO logErrorDTO, HttpServletRequest request, RestTemplateUtil restTemplateUtil){
        this.logErrorDTO = logErrorDTO;
        this.request = request;
        this.restTemplateUtil = restTemplateUtil;
    }

    /**
     *
     */
    public void run(){
        try {
            send(request);
        } catch (Exception e) {
            log.info("记录异常日志失败:"+logErrorDTO+"，异常信息：{}",e);
        }
    }

    /**
     *
     * @param request
     */
    public void send(HttpServletRequest request){
        try {
            log.info("记录异常日志失败开始");
            restTemplateUtil.post(restTemplateUtil.restLogUrl()+"/logError/add", BaseResult.class,logErrorDTO);
            log.info("记录异常日志失败结束");
        } catch (Exception e) {
            log.info("记录异常日志失败失败:"+logErrorDTO+"，异常信息：{}",e);
        }
    }
}
