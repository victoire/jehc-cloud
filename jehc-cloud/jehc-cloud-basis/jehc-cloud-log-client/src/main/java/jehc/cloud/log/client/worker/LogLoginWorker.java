package jehc.cloud.log.client.worker;

import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.util.RestTemplateUtil;
import jehc.cloud.log.client.entity.LogLoginDTO;
import lombok.extern.slf4j.Slf4j;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 登录日志工作线程
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Slf4j
public class LogLoginWorker implements Runnable{

    LogLoginDTO logLoginDTO;//登录日志信息

    HttpServletRequest request;//请求

    RestTemplateUtil restTemplateUtil;//restTemplateUtil工具类


    public LogLoginWorker(){

    }

    /**
     *
     * @param logLoginDTO
     * @param request
     * @param restTemplateUtil
     */
    public LogLoginWorker(LogLoginDTO logLoginDTO, HttpServletRequest request, RestTemplateUtil restTemplateUtil){
        this.logLoginDTO = logLoginDTO;
        this.request = request;
        this.restTemplateUtil = restTemplateUtil;
    }

    /**
     *
     */
    public void run(){
        try {
            send(request);
        } catch (Exception e) {
            log.info("记录登录日志失败:"+logLoginDTO+"，异常信息：{}",e);
        }
    }

    /**
     *
     * @param request
     */
    public void send(HttpServletRequest request){
        try {
            log.info("记录登录日志失败开始");
            restTemplateUtil.post(restTemplateUtil.restLogUrl()+"/logLogin/add", BaseResult.class,logLoginDTO);
            log.info("记录登录日志失败结束");
        } catch (Exception e) {
            log.info("记录登录日志失败失败:"+logLoginDTO+"，异常信息：{}",e);
        }
    }
}
