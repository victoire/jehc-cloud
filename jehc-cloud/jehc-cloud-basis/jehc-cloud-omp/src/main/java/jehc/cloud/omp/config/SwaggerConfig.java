package jehc.cloud.omp.config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo()).select()
//                .apis(RequestHandlerSelectors.basePackage("jehc.cloud.*.web"))
                .paths(PathSelectors.any()).build();
    }

    private ApiInfo apiInfo() {
        Contact contact = new Contact("jehc", "", "");
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("jEhc微服务构建API文档")//大标题
                .description("REST风格API")//小标题
                .contact(contact)//作者
                .termsOfServiceUrl("")
                .version("V1.0.0")//版本
                .license("主页")//链接显示文字
                .licenseUrl("")//网站链接
                .build();
        return apiInfo;
    }
}