package jehc.cloud.awstats.service;

import jehc.cloud.awstats.model.AuthLog;

import java.util.List;
import java.util.Map;

/**
 * @Desc 授权耗时日志传输对象
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface AuthLogService {
    /**
     * 初始化分页
     * @param condition
     * @return
     */
    List<AuthLog> getAuthLogListByCondition(Map<String, Object> condition);

    /**
     * 查询对象
     * @param id
     * @return
     */
    AuthLog getAuthLogById(String id);

    /**
     * 批量添加
     * @param authLogs
     * @return
     */
    int addAuthLogBatch(List<AuthLog> authLogs);


    /**
     * 删除
     * @param condition
     * @return
     */
    int delAuthLog(Map<String, Object> condition);
}
