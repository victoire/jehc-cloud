package jehc.cloud.route.dao;

import jehc.cloud.route.model.DynamicRouteModules;
import java.util.List;
import java.util.Map;

/**
 * @Desc 路由模块
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface DynamicRouteModulesDao {

    /**
     * 查询列表
     * @param condition
     * @return
     */
    List<DynamicRouteModules> getDynamicRouteModulesListByCondition(Map<String, Object> condition);

    /**
     * 查询对象
     * @param id
     * @return
     */
    DynamicRouteModules getDynamicRouteModulesById(String id);

    /**
     * 添加
     * @param dynamicRouteModules
     * @return
     */
    int addDynamicRouteModules(DynamicRouteModules dynamicRouteModules);

    /**
     * 修改
     * @param dynamicRouteModules
     * @return
     */
    int updateDynamicRouteModules(DynamicRouteModules dynamicRouteModules);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delDynamicRouteModules(Map<String, Object> condition);
}
