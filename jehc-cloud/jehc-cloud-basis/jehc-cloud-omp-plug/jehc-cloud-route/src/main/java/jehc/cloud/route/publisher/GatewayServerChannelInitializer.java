package jehc.cloud.route.publisher;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;
import org.springframework.stereotype.Component;

/**
 * @Desc I/O事件处理类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
public class GatewayServerChannelInitializer extends ChannelInitializer<SocketChannel> {
    GatewayChannelUtil gatewayChannelUtil;
    public GatewayServerChannelInitializer(){

    }
    public GatewayServerChannelInitializer(GatewayChannelUtil gatewayChannelUtil){
        this.gatewayChannelUtil = gatewayChannelUtil;
    }
    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        //添加编解码
        socketChannel.pipeline().addLast("decoder", new StringDecoder(CharsetUtil.UTF_8));
        socketChannel.pipeline().addLast("encoder", new StringEncoder(CharsetUtil.UTF_8));
        socketChannel.pipeline().addLast(new GatewayNettyServerHandler(gatewayChannelUtil));
    }
}
