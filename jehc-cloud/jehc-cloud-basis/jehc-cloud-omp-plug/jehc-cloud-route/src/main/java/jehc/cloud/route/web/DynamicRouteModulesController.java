package jehc.cloud.route.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.Auth;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.route.model.DynamicRouteModules;
import jehc.cloud.route.service.DynamicRouteModulesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 路由模块
 * 2015-05-24 08:36:53  邓纯杰
 */
@RestController
@RequestMapping("/dynamicRouteModules")
@Api(value = "路由模块",tags = "路由模块",description = "路由模块")
public class DynamicRouteModulesController extends BaseAction {
    @Autowired
    DynamicRouteModulesService dynamicRouteModulesService;

    /**
     * 查询路由模块列表并分页
     * @param baseSearch
     */
    @PostMapping(value="/list")
    @NeedLoginUnAuth
    @ApiOperation(value="查询路由模块列表并分页", notes="查询路由模块列表并分页")
    public BasePage getDynamicRouteModulesListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<DynamicRouteModules> dynamicRouteModulesList = dynamicRouteModulesService.getDynamicRouteModulesListByCondition(condition);
        PageInfo<DynamicRouteModules> page = new PageInfo<DynamicRouteModules>(dynamicRouteModulesList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个路由模块
     * @param id
     */
    @NeedLoginUnAuth
    @GetMapping(value="/get/{id}")
    @ApiOperation(value="查询单个路由模块", notes="查询单个路由模块")
    public BaseResult<DynamicRouteModules> getDynamicRouteModulesById(@PathVariable("id")String id){
        DynamicRouteModules dynamicRouteModules = dynamicRouteModulesService.getDynamicRouteModulesById(id);
        return BaseResult.success(dynamicRouteModules);
    }

    /**
     * 添加
     * @param dynamicRouteModules
     * @return
     */
    @PostMapping(value="/add")
    @Auth("/dynamicRouteModules/add")
    @ApiOperation(value="添加", notes="添加")
    public BaseResult addDynamicRouteModules(@RequestBody DynamicRouteModules dynamicRouteModules){
        int i = 0;
        if(null != dynamicRouteModules){
            i=dynamicRouteModulesService.addDynamicRouteModules(dynamicRouteModules);
        }
        if(i>0){
            return BaseResult.success();
        }else{
            return BaseResult.fail();
        }
    }

    /**
     * 编辑
     * @param dynamicRouteModules
     */
    @PutMapping(value="/update")
    @Auth("/dynamicRouteModules/update")
    @ApiOperation(value="编辑", notes="编辑")
    public BaseResult updateDynamicRouteModules(@RequestBody DynamicRouteModules dynamicRouteModules){
        int i = 0;
        if(null !=dynamicRouteModules){
            i=dynamicRouteModulesService.updateDynamicRouteModules(dynamicRouteModules);
        }
        if(i>0){
            return BaseResult.success();
        }else{
            return BaseResult.fail();
        }
    }

    /**
     * 删除
     * @param id
     */
    @DeleteMapping(value="/delete")
    @Auth("/dynamicRouteModules/delete")
    @ApiOperation(value="删除", notes="删除")
    public BaseResult delDynamicRouteModules(String id){
        int i = 0;
        if(!StringUtil.isEmpty(id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id",id.split(","));
            i=dynamicRouteModulesService.delDynamicRouteModules(condition);
        }
        if(i>0){
            return BaseResult.success();
        }else{
            return BaseResult.fail();
        }
    }

    /**
     * 查询路由模块列表
     */
    @GetMapping(value="/listAll")
    @NeedLoginUnAuth
    @ApiOperation(value="查询路由模块列表", notes="查询路由模块列表")
    public BaseResult<List<DynamicRouteModules>> getDynamicRouteModulesList(){
        Map<String,Object> condition = new HashMap<>();
        List<DynamicRouteModules> dynamicRouteModulesList = dynamicRouteModulesService.getDynamicRouteModulesListByCondition(condition);
        return BaseResult.success(dynamicRouteModulesList);
    }
}
