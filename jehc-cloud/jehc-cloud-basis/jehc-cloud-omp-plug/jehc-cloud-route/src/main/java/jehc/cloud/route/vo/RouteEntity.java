package jehc.cloud.route.vo;

import lombok.Data;

/**
 * @Desc 路由传输参数
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class RouteEntity {
    private String id;
    private String method;

    public RouteEntity(){

    }

    /**
     *
     * @param id
     * @param method
     */
    public RouteEntity(String id,String method){
        this.id = id;
        this.method = method;
    }
}
