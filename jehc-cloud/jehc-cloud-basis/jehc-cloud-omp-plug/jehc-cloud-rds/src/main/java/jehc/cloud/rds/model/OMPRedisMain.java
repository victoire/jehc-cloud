package jehc.cloud.rds.model;

import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc Redis主监控信息日志
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OMPRedisMain extends BaseEntity {
    private String id;
    private String config_id;//配置id
    private String info;//监控信息

    private String name;//连接name
    private String host;//连接ip
}
