package jehc.cloud.scms.service;

import jehc.cloud.scms.model.SCMSMonitorMem;

import java.util.List;
import java.util.Map;

/**
 * @Desc 服务器内存
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface SCMSMonitorMemService {

    /**
     * 初始化分页
     * @param condition
     * @return
     */
    List<SCMSMonitorMem> getSCMSMonitorMemListByCondition(Map<String,Object> condition);

    /**
     * 查询对象
     * @param id
     * @return
     */
    SCMSMonitorMem getSCMSMonitorMemById(String id);

    /**
     * 新增
     * @param scmsMonitorMem
     * @return
     */
    int addSCMSMonitorMem(SCMSMonitorMem scmsMonitorMem);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delSCMSMonitorMem(Map<String,Object> condition);

    /**
     * 查询列表
     * @param scmsMonitorMem
     * @return
     */
    List<SCMSMonitorMem> getSCMSMonitorMemList(SCMSMonitorMem scmsMonitorMem);
}
