package jehc.cloud.job.service.impl;
import java.util.List;
import java.util.Map;

import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import jehc.cloud.job.model.JobLog;
import jehc.cloud.job.dao.JobLogDao;
import jehc.cloud.job.service.JobLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
* 调度器日志 
* 2016-05-25 20:16:23  邓纯杰
*/
@Service
public class JobLogServiceImpl extends BaseService implements JobLogService {
	@Autowired
	private JobLogDao jobLogDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<JobLog> getJobLogListByCondition(Map<String,Object> condition){
		try{
			return jobLogDao.getJobLogListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param job_log_id
	* @return
	*/
	public JobLog getJobLogById(String job_log_id){
		try{
			return jobLogDao.getJobLogById(job_log_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param jobLog
	* @return
	*/
	public int addJobLog(JobLog jobLog){
		int i = 0;
		try {
			i = jobLogDao.addJobLog(jobLog);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delJobLog(Map<String,Object> condition){
		int i = 0;
		try {
			i = jobLogDao.delJobLog(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
