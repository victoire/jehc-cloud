package jehc.cloud.job;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @Desc 启动应用
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@SpringBootApplication
@MapperScan("jehc.cloud.*.*dao")
@EnableDiscoveryClient
@ServletComponentScan
@ComponentScan("jehc")
@EnableScheduling
public class JobApplication {
	public static void main(String[] args) {
		SpringApplication.run(JobApplication.class, args);
	}

}
