package jehc.cloud.job.vo;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@Slf4j
public class ParamInfo<T> {
    private Object obj;//对象
    private T data;//泛型对象
    private String message;//消息体内容

    public ParamInfo(){

    }

    public ParamInfo(Object obj){

    }

    @Override
    public String toString() {
        return "ParamInfo{" +
                "obj=" + obj +
                ", data=" + data +
                ", message='" + message + '\'' +
                '}';
    }
}
