package jehc.cloud.job.task;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Desc 缓存任务
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class XtCacheTask extends Thread{
	/**
	 * 业务逻辑处理
	 */
	public void service() {
		new XtCacheTask().start();
	}
	public void run(){
		try {

		} catch (Exception e) {
			log.error("调度缓存异常：{}",e);
		}
    }
}
