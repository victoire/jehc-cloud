package jehc.cloud.job.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.cloud.common.annotation.AuthUneedLogin;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.job.model.JobConfig;
import jehc.cloud.job.service.JobConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.github.pagehelper.PageInfo;

/**
 * @Desc 任务调度配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/jobConfig")
@Api(value = "任务调度配置API",tags = "任务调度配置API",description = "任务调度配置API")
public class JobConfigController extends BaseAction {
	@Autowired
	private JobConfigService jobConfigService;
	
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getJobConfigListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<JobConfig> jobConfigList = jobConfigService.getJobConfigListByCondition(condition);
		PageInfo<JobConfig> page = new PageInfo<JobConfig>(jobConfigList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	 *
	 * @return
	 */
	@AuthUneedLogin
	@GetMapping(value="/lists")
	public BaseResult lists(){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("jobStatus", "NORMAL");
		List<JobConfig> jobConfigList = jobConfigService.getJobConfigListAllByCondition(condition);
		return outDataStr(jobConfigList);
	}
	
	/**
	* 单个记录
	* @param id
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getJobConfigById(@PathVariable("id")String id){
		JobConfig jobConfig = jobConfigService.getJobConfigById(id);
		return outDataStr(jobConfig);
	}
	
	/**
	* 添加
	* @param jobConfig
	*/
	@PostMapping(value="/add")
	public BaseResult addJobConfig(@RequestBody JobConfig jobConfig){
		int i = 0;
		if(null != jobConfig){
			jobConfig.setId(toUUID());
			i= jobConfigService.addJobConfig(jobConfig);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 修改
	* @param jobConfig
	*/
	@PutMapping(value="/update")
	public BaseResult updateJobConfig(@RequestBody JobConfig jobConfig){
		int i = 0;
		if(null != jobConfig){
			i= jobConfigService.updateJobConfig(jobConfig);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 删除
	* @param id
	*/
	@DeleteMapping(value="/delete")
	public BaseResult delJobConfig(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i= jobConfigService.delJobConfig(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
