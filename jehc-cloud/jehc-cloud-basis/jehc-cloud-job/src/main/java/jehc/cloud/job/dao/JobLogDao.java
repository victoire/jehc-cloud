package jehc.cloud.job.dao;
import java.util.List;
import java.util.Map;

import jehc.cloud.job.model.JobLog;

/**
* 调度器日志 
* 2016-05-25 20:16:23  邓纯杰
*/
public interface JobLogDao {
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<JobLog> getJobLogListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param xt_quartz_log_id 
	* @return
	*/
	JobLog getJobLogById(String xt_quartz_log_id);
	/**
	* 添加
	* @param jobLog
	* @return
	*/
	int addJobLog(JobLog jobLog);
	/**
	* 删除
	* @param condition
	* @return
	*/
	int delJobLog(Map<String, Object> condition);
}
