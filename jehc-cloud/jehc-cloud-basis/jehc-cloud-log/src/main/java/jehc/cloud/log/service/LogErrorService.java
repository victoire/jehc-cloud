package jehc.cloud.log.service;
import java.util.List;
import java.util.Map;
import jehc.cloud.log.model.LogError;

/**
* @Desc 异常日志表 
* @Author 邓纯杰
* @CreateTime 2022-08-24 11:24:05
*/
public interface LogErrorService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LogError> getLogErrorListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	LogError getLogErrorById(String id);
	/**
	* 添加
	* @param logError 
	* @return
	*/
	int addLogError(LogError logError);
	/**
	* 修改
	* @param logError 
	* @return
	*/
	int updateLogError(LogError logError);
	/**
	* 修改（根据动态条件）
	* @param logError 
	* @return
	*/
	int updateLogErrorBySelective(LogError logError);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLogError(Map<String, Object> condition);
}
