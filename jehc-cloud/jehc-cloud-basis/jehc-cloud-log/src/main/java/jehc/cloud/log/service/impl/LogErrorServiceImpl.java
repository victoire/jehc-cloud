package jehc.cloud.log.service.impl;
import java.util.List;
import java.util.Map;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.log.service.LogErrorService;
import jehc.cloud.log.dao.LogErrorDao;
import jehc.cloud.log.model.LogError;

/**
* @Desc 异常日志表 
* @Author 邓纯杰
* @CreateTime 2022-08-24 11:24:05
*/
@Service("logErrorService")
public class LogErrorServiceImpl extends BaseService implements LogErrorService{
	@Autowired
	private LogErrorDao logErrorDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LogError> getLogErrorListByCondition(Map<String,Object> condition){
		try{
			return logErrorDao.getLogErrorListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public LogError getLogErrorById(String id){
		try{
			LogError logError = logErrorDao.getLogErrorById(id);
			return logError;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param logError 
	* @return
	*/
	public int addLogError(LogError logError){
		int i = 0;
		try {
			i = logErrorDao.addLogError(logError);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param logError 
	* @return
	*/
	public int updateLogError(LogError logError){
		int i = 0;
		try {
			i = logErrorDao.updateLogError(logError);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param logError 
	* @return
	*/
	public int updateLogErrorBySelective(LogError logError){
		int i = 0;
		try {
			i = logErrorDao.updateLogErrorBySelective(logError);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLogError(Map<String,Object> condition){
		int i = 0;
		try {
			i = logErrorDao.delLogError(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
