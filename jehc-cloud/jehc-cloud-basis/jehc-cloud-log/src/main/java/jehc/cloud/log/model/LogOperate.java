package jehc.cloud.log.model;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;

/**
* @Desc 操作日志表 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:27:00
*/
@Data
public class LogOperate extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**id**/
	private Long begin_time;/**开始时间**/
	private Long end_time;/**结束时间**/
	private String class_name;/**执行类名**/
	private String modules;/**模块**/
	private String method;/**执行方式（POST，GET,PUT,DELETE等等）**/
	private String param;/**方法参数**/
	private String result;/**执行结果**/
	private String total_time;/**执行总时间**/
	private String action_;/**访问方法**/
	private String uri;/**访问地址**/
	private Integer max_memory;/**最大内存**/
	private Integer total_memory;/**已分配内存**/
	private Integer free_memory;/**已分配内存中的剩余空间**/
	private Integer use_memory;/**最大可用内存**/
	private String batch;/**批次**/
	private Integer type;/**类型：0业务1参数拦截**/
}
