package jehc.cloud.log.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import jehc.cloud.common.annotation.AuthUneedLogin;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.log.model.LogLoadinfo;
import jehc.cloud.log.service.LogLoadinfoService;

/**
* @Desc 页面加载信息 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:22:05
*/
@RestController
@RequestMapping("/logLoadinfo")
public class LogLoadinfoController extends BaseAction{
	@Autowired
	private LogLoadinfoService logLoadinfoService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getLogLoadinfoListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LogLoadinfo> logLoadinfoList = logLoadinfoService.getLogLoadinfoListByCondition(condition);
		PageInfo<LogLoadinfo> page = new PageInfo<LogLoadinfo>(logLoadinfoList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getLogLoadinfoById(@PathVariable("id")String id){
		LogLoadinfo logLoadinfo = logLoadinfoService.getLogLoadinfoById(id);
		return outDataStr(logLoadinfo);
	}
	/**
	* 添加
	* @param logLoadinfo
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	@AuthUneedLogin
	public BaseResult addLogLoadinfo(@RequestBody LogLoadinfo logLoadinfo){
		int i = 0;
		if(null != logLoadinfo){
			logLoadinfo.setId(toUUID());
			i=logLoadinfoService.addLogLoadinfo(logLoadinfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLogLoadinfo(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=logLoadinfoService.delLogLoadinfo(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
