package jehc.cloud.log.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import jehc.cloud.common.annotation.AuthUneedLogin;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.*;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.common.idgeneration.UUID;
import jehc.cloud.log.model.LogError;
import jehc.cloud.log.service.LogErrorService;

/**
* @Desc 异常日志表 
* @Author 邓纯杰
* @CreateTime 2022-08-24 11:24:05
*/
@RestController
@RequestMapping("/logError")
public class LogErrorController extends BaseAction{

	@Autowired
	private LogErrorService logErrorService;

	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getLogErrorListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LogError> logErrorList = logErrorService.getLogErrorListByCondition(condition);
		PageInfo<LogError> page = new PageInfo<LogError>(logErrorList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getLogErrorById(@PathVariable("id")String id){
		LogError logError = logErrorService.getLogErrorById(id);
		return outDataStr(logError);
	}

	/**
	* 添加
	* @param logError 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	@AuthUneedLogin
	public BaseResult addLogError(@RequestBody LogError logError){
		int i = 0;
		if(null != logError){
			logError.setId(toUUID());
			i=logErrorService.addLogError(logError);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 删除
	 * @param id
	 */
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLogError(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=logErrorService.delLogError(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
