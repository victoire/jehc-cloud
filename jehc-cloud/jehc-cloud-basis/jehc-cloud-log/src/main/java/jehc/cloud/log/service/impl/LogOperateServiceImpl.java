package jehc.cloud.log.service.impl;
import java.util.List;
import java.util.Map;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.log.service.LogOperateService;
import jehc.cloud.log.dao.LogOperateDao;
import jehc.cloud.log.model.LogOperate;

/**
* @Desc 操作日志表 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:27:00
*/
@Service("logOperateService")
public class LogOperateServiceImpl extends BaseService implements LogOperateService{
	@Autowired
	private LogOperateDao logOperateDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LogOperate> getLogOperateListByCondition(Map<String,Object> condition){
		try{
			return logOperateDao.getLogOperateListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public LogOperate getLogOperateById(String id){
		try{
			LogOperate logOperate = logOperateDao.getLogOperateById(id);
			return logOperate;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param logOperate 
	* @return
	*/
	public int addLogOperate(LogOperate logOperate){
		int i = 0;
		try {
			i = logOperateDao.addLogOperate(logOperate);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param logOperate 
	* @return
	*/
	public int updateLogOperate(LogOperate logOperate){
		int i = 0;
		try {
			i = logOperateDao.updateLogOperate(logOperate);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param logOperate 
	* @return
	*/
	public int updateLogOperateBySelective(LogOperate logOperate){
		int i = 0;
		try {
			i = logOperateDao.updateLogOperateBySelective(logOperate);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLogOperate(Map<String,Object> condition){
		int i = 0;
		try {
			i = logOperateDao.delLogOperate(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
