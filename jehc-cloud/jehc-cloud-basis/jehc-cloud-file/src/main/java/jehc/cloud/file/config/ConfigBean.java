package jehc.cloud.file.config;

import jehc.cloud.common.util.logger.Logback4jUtil;
import jehc.cloud.oauth.client.AuthHandler;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Configuration
public class ConfigBean  extends Logback4jUtil implements WebMvcConfigurer {
    //Spring Cloud Ribbon是基于Netflix Ribbon实现的一套客户端 LoadBalanced 负载均衡的工具。
    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    /**
     * 由于在拦截器中注解无效，需要提前注入bean
     * @return
     */
    @Bean
    public AuthHandler authHandler(){
        return new AuthHandler();
    }

    /**
     * 放开资源
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册TestInterceptor拦截器
        InterceptorRegistration registration = registry.addInterceptor(authHandler());
        registration.addPathPatterns("/**");
        registration.excludePathPatterns(
                "/favicon.ico",
                "/js/**",
                "/html/**",
                "/resources/*",
                "/webjars/**",
                "/v2/api-docs/**",
                "/swagger-ui.html/**",
                "/swagger-resources/**",
                "/oauth",
                "/isAdmin",
                "/accountInfo",
                "/httpSessionEntity"
        );
    }
}
