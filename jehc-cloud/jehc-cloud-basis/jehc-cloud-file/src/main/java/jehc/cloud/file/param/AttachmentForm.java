package jehc.cloud.file.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 附件查询条件
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@ApiModel(value = "附件查询条件")
@Data
public class AttachmentForm {

    @ApiModelProperty(value = "附件编号")
    private String xt_attachment_id;/**附件编号**/

}
