package jehc.cloud.file.web;

import com.github.pagehelper.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.AuthUneedLogin;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BaseJson;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.constant.PathConstant;
import jehc.cloud.common.entity.AttachmentEntity;
import jehc.cloud.common.entity.PathEntity;
import jehc.cloud.common.entity.UploadEntity;
import jehc.cloud.common.util.ExceptionUtil;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.common.util.file.FileEntity;
import jehc.cloud.common.util.file.FileUtil;
import jehc.cloud.file.model.XtAttachment;
import jehc.cloud.file.service.XtAttachmentService;
import jehc.cloud.file.util.Constant;
import jehc.cloud.file.util.FileService;
import jehc.cloud.file.util.office.OfficeUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

/**
 * @Desc 通用API
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@Api(value = "通用API",tags = "通用API",description = "通用API")
@Slf4j
public class FileController extends BaseAction {
    @Autowired
    private XtAttachmentService xtAttachmentService;

    @Autowired
    private FileService fileService;

    @Autowired
    private OfficeUtil officeUtil;

    /**
     * 单个上传
     * @param map 校验类型
     * @return
     */
    @AuthUneedLogin
    @PostMapping(value="/upload")
    @ApiOperation(value="上传文件", notes="上传文件")
    public BaseResult upload(@RequestParam Map<String, Object> map, @RequestParam("picFile") MultipartFile multipartFile){
        BaseJson baseJson = new BaseJson();
        List<MultipartFile> multipartFileList = new ArrayList<>();
        if(null != multipartFile){
            multipartFileList.add(multipartFile);
        }
        UploadEntity uploadEntity = JsonUtil.fromFastJson(map,UploadEntity.class);
        try {
            //默认绝对路径
            String path = baseUtils.getXtPathCache(PathConstant.SYS_SOURCES_DEFAULT_PATH).get(0).getXt_path();
            //相对路径
            String relative_path = baseUtils.getXtPathCache(PathConstant.SYS_SOURCES_DEFAULT_RELATIVE_PATH).get(0).getXt_path();
            //校验xt_path_absolutek
            if(!StringUtils.isEmpty(uploadEntity.getXt_path_absolutek())){
                if(!StringUtils.isEmpty(baseUtils.getXtPathCache(uploadEntity.getXt_path_absolutek()).get(0).getXt_path())){
                    path = baseUtils.getXtPathCache(uploadEntity.getXt_path_absolutek()).get(0).getXt_path();
                }
            }
            //校验xt_path_relativek
            if(!StringUtils.isEmpty(uploadEntity.getXt_path_relativek())){
                if(!StringUtils.isEmpty(baseUtils.getXtPathCache(uploadEntity.getXt_path_relativek()).get(0).getXt_path())){
                    relative_path = baseUtils.getXtPathCache(uploadEntity.getXt_path_relativek()).get(0).getXt_path();
                }
            }
            FileUtil.initPath(path);
            uploadEntity.setXt_path_relativek(relative_path);
            List<XtAttachment> xtAttachmentList = fileService.upLoad(uploadEntity,multipartFileList,path);
            int i = xtAttachmentService.addXtAttachment(xtAttachmentList.get(0));
            if(i>0){
                baseJson.setJsonID(xtAttachmentList.get(0).getXt_attachment_id());
                baseJson.setMessage("上传成功");
                baseJson.setFileType(xtAttachmentList.get(0).getXt_attachmentType());
                if(null != xtAttachmentList.get(0).getXt_attachmentType() && !"".equals(xtAttachmentList.get(0).getXt_attachmentType())){
                    if("image/jpeg".equals(xtAttachmentList.get(0).getXt_attachmentType())||
                            "image/png".equals(xtAttachmentList.get(0).getXt_attachmentType())||
                            "image/gif".equals(xtAttachmentList.get(0).getXt_attachmentType())||
                            "image/bmp".equals(xtAttachmentList.get(0).getXt_attachmentType())){
                        //校验是否为自定义路径
                        if(!StringUtils.isEmpty(uploadEntity.getXt_path_urlk())){
                            baseJson.setJsonValue(baseUtils.getXtPathCache(uploadEntity.getXt_path_urlk()).get(0).getXt_path() + Constant.slash + xtAttachmentList.get(0).getXt_attachmentPath());
                        }else{
                            baseJson.setJsonValue(baseUtils.getXtPathCache("jehcsources_base_url").get(0).getXt_path() + Constant.slash + xtAttachmentList.get(0).getXt_attachmentPath());
                        }
                    }else{
                        //非图片格式让其显示成功图标即可
                        baseJson.setJsonValue(baseUtils.getXtPathCache("jehc_upload_sucess").get(0).getXt_path());
                    }
                }else{
                    baseJson.setJsonValue(baseUtils.getXtPathCache("jehc_upload_fail").get(0).getXt_path());
                }
            }else{
                baseJson.setJsonValue(baseUtils.getXtPathCache("jehc_upload_fail").get(0).getXt_path());
                baseJson.setMessage("上传失败");
            }
            return BaseResult.success(baseJson);
        } catch (Exception e) {
            baseJson.setMessage("上传失败、失败原因:"+e.getMessage());
            baseJson.setJsonID("0");
            return BaseResult.fail(baseJson);
        }
    }

    /**
     * 批量上传
     * @param map 校验类型
     * @return
     */
    @AuthUneedLogin
    @PostMapping(value="/batch/upload")
    @ApiOperation(value="批量上传文件", notes="批量上传文件")
    public BaseResult<BaseJson> uploadBatch(@RequestParam Map<String, Object> map,@RequestParam("mutilJehcFile") MultipartFile[] multfiles){
        List<BaseJson> baseJsonList = new ArrayList<>();
        List<MultipartFile> multipartFileList = new ArrayList<>(Arrays.asList(multfiles));
        /*CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        //先判断request中是否包涵multipart类型的数据，
        if (multipartResolver.isMultipart(request)) {
            //再将request中的数据转化成multipart类型的数据
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest)request;
            Iterator iter = multiRequest.getFileNames();
            while(iter.hasNext()){
                MultipartFile file = multiRequest.getFile((String) iter.next());
                if(file != null){
                    multipartFileList.add(file);
                }
            }
        }*/

        UploadEntity uploadEntity = JsonUtil.fromFastJson(map,UploadEntity.class);
        try {
            //默认绝对路径
            String path = baseUtils.getXtPathCache(PathConstant.SYS_SOURCES_DEFAULT_PATH).get(0).getXt_path();
            //相对路径
            String relative_path = baseUtils.getXtPathCache(PathConstant.SYS_SOURCES_DEFAULT_RELATIVE_PATH).get(0).getXt_path();
            //校验xt_path_absolutek
            if(!StringUtils.isEmpty(uploadEntity.getXt_path_absolutek())){
                if(!StringUtils.isEmpty(baseUtils.getXtPathCache(uploadEntity.getXt_path_absolutek()).get(0).getXt_path())){
                    path = baseUtils.getXtPathCache(uploadEntity.getXt_path_absolutek()).get(0).getXt_path();
                }
            }
            //校验xt_path_relativek
            if(!StringUtils.isEmpty(uploadEntity.getXt_path_relativek())){
                if(!StringUtils.isEmpty(baseUtils.getXtPathCache(uploadEntity.getXt_path_relativek()).get(0).getXt_path())){
                    relative_path = baseUtils.getXtPathCache(uploadEntity.getXt_path_relativek()).get(0).getXt_path();
                }
            }
            FileUtil.initPath(path);
            uploadEntity.setXt_path_relativek(relative_path);
            List<XtAttachment> xtAttachmentList = fileService.upLoad(uploadEntity,multipartFileList,path);
            int i = xtAttachmentService.addBatchXtAttachment(xtAttachmentList);
            if(i>0){
                for(XtAttachment xtAttachment : xtAttachmentList){
                    BaseJson baseJson = new BaseJson();
                    baseJson.setJsonID(xtAttachment.getXt_attachment_id());
                    baseJson.setMessage("上传成功");
                    baseJson.setFileType(xtAttachment.getXt_attachmentType());
                    if(StringUtil.isNotEmpty(xtAttachment.getXt_attachmentType())){
                        if("image/jpeg".equals(xtAttachment.getXt_attachmentType())||
                                "image/png".equals(xtAttachment.getXt_attachmentType())||
                                "image/gif".equals(xtAttachment.getXt_attachmentType())||
                                "image/bmp".equals(xtAttachment.getXt_attachmentType())){
                            //校验是否为自定义路径
                            if(!StringUtils.isEmpty(uploadEntity.getXt_path_urlk())){
                                baseJson.setJsonValue(baseUtils.getXtPathCache(uploadEntity.getXt_path_urlk()).get(0).getXt_path()+ Constant.slash + xtAttachment.getXt_attachmentPath());
                            }else{
                                baseJson.setJsonValue(baseUtils.getXtPathCache("jehcsources_base_url").get(0).getXt_path()+ Constant.slash +xtAttachment.getXt_attachmentPath());
                            }
                        }else{
                            //非图片格式让其显示成功图标即可
                            baseJson.setJsonValue(baseUtils.getXtPathCache("jehc_upload_sucess").get(0).getXt_path());
                        }
                    }else{
                        baseJson.setJsonValue(baseUtils.getXtPathCache("jehc_upload_fail").get(0).getXt_path());
                    }
                    baseJsonList.add(baseJson);
                }

            }else{
                BaseResult.fail("上传失败");
            }
            return BaseResult.success(baseJsonList);
        } catch (Exception e) {
            return BaseResult.fail("上传失败、失败原因:"+e.getMessage());
        }
    }

    /**
     * 按条件获取附件所有相关字段信息 并且与相关附件字段匹配
     * @param attachmentEntity
     * field_name与xt_attachment_id必须满足的规则是如parameter={field_name:'field1,field2,field3',xt_attachment_id:'1,2,3'}一一对应
     */
    @AuthUneedLogin
    @PostMapping(value="/attAchmentPathpp")
    @ApiOperation(value="查询文件", notes="查询文件")
    public BaseResult getAttachmentPathPP(@RequestBody AttachmentEntity attachmentEntity){
        Map<String, Object> condition = new HashMap<String, Object>();
        List<PathEntity> pathEntities = baseUtils.getXtPathCache("jehcsources_base_url");
        if(null == pathEntities || pathEntities.isEmpty()){
            throw new ExceptionUtil("未能获取到路径");
        }
        PathEntity pathEntity = pathEntities.get(0);
        String path = pathEntity.getXt_path();
        List<Map<String,Object>> mapList = new ArrayList<>();
        if(!StringUtil.isEmpty(attachmentEntity.getXt_attachment_id()) && !StringUtil.isEmpty(attachmentEntity.getField_name())){
            if(attachmentEntity.getXt_attachment_id().split(",").length>0){
                String[] xt_attachment_id_array= attachmentEntity.getXt_attachment_id().split(",");
                condition.put("xt_attachment_id", attachmentEntity.getXt_attachment_id().split(","));
                if(null != xt_attachment_id_array){
                    for(int i = 0; i < xt_attachment_id_array.length; i++){
                        XtAttachment xtAttachment = xtAttachmentService.getXtAttachmentById(xt_attachment_id_array[i]);
                        //判断该附件是否使用自定义xt_path_urlk
                        if(null != xtAttachment && !StringUtils.isEmpty(xtAttachment.getXt_path_urlk())){
                            path = baseUtils.getXtPathCache(xtAttachment.getXt_path_urlk()).get(0).getXt_path();
                        }
                        if(null != xtAttachment){
                            Map<String, Object> model = new HashMap<String, Object>();
                            model.put("xt_attachment_id", xtAttachment.getXt_attachment_id());
                            model.put("xt_attachmentType", xtAttachment.getXt_attachmentType());
                            model.put("xt_attachmentCtime", xtAttachment.getXt_attachmentCtime());
                            model.put("xt_attachmentSize", xtAttachment.getXt_attachmentSize());
                            //附件字段名称（即表单中附件字段）
                            model.put("field_name", attachmentEntity.getField_name().split(",")[i]);
                            if("image/jpeg".equals(xtAttachment.getXt_attachmentType())||
                                    "image/png".equals(xtAttachment.getXt_attachmentType())||
                                    "image/gif".equals(xtAttachment.getXt_attachmentType())||
                                    "image/bmp".equals(xtAttachment.getXt_attachmentType())){
                                model.put("xt_attachmentPath", path+xtAttachment.getXt_attachmentPath());
                            }else{
                                //非图片格式让其显示成功图标即可
                                model.put("xt_attachmentPath", baseUtils.getXtPathCache("jehc_upload_sucess").get(0).getXt_path());
                            }
                            //整个路径如http://www.jehc.com/images/img.png
                            model.put("xt_all_url", path+xtAttachment.getXt_attachmentPath());
                            model.put("xt_attachmentIsDelete", xtAttachment.getXt_attachmentIsDelete());
                            model.put("xt_attachmentName", xtAttachment.getXt_attachmentName());
                            model.put("xt_userinfo_id", xtAttachment.getXt_userinfo_id());
                            model.put("xt_modules_id", xtAttachment.getXt_modules_id());
                            model.put("xt_modules_order", xtAttachment.getXt_modules_order());
                            model.put("xt_attachmentTitle", xtAttachment.getXt_attachmentTitle());
                            mapList.add(model);
                        }
                    }
                    return BaseResult.success(mapList);
                }else{
                    AttachmentEntity xtAttachment = new AttachmentEntity();
                    return outItemsStr(xtAttachment);
                }
            }else{
                AttachmentEntity xtAttachment = new AttachmentEntity();
                return outItemsStr(xtAttachment);
            }
        }else{
            AttachmentEntity xtAttachment = new AttachmentEntity();
            return outItemsStr(xtAttachment);
        }
    }

    /**
     * 按条件获取附件所有相关字段信息 并且与相关附件字段匹配
     * @param attachmentEntity
     * field_name与xt_attachment_id必须满足的规则是如parameter={field_name:'field1,field2,field3',xt_attachment_id:'1,2,3'}一一对应
     */
    @AuthUneedLogin
    @PostMapping(value="/batch/attAchmentPathpp")
    @ApiOperation(value="查询文件", notes="查询文件")
    public BaseResult<XtAttachment> getBatchAttachmentPathPP(@RequestBody AttachmentEntity attachmentEntity){
        Map<String, Object> condition = new HashMap<String, Object>();
        List<PathEntity> pathEntities = baseUtils.getXtPathCache("jehcsources_base_url");
        if(null == pathEntities || pathEntities.isEmpty()){
            throw new ExceptionUtil("未能获取到路径");
        }
        PathEntity pathEntity = pathEntities.get(0);
        String path = pathEntity.getXt_path();
        List<XtAttachment> attachmentList = new ArrayList<>();
        if(!StringUtil.isEmpty(attachmentEntity.getXt_attachment_id())){
            condition.put("xt_attachment_id", attachmentEntity.getXt_attachment_id().split(","));
            attachmentList = xtAttachmentService.getXtAttachmentList(condition);
            for(XtAttachment xtAttachment:attachmentList){
                //判断该附件是否使用自定义xt_path_urlk
                if(!StringUtils.isEmpty(xtAttachment.getXt_path_urlk())){
                    path = baseUtils.getXtPathCache(xtAttachment.getXt_path_urlk()).get(0).getXt_path();
                }
                /*if(null != xtAttachment){
                    if("image/jpeg".equals(xtAttachment.getXt_attachmentType())||
                            "image/png".equals(xtAttachment.getXt_attachmentType())||
                            "image/gif".equals(xtAttachment.getXt_attachmentType())||
                            "image/bmp".equals(xtAttachment.getXt_attachmentType())){
                    }else{
                        //非图片格式让其显示成功图标即可
                        xtAttachment.setXt_attachmentPath(baseUtils.getXtPathCache("jehc_upload_sucess").get(0).getXt_path());
                    }
                }*/
                //相对路径
                String relative_path = baseUtils.getXtPathCache(PathConstant.SYS_SOURCES_DEFAULT_RELATIVE_PATH).get(0).getXt_path();
                //整个路径如http://www.jehc.com/images/img.png
                xtAttachment.setFullUrl( path+ xtAttachment.getXt_attachmentPath());
            }
        }
        return BaseResult.success(attachmentList);
    }

    /**
     * 按条件获取附件所有相关字段信息
     * @param xt_attachment_id
     * @param request
     * @param response
     */
    @AuthUneedLogin
    @GetMapping(value="/get/attachmentPath/{xt_attachment_id}")
    @ApiOperation(value="根据附件编号查询文件", notes="根据附件编号查询文件")
    public BaseResult<XtAttachment> getAttachmentPath(@PathVariable("xt_attachment_id")String xt_attachment_id, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> condition = new HashMap<>();
        condition.put("xt_attachment_id", xt_attachment_id.split(","));
        List<XtAttachment> list = xtAttachmentService.getXtAttachmentList(condition);
        return outItemsStr(list);
    }

    /**
     * 下载文件
     * @return
     * @throws IOException
     */
    @AuthUneedLogin
    @GetMapping(value="/downFile")
    @ApiOperation(value="下载文件", notes="下载文件")
    public void downFile(HttpServletRequest request,HttpServletResponse response) throws IOException{
        String xt_attachment_id = request.getParameter("xt_attachment_id");
        if(!StringUtils.isEmpty(xt_attachment_id)){
            XtAttachment xtAttachment = xtAttachmentService.getXtAttachmentById(xt_attachment_id);
            String path = baseUtils.getXtPathCache(PathConstant.SYS_SOURCES_DEFAULT_PATH).get(0).getXt_path();/////////////////////后期全部废除Ehcache
            //判断该附件是否使用自定义xt_path_absolutek（自定义绝对路径K）
            if(null != xtAttachment && !StringUtils.isEmpty(xtAttachment.getXt_path_absolutek())){
                path = baseUtils.getXtPathCache(xtAttachment.getXt_path_absolutek()).get(0).getXt_path();
            }
            // * 表示允许任何域名跨域访问
            response.setContentType("text/html;charset=utf-8");
            File file = new File(path+ Constant.slash +xtAttachment.getXt_attachmentName());
            String xt_attachment_name=xtAttachment.getXt_attachmentTitle();
            if(!file.exists()){
                response.getWriter().println("<script type='text/javascript'>top.window.parent.toastrBoot(4,'<font color=red>您访问的文件已经不存在，请联系管理员！</font>');</script>");
            }else{
                int len=0;
                byte []buffers = new byte[1024];
                BufferedInputStream br = null;
                OutputStream ut = null;
                response.reset();
                response.setHeader("Access-Control-Allow-Origin", "*");
                response.setContentType("application/x-msdownload");
                response.setHeader("Content-Disposition","attachment;filename=" +java.net.URLEncoder.encode(xt_attachment_name,"UTF-8"));
                br = new BufferedInputStream(new FileInputStream(file));
                ut = response.getOutputStream();
                while((len=br.read(buffers))!=-1){
                    ut.write(buffers, 0, len);
                }
                ut.flush();
                ut.close();
                br.close();
            }
        }else{
            response.getWriter().println("<script type='text/javascript'>top.window.parent.toastrBoot(4,'<font color=red>您访问的文件已经不存在，请联系管理员！</font>');</script>");
        }
    }

    /*
     *//**
     * 下载文件（网络批量）
     * @return
     * @throws IOException
     *//*
	@AuthUneedLogin
	@RequestMapping(value="/downBatchFileByUrl")
	public void downBatchFileByUrl(String[] pathList,HttpServletRequest request,HttpServletResponse response) throws IOException{
		String downloadFilename = "jEHC开源平台压缩文件.zip";//文件的名称
		downloadFilename = URLEncoder.encode(downloadFilename, "UTF-8");//转换中文否则可能会产生乱码
		response.setContentType("application/octet-stream");// 指明response的返回对象是文件流
        response.setHeader("Content-Disposition", "attachment;filename=" + downloadFilename);// 设置在下载框默认显示的文件名
        ZipOutputStream zos = new ZipOutputStream(response.getOutputStream());
        for (int i=0;i<pathList.length;i++) {
           URL url = new URL(pathList[i]);
           zos.putNextEntry(new ZipEntry(pathList[i]));
           InputStream fis = url.openConnection().getInputStream();
           bytesources[] buffer = new bytesources[1024];
           int r = 0;
           while ((r = fis.read(buffer)) != -1) {
               zos.write(buffer, 0, r);
           }
           fis.close();
          }
        zos.flush();
        zos.close();
	}
	*/

    /**
     * 下载文件（非网络批量）
     * @return
     * @throws IOException
     */
    @AuthUneedLogin
    @GetMapping(value="/downBatchFile")
    @ApiOperation(value="批量下载文件", notes="批量下载文件")
    public void downBatchFile(HttpServletRequest request,HttpServletResponse response) throws IOException{
        String xt_attachment_id = request.getParameter("xt_attachment_id");
        if(!StringUtils.isEmpty(xt_attachment_id)){
            String[] xt_attachment_ids = xt_attachment_id.split(",");
            List<String> paths = new ArrayList<String>();
            List<String> oldNames = new ArrayList<String>();
            for(String xt_attachment_id_:xt_attachment_ids){
                String path = baseUtils.getXtPathCache(PathConstant.SYS_SOURCES_DEFAULT_PATH).get(0).getXt_path();
                XtAttachment xtAttachment = xtAttachmentService.getXtAttachmentById(xt_attachment_id_);
                //判断该附件是否使用自定义xt_path_absolutek（自定义绝对路径K）
                if(null != xtAttachment && !StringUtils.isEmpty(xtAttachment.getXt_path_absolutek())){
                    path = baseUtils.getXtPathCache(xtAttachment.getXt_path_absolutek()).get(0).getXt_path();

                }
                /*if(path.substring(path.length()-2, path.length()-1).equals("/")){
                    path = path+xtAttachment.getXt_attachmentName();
                }else {

                }*/
                path = path+ Constant.slash +xtAttachment.getXt_attachmentName();
                oldNames.add(xtAttachment.getXt_attachmentTitle());
                paths.add(path);
            }
            FileUtil fileUtil = new FileUtil();
            FileEntity fileEntity = new FileEntity(paths,oldNames,"D://","jEhc开源.zip");
            fileUtil.callZipFilePath(fileEntity);
        }else{
            response.getWriter().println("<script type='text/javascript'>top.window.parent.toastrBoot(4,'<font color=red>您访问的文件已经不存在，请联系管理员！</font>');</script>");
        }
    }

    /**
     * 附件预览
     * @param multipartFile
     * @param response
     */
    @RequestMapping(value = "/preview")
    @AuthUneedLogin
    public void preview(@RequestParam("file")MultipartFile multipartFile, HttpServletResponse response) {
        officeUtil.preview(multipartFile,response);
    }

    /**
     * 附件预览
     * @param xt_attachment_id
     * @param response
     */
    @AuthUneedLogin
    @GetMapping(value="/preview/{xt_attachment_id}")
    @ApiOperation(value="附件预览", notes="附件预览")
    public void preview(@PathVariable("xt_attachment_id")String xt_attachment_id,HttpServletResponse response){
        XtAttachment xtAttachment = xtAttachmentService.getXtAttachmentById(xt_attachment_id);
        String path = baseUtils.getXtPathCache(PathConstant.SYS_SOURCES_DEFAULT_PATH).get(0).getXt_path();/////////////////////后期全部废除Ehcache
        //判断该附件是否使用自定义xt_path_absolutek（自定义绝对路径K）
        if(null != xtAttachment && !StringUtils.isEmpty(xtAttachment.getXt_path_absolutek())){
            path = baseUtils.getXtPathCache(xtAttachment.getXt_path_absolutek()).get(0).getXt_path();
        }
        try {
            FileInputStream  fileInputStream = new FileInputStream(path+ Constant.slash +xtAttachment.getXt_attachmentName());
            officeUtil.preview(fileInputStream,xtAttachment.getXt_attachmentTitle(),response);
        }catch (Exception e){
            log.error("预览异常：{}",e);
        }
    }
}
