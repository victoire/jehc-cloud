//返回
function goback(){
    tlocation(base_html_redirect+'/omp/route/dynamic-route/dynamic-route-list.html');
}

$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});

$(document).ready(function(){
    initComboData("modules_id",ompModules+"/dynamicRouteModules/listAll","id","name",null,"");
});

//保存
function addDynamicRoute(){
    submitBForm('defaultForm',ompModules+'/dynamicRoute/add',base_html_redirect+'/omp/route/dynamic-route/dynamic-route-list.html');
}

