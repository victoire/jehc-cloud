//返回
function goback(){
    tlocation(base_html_redirect+'/omp/route/dynamic-route-modules/dynamic-route-modules-list.html');
}

$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});

//保存
function updateDynamicRouteModules(){
    submitBForm('defaultForm',ompModules+'/dynamicRouteModules/update',base_html_redirect+'/omp/route/dynamic-route-modules/dynamic-route-modules-list.html',null,"PUT");
}

$(document).ready(function(){
    var id = GetQueryString("id");
    //加载表单数据
    ajaxBRequestCallFn(ompModules+"/dynamicRouteModules/get/"+id,{},function(result){
        $("#id").val(result.data.id);
        $("#name").val(result.data.name);
    });
});