//返回r
function goback(){
	tlocation(base_html_redirect+'/job/job-config/job-config-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateXtQuartz(){
    submitBForm('defaultForm',jobModules+'/jobConfig/update',base_html_redirect+'/job/job-config/job-config-list.html',null,"PUT");
}


$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
    ajaxBRequestCallFn(jobModules+"/jobConfig/get/"+id,{},function(result){
        $("#id").val(result.data.id);
        $("#jobId").val(result.data.jobId);
        $("#jobName").val(result.data.jobName);
        $("#jobGroup").val(result.data.jobGroup);
        $("#jobStatus").val(result.data.jobStatus);
        $("#cronExpression").val(result.data.cronExpression);
        $("#desc_").val(result.data.desc_);
        $("#jobTitle").val(result.data.jobTitle);
        $("#clientId").val(result.data.clientId);
        $("#clientGroupId").val(result.data.clientGroupId);
        $("#jobHandler").val(result.data.jobHandler);
        $("#jobPara").val(result.data.jobPara);
    });
});

