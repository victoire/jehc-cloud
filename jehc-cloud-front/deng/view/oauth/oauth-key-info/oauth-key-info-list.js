var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,oauthModules+'/oauthKeyInfo/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"key_info_id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:"key_info_id",
				width:"50px"
			},
			{
				data:'title',
                width:"50px"
			},
			{
				data:'key_exp_date',
                width:"100px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
			},
			{
				data:'isUseExpDate',
                width:"100px",
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='m-badge m-badge--info'>是</span>"
                    }
                    if(data == 1){
                        return "<span class='m-badge m-badge--success'>否</span>"
                    }
                    return "<span class='m-badge m-badge--info'>缺省</span>"
                }
			},
			{
				data:'status',
                width:"50px",
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='m-badge m-badge--info'>正常</span>"
                    }
                    if(data == 1){
                        return "<span class='m-badge m-badge--danger'>冻结</span>"
                    }
                    return "<span class='m-badge m-badge--info'>缺省</span>"
                }
			},
            {
                data:"createBy",
                render:function(data, type, row, meta) {
                    if(null == data || data == ''){
                        return "<font color='#6495ed'>\</font>";
                    }
                    return data;
                }
            },
            {
                data:"create_time",
                width:"150px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
            {
                data:"modifiedBy",
                render:function(data, type, row, meta) {
                    if(null == data || data == ''){
                        return "<font color='#6495ed'>\</font>";
                    }
                    return data;
                }
            },
            {
                data:"update_time",
                width:"150px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
			{
				data:"key_info_id",
				render:function(data, type, row, meta) {
					var key_info_id = row.key_info_id;
					var title = row.title;
                    var btn = '<button onclick=sysModeWin("'+title+'","'+key_info_id+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="子系统列表"><i class="icon-wrench"></i></button>'
					btn = btn+'<button onclick=javascript:toOauthKeyInfoDetail("'+key_info_id+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" ><i class="la la-bell"></i></button>';
                    return btn;
				}
			}
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
});
//新增
function toOauthKeyInfoAdd(){
	tlocation(base_html_redirect+'/oauth/oauth-key-info/oauth-key-info-add.html');
}
//修改
function toOauthKeyInfoUpdate(){
	if($(".checkchild:checked").length != 1){
		toastrBoot(4,"选择数据非法");
		return;
	}
	var id = $(".checkchild:checked").val();
	tlocation(base_html_redirect+'/oauth/oauth-key-info/oauth-key-info-update.html?key_info_id='+id);
}
//详情
function toOauthKeyInfoDetail(id){
	tlocation(base_html_redirect+'/oauth/oauth-key-info/oauth-key-info-detail.html?key_info_id='+id);
}
//删除
function delOauthKeyInfo(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要删除的数据");
		return;
	}
	msgTishCallFnBoot("确定要删除所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {key_info_id:id,_method:'DELETE'};
		ajaxBReq(oauthModules+'/oauthKeyInfo/delete',params,['datatables'],null,"DELETE");
	})
}

/**
 * 子系统列表
 * @param title
 * @param key_info_id
 */
function sysModeWin(title,key_info_id) {

}