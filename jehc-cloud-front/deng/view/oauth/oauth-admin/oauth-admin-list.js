$(function () {
    initSysModeTable();
})

var dialogWating;
function initSysModeTable(){
    dialogWating = showWating({msg:'正在拼命的加载中...'});
    $.ajax({
        url:oauthModules+"/oauthSysMode/listAll",
        type:"GET",
        dataType:"json",
        success:function(data) {
            data = data.data;
            closeWating(null,dialogWating);
            var $table = $("#table");
            $('#table').bootstrapTable('destroy').bootstrapTable({
                columns:[
                    {
                        title:'<font style="">隶属平台</font>',
                        field:'sysname',
                        width:250,
                        align:'left'
                    },
                    {
                        title:'<font style="">状态</font>',
                        field:'sys_mode_status',
                        align:'left',
                        width:120,
                        formatter:function(value, row, index) {
                            if(value == 0){
                                return "<span class='m-badge m-badge--info'>正常</span>"
                            }
                            if(value == 1){
                                return "<span class='m-badge m-badge--success'>冻结</span>"
                            }
                            if(value == 2){
                                return "<span class='m-badge m-badge--danger'>禁用</span>"
                            }
                            return "<span class='m-badge m-badge--info'>缺省</span>"
                        }
                    },
                    {
                        title:'<font style="">创建日期</font>',
                        field:'sys_mode_create_time',
                        align:'left',
                        width:160,
                        formatter:function(value, row, index) {
                            return dateformat(value);
                        }
                    },
                    {
                        title:'<font style="">修改日期</font>',
                        field:'sys_mode_update_last_time',
                        align:'left',
                        width:160,
                        formatter:function(value, row, index) {
                            return dateformat(value);
                        }
                    },
                    {
                        title:'<font style=""></font>',
                        field:'sys_mode_id',
                        formatter:function(value, row, index) {
                            return '<button onclick=addAdmin("'+value+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="设置管理员">' +
                                '<i class="m-menu__link-icon la la-user-plus"></i>' +
                                '</button>'
                        }
                    }],
                data:data,
                method:'post',//请求方式(*)
                cache:false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性(*)
                pagination:false,//是否显示分页(*)
                sortable:true,//是否启用排序
                sortOrder:"desc",//排序方式
                sidePagination:"server",//分页方式：client客户端分页，server服务端分页(*)
                showColumns:false,//是否显示所有的列
                showRefresh:false,//是否显示刷新按钮
                minimumCountColumns:2,//最少允许的列数
                clickToSelect:true,//是否启用点击选中行
                detailView:true,//是否显示父子表    *关键位置*
                contentType:"application/json;charset=UTF-8",
                checkboxHeader:true,
                search:false,
                singleSelect:true,
                striped:false,
                showColumns:false,//开启自定义列显示功能
                //注册加载子表的事件。你可以理解为点击父表中+号时触发的事件
                // expanderExpandedClass:'fa fa-caret-down',
                // expanderCollapsedClass:'fa fa-caret-right',
                onExpandRow:function(index, row, $detail) {
                    var cur_table = $detail.html('<table></table>').find('table');
                    var html = "";
                    html += "<div style='text-align: center; '><table  class='table' style='width: 80%;margin: auto;' id='table"+index+"'>";
                    html += "</table></div>";
                    $detail.html(html);
                    initAdminTable("table"+index,row);
                }
            });
        }
    });

    $('#table').bootstrapTable('resetView', { height: 260 });
}

function detail(sys_mode_id){

}

function initAdminTable(tableId,row){
    dialogWating = showWating({msg:'正在拼命的加载中...'});
    $.ajax({
        url:oauthModules+'/oauthAdmin/listAll?sys_mode_id='+row.sys_mode_id,
        type:"POST",
        dataType:"json",
        success:function(data) {
            var $table = $("#"+tableId);
            data = data.data;
            $table.bootstrapTable('destroy').bootstrapTable({
                data:data,
                striped:true,
                class:'table table-hover table-bordered',
                sidePagination:"client",//表示服务端请求
                pagination:false,
                treeView:true,
                treeId:"id",
                // height:tableHeight()*0.7,
                treeField:"name",
                sortable:false,//是否启用排序
                columns:[
                    /*
                    {
                        field: 'ck',
                        checkbox:true
                    },
                    */
                    {
                        field:'account',
                        title:'账号'
                    },
                    {
                        field:'name',
                        title:'姓名'
                    },
                    {
                        field:'status',
                        title:'状态',
                        formatter:function(value, row, index) {
                            if(value == 0){
                                return "正常";
                            }
                            if(value == 1){
                                return "冻结";
                            }
                            if(value == 2){
                                return "禁用";
                            }
                            return "缺省";
                        }
                    },
                    {
                        field:'level',
                        title:'级别',
                        formatter:function(value, row, index) {
                            if(value == 1){
                                return "全平台管理员";
                            }
                            if(value == 2){
                                return "子系统管理员";
                            }
                            return "缺省";
                        }
                    },
                    {
                        field:'email',
                        title:'电子邮件'
                    },
                    {
                        field:'account_id',
                        title:'操作',
                        formatter:function(value, row1, index) {
                            return '<button onclick=delAdminSys("'+value+'","'+row.sys_mode_id+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="移除">' + '<i class="la la-remove"></i>' +  '</button>'
                        }
                    }
                ],
                onLoadSuccess:function(data){

                }
            });
            closeWating(null,dialogWating);
        }
    });
}

/**
 * 删除
 * @param account_id
 * @param sys_mode_id
 */
function delAdminSys(account_id,sys_mode_id){
    msgTishCallFnBoot("确定要移除该管理员？",function(){
        var params = {account_id:account_id,sys_mode_id:sys_mode_id,_method:'DELETE'};
        ajaxBRequestCallFn(oauthModules+'/oauthAdmin/delete',params,function(result){
            if(typeof(result.success) != "undefined"){
                window.parent.toastrBoot(3,result.message);
                initSysModeTable();
            }
        },null,"DELETE");
    })
}