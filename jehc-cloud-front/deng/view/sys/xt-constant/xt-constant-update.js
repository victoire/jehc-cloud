//返回
function goback(){
	tlocation(base_html_redirect+'/sys/xt-constant/xt-constant-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateXtConstant(){
	submitBForm('defaultForm',sysModules+'/xtConstant/update',base_html_redirect+'/sys/xt-constant/xt-constant-list.html',null,"PUT");
}

$(document).ready(function(){
	var xt_constant_id = GetQueryString("xt_constant_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtConstant/get/"+xt_constant_id,{},function(result){
        $("#xt_constant_id").val(result.data.xt_constant_id);
        $("#name").val(result.data.name);
        $("#ckey").val(result.data.ckey);
        $("#value").val(result.data.value);
        $("#type").val(result.data.type);
        $("#url").val(result.data.url);
        $("#remark").val(result.data.remark);
    });
});