//返回r
function goback(){
	tlocation(base_html_redirect+'/sys/xt-unit/xt-unit-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateXtUnit(){
	submitBForm('defaultForm',sysModules+'/xtUnit/update',base_html_redirect+'/sys/xt-unit/xt-unit-list.html',null,"PUT");
}


$(document).ready(function(){
	var xt_unit_id = GetQueryString("xt_unit_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtUnit/get/"+xt_unit_id,{},function(result){
        $("#xt_unit_id").val(result.data.xt_unit_id);
        $("#name").val(result.data.name);
    });
});