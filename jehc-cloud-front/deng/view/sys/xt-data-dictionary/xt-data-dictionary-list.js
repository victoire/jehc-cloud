$(function () {
	initTreeTable();
}) 
var dialogWating;
function initTreeTable(){
	dialogWating = showWating({msg:'正在拼命的加载中...'});
	$.ajax({
        url:sysModules+"/xtDataDictionary/list",
        type:"get",
        dataType:"json",
        success:function(result) {
        	closeWating(null,dialogWating);
        	var $table = $("#table");
        	var data = eval("(" + result.data + ")");
            $table.bootstrapTable('destroy').bootstrapTable({
            	data:data,
                striped:true,
                sidePagination:"client",//表示服务端请求  
                idField:'id',
                sortable:false,//是否启用排序
                showColumns:false,//开启自定义列显示功能
                columns:[
                    /* 
                    {
                        field: 'ck',
                        checkbox:true
                    }, 
                    */
                    {
                        field:'name',
                        title:'名称'
                    },
                    {
                        field:'content',
                        title:'备注'
                    },
                    {
                        field:'tempObject',
                        title:'状态',
                        formatter:'statusFormatter'
                    },
                    {
                        field:'buessid',
                        title:'操作',
                        formatter:'btnFormatter'
                    }
                ],
                treeShowField:'name',
                parentIdField:'pid',
                onLoadSuccess:function(data){
                }
            });
            $table.treegrid({
                initialState:'collapsed',//收缩
                treeColumn:0,//指明第几列数据改为树形
                expanderExpandedClass:'fa fa-minus-circle',
                expanderCollapsedClass:'fa fa-plus-circle',
                onChange:function(){
                    $table.bootstrapTable('resetWidth');
                }
            });
        }
	});
}
//格式化按钮
function btnFormatter(value, row, index){
	var integerappend = row.integerappend;
	if(integerappend == 0){
		return '<a href=javascript:addXtDataDictionary("'+value+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="添加下级"><i class="fa fa-gears"></i></a><a href=javascript:updateXtDataDictionary("'+value+'") title="编辑" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-edit"></i></a><a href=javascript:delXtDataDictionary("'+value+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="删 除"><i class="fa fa-trash-o"></i></a>';
	}else{
		return '<a href=javascript:updateXtDataDictionary("'+value+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="编 辑"><i class="fa fa-edit"></i></a><a href=javascript:delXtDataDictionary("'+value+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="删 除"><i class="fa fa-trash-o"></i></a>';
	}
}

//格式化状态
function statusFormatter(value, row, index) {
  if(value === '启用') {
    return '<span class="m-badge m-badge--info">启用</span>';
  } else {
    return '<span class="m-badge m-badge--danger">禁用</span>';
  }
}

function delXtDataDictionary(value){
	if(value == null){
		toastrBoot(4,"未能获取该数据编号");
		return;
	}
	msgTishCallFnBoot("确定删除该数据？",function(){
		var params = {xt_data_dictionary_id:value, _method:'DELETE'};
		ajaxBRequestCallFn(sysModules+'/xtDataDictionary/delete',params,function(result){
			try {
	    		if(typeof(result.success) != "undefined"){
	    			if(result.success){
	            		window.parent.toastrBoot(3,result.message);
	            		initTreeTable();
	        		}else{
	        			window.parent.toastrBoot(4,result.message);
	        		}
	    		}
			} catch (e) {
				
			}
		},null,"DELETE");
	})
}
setInterval(function(){$(".fixed-table-container").removeAttr("style");},50);