//返回
function goback(){
    tlocation(base_html_redirect+'/sys/xt-knowledge/xt-knowledge-list.html');
}

$(document).ready(function(){
    var xt_knowledge_id = GetQueryString("xt_knowledge_id");
    //加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtKnowledge/get/"+xt_knowledge_id,{},function(result){
        $("#title").val(result.data.title);
        $("#type").val(result.data.type);
        $("#state").val(result.data.state);
        $("#level").val(result.data.level);
        $("#content").val(result.data.content);
        $("#xt_knowledge_id").val(result.data.xt_knowledge_id);
        $('#summernote').summernote('code', result.data.content);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time)
    });
});

jQuery(document).ready(function(){
    $('#summernote').summernote({
        airMode: true
    });
});