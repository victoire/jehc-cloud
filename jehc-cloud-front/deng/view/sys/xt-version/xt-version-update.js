//返回
function goback(){
	tlocation(base_html_redirect+'/sys/xt-version/xt-version-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function updateXtVersion(){
	submitBForm('defaultForm',sysModules+'/xtVersion/update',base_html_redirect+'/sys/xt-version/xt-version-list.html',null,"PUT");
}

/**初始化附件右键菜单开始 参数4为1表示拥有上传和删除功能 即新增和编辑页面使用**/
initBFileRight('xt_attachment_id','xt_attachment_id_pic',1);
/**初始化附件右键菜单结束**/



$(document).ready(function(){
	var xt_version_id = GetQueryString("xt_version_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtVersion/get/"+xt_version_id,{},function(result){
        $("#xt_version_id").val(result.data.xt_version_id);
        $("#name").val(result.data.name);
        $("#remark").val(result.data.remark);
        $("#xt_attachment_id").val(result.data.xt_attachment_id);
        
        /**配置附件回显方法开始**/
        var params = {xt_attachment_id:$('#xt_attachment_id').val(),field_name:'xt_attachment_id'};
        ajaxBFilePathBackRequest(fileModules+'/attAchmentPathpp',params,null,"POST");
        /**配置附件回显方法结束**/
    });
});
