//返回
function goback(){
	tlocation(base_html_redirect+'/sys/xt-version/xt-version-list.html');
}
/**初始化附件右键菜单开始 参数4为1表示不拥有上传和删除功能 即明细页面使用**/
initBFileRight('xt_attachment_id','xt_attachment_id_pic',2);
/**初始化附件右键菜单结束**/



$(document).ready(function(){
	var xt_version_id = GetQueryString("xt_version_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtVersion/get/"+xt_version_id,{},function(result){
        $("#name").val(result.data.name);
        $("#remark").val(result.data.remark);
        $("#xt_attachment_id").val(result.data.xt_attachment_id);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time)
        
        /**配置附件回显方法开始**/
        var params = {xt_attachment_id:$('#xt_attachment_id').val(),field_name:'xt_attachment_id'};
        ajaxBFilePathBackRequest(fileModules+'/xtCommon/attAchmentPathpp',params,null,"POST");
        /**配置附件回显方法结束**/
    });
});
