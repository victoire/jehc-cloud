//返回
function goback(){
	tlocation(base_html_redirect+'/sys/xt-path/xt-path-list.html');
}


$(document).ready(function(){
	var xt_path_id = GetQueryString("xt_path_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtPath/get/"+xt_path_id,{},function(result){
        $("#xt_path_name").val(result.data.xt_path_name);
        $("#xt_path").val(result.data.xt_path);
        $("#xt_value").val(result.data.xt_value);
        $("#xt_type").val(result.data.xt_type);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time)
    });
});