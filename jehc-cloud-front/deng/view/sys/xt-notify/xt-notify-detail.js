//返回
function goback(){
    tlocation(base_html_redirect+'/sys/xt-notify/xt-notify-list.html');
}

$(document).ready(function() {
    var notify_id = GetQueryString("notify_id");
    //加载表单数据
    ajaxBRequestCallFn(sysModules + "/xtNotify/get/" + notify_id, {}, function (result) {
        $("#title").val(result.data.title);
        $("#content").val(result.data.content);
        $("#create_time").append(result.data.create_time);
        var notifyReceivers = result.data.notifyReceivers;
        if(undefined != notifyReceivers && null != notifyReceivers){
            var xtNotifyReceiver = "";
            for(var i in notifyReceivers){
                if(null != xtNotifyReceiver && "" != xtNotifyReceiver){
                    xtNotifyReceiver = xtNotifyReceiver+","+notifyReceivers[i].receive_name;
                }else{
                    xtNotifyReceiver = notifyReceivers[i].receive_name;
                }
            }
            $("#xtNotifyReceiver").append(xtNotifyReceiver);
        }
    });
});

