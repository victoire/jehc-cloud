var grid;
$(document).ready(function() {
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,logModules+'/logLoadinfo/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"id",
                width:"50px"
            },
            {
                data:'modules'
            },
            {
                data:'begtime',
                render:function(data, type, row, meta) {
                    var endtime = row.endtime;
                    try {
                        return (endtime-data)+'/ms';
                    }catch (e){
                        return "未知";
                    }
                }
            },
            {
                data:'begtime'
            },
            {
                data:'endtime'
            },
            {
                data:'createBy'
            },
            {
                data:"id",
                width:"150px",
                render:function(data, type, row, meta) {
                    var btn = '<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick=delLogLoadinfo("'+data+'") title="删 除"><i class="fa fa-times"></i></button> ';
                    return btn;
                }
            }
        ]
    });
    grid=$('#datatables').dataTable(options);
    //实现全选反选
    docheckboxall('checkall','checkchild');
    //实现单击行选中
    clickrowselected('datatables');
});

//删除
function delLogLoadinfo(id){
    msgTishCallFnBoot("确定要删除所选择的数据？",function(){
        var params = {id:data, _method:'DELETE'};
        ajaxBReq(logModules+'/logLoadinfo/delete',params,['datatables'],null,"DELETE");
    })
}

//初始化日期选择器
$(document).ready(function(){
    datetimeInit();
});
